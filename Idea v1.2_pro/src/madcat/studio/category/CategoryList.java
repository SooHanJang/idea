package madcat.studio.category;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.dialogs.CategoryDialog;
import madcat.studio.dialogs.ConfigConfirmDialog;
import madcat.studio.idea.R;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class CategoryList extends ListActivity {
	
//	private final String TAG											=	"CategoryList";
	
	private final int CATEGORY_LIST_FLAG								=	1;
	private final int CATEGORY_DEL_FLAG									=	2;
	
	private final int MODIFY_CATEGORY									=	1;
	private final int DELETE_CATEGORY									=	2;
	
	private int mCtrlFlag												=	CATEGORY_LIST_FLAG;
	private Context mContext;
	private ArrayList<String> mItems;
	private CategoryListAdapter mListAdapter;
	private LinearLayout mDynamicLayout;
	
	private SharedPreferences mConfigPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.idea_category_list);
		
		mContext = this;
		
		// SharedPrefrence 설정
		mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		// Resource Id 설정
		mDynamicLayout = (LinearLayout) findViewById(R.id.category_dynamic_del_area);

		// 사용자가 등록한 키워드를 DB 에서 불러온다.
		mItems = new ArrayList<String>();
		mItems = Utils.getCategoryList(mContext);
		
		// 유저 키워드 데이터가 없다면, 키워드를 추가하라는 토스트를 출력한다.
		if(mItems.isEmpty()) {
			Toast.makeText(mContext, getString(R.string.toast_title_empty_category), Toast.LENGTH_SHORT).show();
		}
		
		mListAdapter = new CategoryListAdapter(mContext, R.layout.idea_category_list_row, mItems);
		setListAdapter(mListAdapter);
		
		registerForContextMenu(getListView());
		
		// 카테고리 관련 도움말 토스트 설정
		Toast.makeText(mContext, getString(R.string.toast_title_help_manage_category), Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		
		menu.setHeaderTitle(getString(R.string.dialog_title_manage_category));
		menu.add(0, MODIFY_CATEGORY, Menu.NONE, getString(R.string.menu_title_category_modify));
		menu.add(0, DELETE_CATEGORY, Menu.NONE, getString(R.string.menu_title_category_del));
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo menuInfo;
		final int index;
		
		switch(item.getItemId()) {
			case MODIFY_CATEGORY:
				menuInfo = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
				index = menuInfo.position;
				
				final CategoryDialog modifyCategoryDialog = new CategoryDialog(mContext, Constants.DIALOG_CATEGORY_MODIFY, mItems.get(index));
				modifyCategoryDialog.show();
				
				modifyCategoryDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(modifyCategoryDialog.getDialogFlag()) {
							refreshList();
						}
					}
				});
				
				return true;
			case DELETE_CATEGORY:
				menuInfo = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
				index = menuInfo.position;
				
				final ConfigConfirmDialog delCategoryDialog = new ConfigConfirmDialog(mContext, getString(R.string.dialog_title_delete));
				delCategoryDialog.show();
				
				delCategoryDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(delCategoryDialog.getDialogStateFlag()) {
							SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
							ContentValues updateValues = new ContentValues();
							updateValues.put(Constants.KEYWORD_COLUMN_CATEGORY, Constants.DEFAULT_ALL_CATEGORY);
							
							updateSharedCategory(mItems.get(index));
							
							// 먼저, 삭제할 카테고리의 키워드 카테고리를 All 로 업데이트
							sdb.update(Constants.TABLE_IDEA_KEYWORD, updateValues, 
									Constants.KEYWORD_COLUMN_CATEGORY + " = '" + mItems.get(index) + "'", null);
							
							// 해당 카테고리를 데이터베이스에서 삭제
							sdb.delete(Constants.TABLE_IDEA_CATEGORY, 
									Constants.CATEGORY_COLUMN_CATEGORY + " = '" + mItems.get(index) + "'", null);
							
							sdb.close();
							
							// 사용자가 화면에 표시할 키워드의 카테고리가 지워진 카테고리로 설정되어있다면, 환경설정 값을 변경시켜야 한다.
							SharedPreferences.Editor editor = mConfigPref.edit();
							for(int i=0; i < Constants.MAX_KEYWORD_TEXTVIEW; i++) {
								if(mItems.get(index).equals(
										mConfigPref.getString(Constants.CONFIG_KEYWORD_CATEGORY + (i+1), Constants.DEFAULT_ALL_CATEGORY))) {
									editor.putString(Constants.CONFIG_KEYWORD_CATEGORY + (i+1), Constants.DEFAULT_ALL_CATEGORY);
								}
							}
							editor.commit();
							
							refreshList();
						}
					}
				});
				return true;
			default:
				return false;
		}
		
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// 누를때마다 메뉴가 Infalter 되는 것을 막기 위해, 표시된 메뉴들을 먼저 삭제한다.
		if(mCtrlFlag != CATEGORY_DEL_FLAG) {
			menu.removeItem(R.id.menu_category_add);
			menu.removeItem(R.id.menu_category_del);
			
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.category_list_menu, menu);
			
			return super.onPrepareOptionsMenu(menu);
		} 
		
		return false;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		
		switch(item.getItemId()) {
			case R.id.menu_category_add:
				mCtrlFlag = CATEGORY_LIST_FLAG;
				
				final CategoryDialog addCategoryDialog = new CategoryDialog(mContext, Constants.DIALOG_CATEGORY_ADD);
				addCategoryDialog.show();
				
				addCategoryDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(addCategoryDialog.getDialogFlag()) {
							refreshList();
						}
					}
				});
				break;
			case R.id.menu_category_del:
				mCtrlFlag = CATEGORY_DEL_FLAG;
				
				final CategoryDelAdapter delAdapter = new CategoryDelAdapter(mContext, R.layout.idea_category_list_del_row, mItems);
				setListAdapter(delAdapter);
				
				// 동적으로 키워드를 삭제할 수 있는 버튼을 추가
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
						 														 ViewGroup.LayoutParams.FILL_PARENT,
						 														 1.0F);
				Button btnDelOk = new Button(mContext);
				Button btnDelCancel = new Button(mContext);
				
				btnDelOk.setText(getString(R.string.btn_title_del_ok));
				btnDelCancel.setText(getString(R.string.btn_title_del_cancel));
				
				btnDelOk.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						if(delAdapter.getCheckedList().size() != 0) {
							final ConfigConfirmDialog deleteDialog = new ConfigConfirmDialog(mContext, getString(R.string.dialog_title_delete));
							deleteDialog.show();
							
							deleteDialog.setOnDismissListener(new OnDismissListener() {
								public void onDismiss(DialogInterface dialog) {
									if(deleteDialog.getDialogStateFlag()) {
										SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
										
										ContentValues updateValues = new ContentValues();
										updateValues.put(Constants.KEYWORD_COLUMN_CATEGORY, Constants.DEFAULT_ALL_CATEGORY);
										
										for(int i=0; i < delAdapter.getCheckedList().size(); i++) {
											updateSharedCategory(delAdapter.getCheckedList().get(i));
											
											// 먼저, 삭제할 카테고리의 키워드 카테고리를 All 로 업데이트
											sdb.update(Constants.TABLE_IDEA_KEYWORD, updateValues, 
													Constants.KEYWORD_COLUMN_CATEGORY + " = '" + delAdapter.getCheckedList().get(i) + "'", null);
											
											// 해당 카테고리를 데이터베이스에서 삭제
											sdb.delete(Constants.TABLE_IDEA_CATEGORY, 
													Constants.CATEGORY_COLUMN_CATEGORY + " = '" + delAdapter.getCheckedList().get(i) + "'", null);
											
											// 사용자가 화면에 표시할 키워드의 카테고리가 지워진 카테고리로 설정되어있다면, 환경설정 값을 변경시켜야 한다.
											SharedPreferences.Editor editor = mConfigPref.edit();
											for(int j=0; j < Constants.MAX_KEYWORD_TEXTVIEW; j++) {
												if(delAdapter.getCheckedList().get(i).equals(
														mConfigPref.getString(Constants.CONFIG_KEYWORD_CATEGORY + (j+1), Constants.DEFAULT_ALL_CATEGORY))) {
													editor.putString(Constants.CONFIG_KEYWORD_CATEGORY + (j+1), Constants.DEFAULT_ALL_CATEGORY);
												}
											}
											editor.commit();
										}
										
										sdb.close();
										
										mDynamicLayout.removeAllViews();
										refreshList();
									}
								}
							});
						}
					}
				});
				
				btnDelCancel.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						mCtrlFlag = CATEGORY_LIST_FLAG;
						mDynamicLayout.removeAllViews();
						setListAdapter(mListAdapter);
					}
				});

				// 중복 표시 방지를 위해 먼저 버튼을 삭제한다.
				mDynamicLayout.removeAllViews();
				
				mDynamicLayout.addView(btnDelOk, params);
				mDynamicLayout.addView(btnDelCancel, params);
				
				break;
			default:
				break;
		}
		
		return result;
	}
	
	private void refreshList() {
//		Log.d(TAG, "refreshList");
		mCtrlFlag = CATEGORY_LIST_FLAG;
		
		mListAdapter.clear();
		mItems = Utils.getCategoryList(mContext);
		
//		Log.d(TAG, "mItems : " + mItems + ", " + mItems.size());
		
		mListAdapter = new CategoryListAdapter(mContext, R.layout.idea_category_list_row, mItems);
		setListAdapter(mListAdapter);
	}
	
	
	/**
	 * 사용자가 사전에 설정한 카테고리가 삭제되었다면, 그 값을 All 로 바꿔줍니다.
	 * @param items
	 */
	private void updateSharedCategory(String delCategory) {
		
		SharedPreferences.Editor editor = mConfigPref.edit();
		
		for(int i=0; i < Constants.MAX_KEYWORD_TEXTVIEW; i++) {
			if(delCategory.equals(
					mConfigPref.getString(Constants.CONFIG_KEYWORD_CATEGORY + (i+1), Constants.DEFAULT_ALL_CATEGORY))) {
				editor.putString(Constants.CONFIG_KEYWORD_CATEGORY + (i+1), Constants.DEFAULT_ALL_CATEGORY);
			}
		}
		
		editor.commit();
		
//		Log.d(TAG, "mConfigPref 카테고리 수정 완료");
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		switch(mCtrlFlag) {
		case CATEGORY_LIST_FLAG:
			super.onBackPressed();
			overridePendingTransition(R.anim.fade, R.anim.fade_out);
			break;
		case CATEGORY_DEL_FLAG:
			mDynamicLayout.removeAllViews();
			refreshList();
			break;
		default:
			break;
		}
	}
}
