package madcat.studio.category;

import java.util.ArrayList;

import madcat.studio.idea.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CategoryDelAdapter extends ArrayAdapter<String> {

	private Context mContext;
	private ArrayList<String> mItems;
	
	private ArrayList<String> mDelItems;
	private ArrayList<Integer> mListItems;
	
	private LayoutInflater mInflater;
	private int mResId;
	
	public CategoryDelAdapter(Context context, int resId, ArrayList<String> items) {
		super(context, resId, items);
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		this.mDelItems = new ArrayList<String>();
		this.mListItems = new ArrayList<Integer>();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		
		if(convertView == null) {
			convertView = mInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			holder.linearLayout = (LinearLayout)convertView.findViewById(R.id.category_list_del_layout);
			holder.textCategory = (TextView)convertView.findViewById(R.id.category_list_del_row_name);
			holder.checkCategory = (CheckBox)convertView.findViewById(R.id.category_list_del_row_check);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder)convertView.getTag();
		}
		
		holder.linearLayout.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				holder.checkCategory.setChecked(!holder.checkCategory.isChecked());
			}
		});
		
		holder.textCategory.setText(mItems.get(position));
		holder.checkCategory.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					for(int i=0; i < mListItems.size(); i++) {
						if(mListItems.get(i) == position) {
							return;
						}
					}
//					Log.d(TAG, "checkPosition : " + checkPosition + ", position : " + position);
					mListItems.add(position);
					mDelItems.add(mItems.get(position));
//					mDelItems.add(mItems.get(position));
				} else {
					for(int i=0; i < mListItems.size(); i++) {
						if(mListItems.get(i) == position) {
							mListItems.remove(i);
							mDelItems.remove(mItems.get(position));
							break;
						}
					}
//					mDelItems.remove(mItems.get(position));
				}
			}
		});
		
		boolean reChecked = false;
		for(int i=0; i < mListItems.size(); i++) {
			if(mListItems.get(i) == position) {
				holder.checkCategory.setChecked(true);
				reChecked = true;
				break;
			}
		}
		
		if(!reChecked) {
			holder.checkCategory.setChecked(false);
		}
		
		return convertView;
	}
	
	public class ViewHolder {
		LinearLayout linearLayout;
		TextView textCategory;
		CheckBox checkCategory;
	}
	
	public ArrayList<String> getCheckedList() {
		return mDelItems;
	}
	
}
