package madcat.studio.service;

import java.text.SimpleDateFormat;

import madcat.studio.constants.Constants;
import madcat.studio.widget.WidgetRefresh;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

public class DailyRefreshService extends Activity {
	
	private final String TAG										=	"DailyRefreshService";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onCreate 호출");
		}
		
		super.onCreate(savedInstanceState);

		this.mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		// 키워드 갱신
		Intent refereshIntent = new Intent(this, WidgetRefresh.class);
		startActivity(refereshIntent);
		
		// 매일 주기에 따른 조건 갱신
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    String todayDate = dateFormat.format(new java.util.Date());
		
		SharedPreferences.Editor editor = mConfigPref.edit();
		editor.putString(Constants.CONFIG_TODAY_DATE, todayDate);
		editor.commit();
		
		finish();
	}
}
