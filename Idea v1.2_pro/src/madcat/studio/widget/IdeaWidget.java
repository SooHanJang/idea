package madcat.studio.widget;

import madcat.studio.constants.Constants;
import madcat.studio.idea.MainMenu;
import madcat.studio.idea.R;
import madcat.studio.note.NoteActivity;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.RemoteViews;

public class IdeaWidget extends AppWidgetProvider {
	
	private final String TAG										=	"IdeaWidget";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	private String[] mKeyword										=	new String[3];
	
	private String mReceiveAction;
	private int mKeywordSize;

	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);

		
		mReceiveAction = intent.getAction();
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "Receive Action : " + mReceiveAction);
		}
		
		if(intent.getAction().equals(Constants.ACTION_WIDGET_UPDATE) || intent.getAction().equals(Constants.ACTION_THEME_UPDATE) 
				|| intent.getAction().equals(Constants.ACTION_KEYWORD_REFRESH) || intent.getAction().equals(Constants.ACTION_KEYWORD_NUMBER)) {
			updateWidget(context);
		} else if(intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			Utils.registeDailyAlarm(context);
		}
	}
	
	private void updateWidget(Context context) {
		ComponentName thisAppWidget = new ComponentName(context.getPackageName(), IdeaWidget.class.getName());
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);
		
		onUpdate(context, appWidgetManager, appWidgetIds);
	}
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		int widgetSize = appWidgetIds.length;
		for(int i=0; i < widgetSize; i++) {
			int widgetId = appWidgetIds[i];
			updateAppWidget(context, appWidgetManager, widgetId);
		}
	}
	
	public void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "전달 받은 액션 : " + mReceiveAction);
		}
		
		RemoteViews updateViews = new RemoteViews(context.getPackageName(), R.layout.idea_widget_layout);
		SharedPreferences configPref = context.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);

		if(mReceiveAction != null) {
			if(mReceiveAction.equals(Constants.ACTION_THEME_UPDATE)) {
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "테마 변경 호출");
				}
			
				// 테마 변경 설정
				int themeIndex = configPref.getInt(Constants.CONFIG_THEME_INDEX, 1);
				
				switch(themeIndex) {
					case 1:
						themeIndex = 2;
						break;
					case 2:
						themeIndex = 3;
						break;
					case 3:
						themeIndex = 1;
						break;
				}
				
				SharedPreferences.Editor editor = configPref.edit();
				editor.putInt(Constants.CONFIG_THEME_INDEX, themeIndex);
				editor.commit();
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "현재 선택된 테마 인덱스 : " + themeIndex);
				}
				
				int bgHeadResId = context.getResources().getIdentifier("widget_post_head_" + themeIndex, "drawable", context.getPackageName());
				int bgBodyResId = context.getResources().getIdentifier("widget_post_body_" + themeIndex, "drawable", context.getPackageName());
				
				updateViews.setInt(R.id.widget_post_head, "setBackgroundResource", bgHeadResId);
				updateViews.setInt(R.id.widget_post_body, "setBackgroundResource", bgBodyResId);
			}
		}
			
		if(DEVELOPE_MODE) {
			Log.d(TAG, "키워드 설정 호출");
		}
	
		// 키워드 표시 설정 (표시 개수에 따라 텍스트의 크기를 바꿔줄 수 있는 코드를 추가해야 한다.)
		StringBuilder builder = new StringBuilder();
		mKeywordSize = configPref.getInt(Constants.CONFIG_KEYWORD_NUMBER, 3);
		
		for(int i=0; i < Constants.MAX_KEYWORD_TEXTVIEW; i++) {
			mKeyword[i] = configPref.getString(Constants.CONFIG_KEYWORD + (i+1), "Empty");
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "현재 키워드 : " + mKeyword[i]);
			}
		}
		
		switch(mKeywordSize) {
			case 1:
				builder.append(mKeyword[1]);
				break;
			case 2:
				builder.append(mKeyword[1]);
				builder.append(Constants.DEFAULT_LINE_BREAK);
				builder.append(mKeyword[2]);
				break;
			case 3:
				builder.append(mKeyword[0]);
				builder.append(Constants.DEFAULT_LINE_BREAK);
				builder.append(mKeyword[1]);
				builder.append(Constants.DEFAULT_LINE_BREAK);
				builder.append(mKeyword[2]);
				break;
		}
		
		updateViews.setTextViewText(R.id.widget_post_keyword, builder.toString());
		
		
		
		// 새로고침 설정
		
		Intent refreshIntent = new Intent(context, WidgetRefresh.class);
		refreshIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
		
		updateViews.setOnClickPendingIntent(R.id.widget_post_refresh, 
				PendingIntent.getActivity(context, 0, refreshIntent, PendingIntent.FLAG_CANCEL_CURRENT));
		

		// 노트작성 설정
		String sendKeywordText = Utils.sendKeyword(mKeyword[0], mKeyword[1], mKeyword[2], mKeywordSize);
		
		Intent writeNoteIntent = new Intent(context, NoteActivity.class);
		writeNoteIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		
		writeNoteIntent.putExtra(Constants.PUT_CALLER_ACTIVITY, Constants.CALLER_CREATE_IDEA);
		writeNoteIntent.putExtra(Constants.PUT_SEND_KEYWORD, sendKeywordText);
		
		updateViews.setOnClickPendingIntent(R.id.widget_post_keyword, 
				PendingIntent.getActivity(context, 0, writeNoteIntent, PendingIntent.FLAG_UPDATE_CURRENT));
		
		

		// 메인 메뉴 설정
		
		Intent executeIntent = new Intent(context, MainMenu.class);
		executeIntent.putExtra(Constants.PUT_CALLER_ACTIVITY, IdeaWidget.class.toString());
		executeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		
		updateViews.setOnClickPendingIntent(R.id.widget_post_home, 
				PendingIntent.getActivity(context, 0, executeIntent, PendingIntent.FLAG_UPDATE_CURRENT));

		
		
		// 테마 변경 설정
		
		Intent changeThemeIntent = new Intent(Constants.ACTION_THEME_UPDATE);
		updateViews.setOnClickPendingIntent(R.id.widget_post_change_theme, 
				PendingIntent.getBroadcast(context, 0, changeThemeIntent, PendingIntent.FLAG_UPDATE_CURRENT));
		
		
		appWidgetManager.updateAppWidget(appWidgetId, updateViews);
		
	}
}







