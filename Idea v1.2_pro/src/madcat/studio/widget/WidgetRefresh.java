package madcat.studio.widget;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.idea.R;
import madcat.studio.keyword.Keyword;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class WidgetRefresh extends Activity {
	
	private final String TAG										=	"WidgetRefresh";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private MessagePool mMessagePool;
	
	private String[] mTextKeyword;
	private SharedPreferences mConfigPref;
	
	private ArrayList<String> mTodayKeyword;
	private String[] mSelectedCategory;
	private ArrayList<Keyword>[] mLoadKeywordText;
	private ArrayList<Integer> mLoadRandomIndex;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onCreate 호출");
		}
			
		super.onCreate(savedInstanceState);
		
		mContext = this;
		mMessagePool = (MessagePool)getApplicationContext();
		
		if(!mMessagePool.getNoteWritingFlag()) {
			mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
			
			mTextKeyword = new String[Constants.MAX_KEYWORD_TEXTVIEW];
			
			mLoadRandomIndex = new ArrayList<Integer>();
			mTodayKeyword = new ArrayList<String>();
			
			setKeyword();
		} else {
			Toast.makeText(mContext, getString(R.string.toast_title_cannot_refresh), Toast.LENGTH_SHORT).show();
		}
		
		finish();
	}
	
	private void setKeyword() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "setKeyword 호출");
		}
		
		mSelectedCategory = new String[Constants.MAX_KEYWORD_TEXTVIEW];
		for(int i=0; i < mSelectedCategory.length; i++) {
			mSelectedCategory[i] = mConfigPref.getString(Constants.CONFIG_KEYWORD_CATEGORY + (i+1), Constants.DEFAULT_ALL_CATEGORY);
		}
		
		mLoadKeywordText = new ArrayList[Constants.MAX_KEYWORD_TEXTVIEW];
		for(int i=0; i < mLoadKeywordText.length; i++) {
			mLoadKeywordText[i] = Utils.getKeywordList(mContext, mSelectedCategory[i]);
		}
		
		mLoadRandomIndex.clear();
		mTodayKeyword.clear();
		
		if(mSelectedCategory[0].equals(mSelectedCategory[1]) && mSelectedCategory[0].equals(mSelectedCategory[2])) {	//	Clear
			mLoadRandomIndex = Utils.getKeywordIndexToArray(mLoadRandomIndex, mLoadKeywordText[0].size(), 3);
			
			for(int i=0; i < mLoadRandomIndex.size(); i++) {
				mTodayKeyword.add(mLoadKeywordText[i].get(mLoadRandomIndex.get(i)).getKeyword());
			}
			
		} else if(mSelectedCategory[0].equals(mSelectedCategory[1])) {
			mLoadRandomIndex = Utils.getKeywordIndexToArray(mLoadRandomIndex, mLoadKeywordText[0].size(), 2);
			mLoadRandomIndex.add(Utils.getRandomIndex(mLoadKeywordText[2].size()));			//	3번 키워드 랜덤 값 추가
			
			for(int i=0; i < mLoadRandomIndex.size(); i++) {
				mTodayKeyword.add(mLoadKeywordText[i].get(mLoadRandomIndex.get(i)).getKeyword());
			}
			
		} else if(mSelectedCategory[0].equals(mSelectedCategory[2])) {
			mLoadRandomIndex = Utils.getKeywordIndexToArray(mLoadRandomIndex, mLoadKeywordText[0].size(), 2);
			mLoadRandomIndex.add(1, Utils.getRandomIndex(mLoadKeywordText[1].size()));		//	2번 키워드 랜덤 값 추가
			
			for(int i=0; i < mLoadRandomIndex.size(); i++) {
				mTodayKeyword.add(mLoadKeywordText[i].get(mLoadRandomIndex.get(i)).getKeyword());
			}

		} else if(mSelectedCategory[1].equals(mSelectedCategory[2])) {
			mLoadRandomIndex.add(Utils.getRandomIndex(mLoadKeywordText[0].size()));			//	1번 키워드 랜덤 값 추가
			mLoadRandomIndex = Utils.getKeywordIndexToArray(mLoadRandomIndex, mLoadKeywordText[1].size(), 2);
			
			for(int i=0; i < mLoadRandomIndex.size(); i++) {
				mTodayKeyword.add(mLoadKeywordText[i].get(mLoadRandomIndex.get(i)).getKeyword());
			}
		} else {
			for(int i=0; i < Constants.MAX_KEYWORD_TEXTVIEW; i++) {
				mLoadRandomIndex.add(Utils.getRandomIndex(mLoadKeywordText[i].size()));
				mTodayKeyword.add(mLoadKeywordText[i].get(mLoadRandomIndex.get(i)).getKeyword());
			}
		}
		
		SharedPreferences.Editor editor = mConfigPref.edit();

		for(int i=0; i < mTextKeyword.length; i++) {
			mTextKeyword[i] = mTodayKeyword.get(i);
			editor.putString(Constants.CONFIG_KEYWORD + (i+1), mTodayKeyword.get(i));
		}
		
		editor.commit();
		
		Utils.sendBroadCasting(mContext, Constants.ACTION_KEYWORD_REFRESH);
	}
	
	@Override
	protected void onDestroy() {
		if(DEVELOPE_MODE){
			Log.d(TAG, "onDestroy 호출");
		}
		
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
