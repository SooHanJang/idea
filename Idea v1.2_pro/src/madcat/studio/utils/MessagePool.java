package madcat.studio.utils;

import android.app.Application;

public class MessagePool extends Application {
	
	private int mMadCount;
	private boolean mNoteEmptyFlag;
	private boolean mNoteWritingFlag										=	false;
	
	public boolean getNoteEmptyFlag() {	return mNoteEmptyFlag;	}
	public void setNoteEmptyFlag(boolean noteEmptyFlag) {	this.mNoteEmptyFlag = noteEmptyFlag;	}

	public int getMadCount() {	return mMadCount;	}
	public void setMadCount(int madCount) {	this.mMadCount = madCount;	}
	
	public boolean getNoteWritingFlag() {	return mNoteWritingFlag;	}
	public void setNoteWritingFlag(boolean noteWritingFlag) {	this.mNoteWritingFlag = noteWritingFlag;	}
}
