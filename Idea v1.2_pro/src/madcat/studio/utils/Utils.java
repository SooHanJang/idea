package madcat.studio.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import madcat.studio.challenge.Challenge;
import madcat.studio.constants.Constants;
import madcat.studio.idea.R;
import madcat.studio.keyword.Keyword;
import madcat.studio.note.list.Note;
import madcat.studio.note.list.NoteDayInfo;
import madcat.studio.service.DailyRefreshService;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Utils {
	
	public static final String TAG										=	"Utils";
	public static final boolean DEVELOPE_MODE							=	Constants.DEVELOPE_MODE;
	
	// 테스트 용 시작
	public static int getRandomIndex(int maxSize) {
		Random random = new Random(System.currentTimeMillis());
		return random.nextInt(maxSize);
	}
	
	public static ArrayList<Integer> getKeywordIndexToArray(ArrayList<Integer> items, int keywordSize, int textViewNum) {
		Random random = new Random(System.currentTimeMillis());
		items = setRandomNumberArray(random, items, 0, keywordSize, textViewNum);
		return items;
	}
	
	public static ArrayList<Integer> setRandomNumberArray(Random random, ArrayList<Integer> randArray, int count, int maxNumber, int maxCount) {
		int randNum = 0;
		
		if(count == 0) {
			randNum = random.nextInt(maxNumber);
			randArray.add(randNum);
			count++;
			return setRandomNumberArray(random, randArray, count, maxNumber, maxCount);
		} else if(count > 0 && count < maxCount) {
			randNum = random.nextInt(maxNumber);
			
			for(int i=0; i < count; i++) {
				if(randNum == randArray.get(i)) {
					return setRandomNumberArray(random, randArray, count, maxNumber, maxCount);
				}
			}
			
			randArray.add(randNum);
			count++;
			return setRandomNumberArray(random, randArray, count, maxNumber, maxCount);
		} else {
			return randArray;
		}
	}
	
	// 테스트용 끝
	
	/**
	 * maxNumber 범위 내에서 랜덤 숫자를 얻은 뒤, keywordSize 만큼의 인덱스에 저장하여 반환합니다.
	 * 
	 * @param keywordSize
	 * @param textViewNum
	 * @return
	 */
	public static int[] getKeywordIndex(int keywordSize, int textViewNum) {
		int[] randKeywordNum = new int[textViewNum];
		
		Random random = new Random(System.currentTimeMillis());
		randKeywordNum = setRandomNumber(random, randKeywordNum, 0, keywordSize);
		
		return randKeywordNum;
	}
	
	/**
	 * 재귀 호출을 이용하여, maxNumber 범위 내에서 랜덤 숫자를 얻은 뒤, randArray 에 저장하여 반환합니다. 
	 * Utils.getKeywordIndex() 에서 호출되는 메소드입니다.
	 * 
	 * @param random
	 * @param randArray
	 * @param count
	 * @param maxNumber
	 * @return
	 */
	
	public static int[] setRandomNumber(Random random, int[] randArray, int count, int maxNumber) {
		int randNum = 0;
		
		if(count == 0) {
			randNum = random.nextInt(maxNumber);
			randArray[count] = randNum;
			count++;
			return setRandomNumber(random, randArray, count, maxNumber);
		} else if(count > 0 && count < randArray.length) {
			randNum = random.nextInt(maxNumber);
			
			for(int i=0; i < count; i++) {
				if(randNum == randArray[i]) {
					return setRandomNumber(random, randArray, count, maxNumber);
				}
			}
			
			randArray[count] = randNum;
			count++;
			return setRandomNumber(random, randArray, count, maxNumber);
		} else {
			return randArray;
		}
	}
	
	/**
	 * Object 타입을 byte 타입으로 Serialize 합니다.
	 * 
	 * @param object
	 * @return
	 */
	
	public static byte[] serializeObject(Object object) {
//		Log.d(TAG, "직렬화 시작");
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		try {
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(object);
			oos.close();
			
			byte[] buf = bos.toByteArray();
			
//			Log.d(TAG, "직렬화 성공");
			
			return buf;
		} catch (IOException e) {
			e.printStackTrace();
			
//			Log.d(TAG, "직렬화 실패");
			
			return null;
		}
	}
	
	/**
	 * byte 타입을 해당 Object 로 deSerialize 합니다. 
	 * 
	 * @param b
	 * @return
	 */
	
	public static Object deSerializeObject(byte[] b) {
		try {
			ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(b));
			Object object = ois.readObject();
			
			ois.close();
			
			return object;
		} catch(IOException ie) {
			return null;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 해당 date 에 따른 데이터를 데이터베이스에서 지웁니다.
	 * (date 는 반드시, java.util.Date() 를 이용, 포맷은 Constants.DATE_FORMAT_IN_TIME 형식으로 가져와야 합니다.)
	 * 
	 * @param context
	 * @param date
	 */

	public static void deleteNote(Context context, String date, String keyword) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);

		sdb.delete(Constants.TABLE_IDEA_NOTE, Constants.NOTE_COLUMN_DATE + "='" + date + "' AND "
				+ Constants.NOTE_COLUMN_KEYWORD + "='" + keyword + "'", null);
		
		sdb.close();
	}
	
	// 백업 시에는 먼저 db 경로에서 db 파일을 복사해 온 다음, db 와 같이 압축을 한 뒤, db 파일은 삭제한다.
	public static boolean dataBackUp(Context context) throws Exception {
		int ZIP_COMPRESS_LEVEL										=	8;
		
		if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
		
			// 파일명을 날짜로 설정
			Date nowTime = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
			
			// 파일 시스템 컨트롤에 사용할 변수
			String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();	//	SD 카드 경로
			File dbFile = context.getDatabasePath(Constants.DATABASE_NAME);						//	원본 데이터베이스가 저장된 경로
			
			String targetPath = sdCardPath + Constants.IDEA_NOTE_BACKUP_PATH;					//	압축 파일을 저장할 경로
			String zipOutPut = "AllThatIdea_" + dateFormat.format(nowTime) + ".zip";				//	압축할 파일명
			String dbOutPut = Constants.DATABASE_NAME;											//	데이터베이스 파일명
	
			// 백업할 경로가 존재하는지를 판단하여, 존재하지 않으면 경로를 생성한다.
			File dir = null;
			dir = new File(targetPath);
			if(!dir.exists()) {
				dir.mkdirs();
			}
			
			// 압축 대상이 될 파일 경로를 설정한다.
			// 데이터베이스 파일을 원본 이미지가 있는 폴더에 먼저 복사를 한다.
			databaseCopy(dbFile, targetPath, dbOutPut);
				
			// 데이터베이스 복사가 완료되면, 파일 백업 시작한다.
			FileOutputStream fos = null;
			BufferedOutputStream bos = null;
			ZipOutputStream zos = null;
			
			try {
				fos = new FileOutputStream(targetPath + "/" + zipOutPut);					
				bos = new BufferedOutputStream(fos);										
				zos = new ZipOutputStream(bos);
				zos.setLevel(ZIP_COMPRESS_LEVEL);
				zipEntry(dbFile, targetPath, zos);
				zos.finish();
			} finally {
				if(zos != null) {
					zos.close();
				}
				if(bos != null) {
					bos.close();
				}
				if(fos != null) {
					fos.close();
				}
			}
			
			// 백업이 끝나면 복사한 DB 파일은 삭제한다.
			File dbDelFile = new File(targetPath + "/" + dbOutPut);
			dbDelFile.delete();
			
			return true;
		} else {
			Toast.makeText(context, "SD 카드 상태를 확인해주세요.", Toast.LENGTH_SHORT).show();
			return false;
		}
	} 
	
	/**
	 * 해당 dbFile 을 targetPath 경로에 output 형태로 복사합니다.
	 * 
	 * @param dbFile
	 * @param targetPath
	 * @param output
	 * @throws IOException
	 */
	
	public static void databaseCopy(File dbFile, String targetPath, String output) throws IOException {
		if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			FileChannel inChannel = new FileInputStream(dbFile).getChannel();
			FileChannel outChannel = new FileOutputStream(targetPath + "/" + output).getChannel();
			
			try {
				int maxCount = (64 * 1024 * 1024) - (32 * 1024);
				long size = inChannel.size();
				long position = 0;
				
				while(position < size) {
					position += inChannel.transferTo(position, maxCount, outChannel);
				}
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} finally {
				if(inChannel != null) {
					inChannel.close();
				}
				
				if(outChannel != null) {
					outChannel.close();
				}
			}
		}
	}
	
	/**
	 * 압축할 sourceFile 과 압축 경로인 sourcePath 에 맞는 zipEntry List를 생성한 뒤, 설정된 zos 경로에 압축 파일을 생성한다.
	 * 
	 * @param sourceFile
	 * @param sourcePath
	 * @param zos
	 * @throws Exception
	 */
	
	public static void zipEntry(File sourceFile, String sourcePath, ZipOutputStream zos) throws Exception {
		int BUFFER_SIZE = 1024 * 2;
		
		if(sourceFile.isDirectory()) {
			if(sourceFile.getName().equalsIgnoreCase(".metadata")) {
				return;
			}
			
			File[] fileArray = sourceFile.listFiles();
			for(int i=0; i < fileArray.length; i++) {
				zipEntry(fileArray[i], sourcePath, zos);
			}
		} else {
			BufferedInputStream bis = null;
			
			try {
				
				// 1-1. 일반 파일인 경우는 다음과 SD 카드의 경로와 외장 파일이 들어 있는 경로를 파싱하여 압축할 파일 명을 뽑아낸다.
//				String sFilePath = sourceFile.getPath();
//				String zipEntryName = sFilePath.substring(sourcePath.length() + 1, sFilePath.length());
				
				// 1-2. 데이터베이스만을 복사하여 압축할 때는 하단의 처럼 압축할 파일 명을 직접 지정
				String zipEntryName = Constants.DATABASE_NAME;
				
				bis = new BufferedInputStream(new FileInputStream(sourceFile));
				ZipEntry zentry = new ZipEntry(zipEntryName);
				zos.putNextEntry(zentry);
				
				byte[] buffer = new byte[BUFFER_SIZE];
				int cnt = 0;
				while((cnt = bis.read(buffer, 0, BUFFER_SIZE)) != -1) {
					zos.write(buffer, 0, cnt);
				}
				zos.closeEntry();
			} finally {
				if(bis != null) {
					bis.close();
				}
			}
		}
	}
	
	/**
	 * Zip 파일의 압축을 푼다.
	 * 
	 * @param zipFile - 압축 풀 Zip 파일
	 * @param targetDir	- 압축 푼 파일이 들어갈 디렉토리
	 * @param fileNameToLowerCase - 파일명을 소문자로 바꿀지에 대한 여부
	 * @throws Exception
	 */
	
	public static void unZip(File zipFile, File targetDir, boolean fileNameToLowerCase) throws Exception {
		FileInputStream fis = null;
		ZipInputStream zis = null;
		ZipEntry zentry = null;
		
		try {
			fis = new FileInputStream(zipFile);
			zis = new ZipInputStream(fis);
			
			while((zentry = zis.getNextEntry()) != null) {
				String fileNameToUnZip = zentry.getName();
				if(fileNameToLowerCase) {
					fileNameToUnZip = fileNameToUnZip.toLowerCase();
				}
				
				File targetFile = new File(targetDir, fileNameToUnZip);
				
				if(zentry.isDirectory()) {		//	디렉토리인 경우
					targetFile.mkdirs();		//	디렉토리를 생성한다.
				} else {						//	파일인 경우
					unZipEntry(zis, targetFile);
				}
			}
		} finally {
			if(zis != null) {
				zis.close();
			}
			
			if(fis != null) {
				fis.close();
			}
		}
	}
	
	public static File unZipEntry(ZipInputStream zis, File targetFile) throws Exception {
//		Log.d(TAG, "unZipEntry 호출");
		int BUFFER_SIZE									=	1024 * 2;
		
		FileOutputStream fos = null;
		
		try {
			fos = new FileOutputStream(targetFile);
			
			byte[] buffer = new byte[BUFFER_SIZE];
			int len = 0;
			
			while((len = zis.read(buffer)) != -1) {
				fos.write(buffer, 0, len);
			}
		} finally {
			if(fos != null) {
				fos.close();
			}
		}
		
//		Log.d(TAG, "리턴할 targetFile : " + targetFile);
		
		return targetFile;
	}
	
	/**
	 * 해당 List 안에 AllThatIdea 파일명이 있는 지를 체크하여 필터링 합니다.
	 * 
	 * @param list
	 * @return
	 */
	
	public static ArrayList<String> filterFileName(ArrayList<String> list) {
		ArrayList<String> fileFilterArray = new ArrayList<String>();
		
		for(int i=0; i < list.size(); i++) {
			String[] splitFileName = list.get(i).split("_");
			
			if(splitFileName.length == 7 && splitFileName[0].equals("AllThatIdea")) {
				fileFilterArray.add(list.get(i));
			}
		}
		
		return fileFilterArray;
	}
	
	public static String[] splitFileName(Context context, ArrayList<String> list) {
		String[] splitFileArray = new String[list.size()];
		
		for(int i=0; i < list.size(); i++) {
			String[] splitFileName = list.get(i).split("_");
			
			splitFileArray[i] = splitFileName[1] + "." + splitFileName[2] + "." + splitFileName[3] + " " +
								splitFileName[4] + context.getString(R.string.suffix_date_hour) + 
								splitFileName[5] + context.getString(R.string.suffix_date_min) + context.getString(R.string.suffix_data);
		}
		
		return splitFileArray;
	}
	
	/**
	 * sourceFileName으로 넘어온 파일명에 읽어들여 정해진 sourcePath 에 복구합니다.
	 * 
	 * @param context
	 * @param sourceFileName - 복구할 파일명
	 * @return
	 */
	public static boolean dataRestore(Context context, String sourceFileName) {
		String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		String sourcePath = sdCardPath + Constants.IDEA_NOTE_BACKUP_PATH + "/" + sourceFileName;
		
		File sourceFile = new File(sourcePath);
		File dbPath = context.getDatabasePath(Constants.DATABASE_NAME);
		File targetPath = new File(dbPath.getParent());
		
		try {
			Utils.unZip(sourceFile, targetPath, false);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/**
	 * Keyword Table 에 저장된 키워드를 가져옵니다.
	 * @return items;
	 */
	
	public static ArrayList<Keyword> getKeywordList(Context context, String category) {
		ArrayList<Keyword> items = new ArrayList<Keyword>();

		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String keywordListSQL;
		
		if(category.equals(Constants.DEFAULT_ALL_CATEGORY)) {
			keywordListSQL = "SELECT * FROM " + Constants.TABLE_IDEA_KEYWORD;
		} else {
			keywordListSQL = "SELECT * FROM " + Constants.TABLE_IDEA_KEYWORD + 
							 " WHERE " + Constants.KEYWORD_COLUMN_CATEGORY + " = '" + category + "';";
		}
		
		Cursor cursor = sdb.rawQuery(keywordListSQL, null);

		while(cursor.moveToNext()) {
			Keyword keyword = new Keyword(cursor.getString(1), cursor.getString(2));
			items.add(keyword);
		}
		
		cursor.close();
		sdb.close();
		
		return items;
	}
	
	/**
	 * Category Table 에 저장된 카테고리를 ArrayList<String> 타입으로 리턴합니다.
	 * @return items;
	 */
	
	public static ArrayList<String> getCategoryList(Context context) {
		ArrayList<String> items = new ArrayList<String>();

		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String matchLunarDate = "SELECT * FROM " + Constants.TABLE_IDEA_CATEGORY;
		
		Cursor cursor = sdb.rawQuery(matchLunarDate, null);

		while(cursor.moveToNext()) {
			if(!cursor.getString(1).equals(Constants.DEFAULT_ALL_CATEGORY)) { 
				items.add(cursor.getString(1));
			}
		}
		
		cursor.close();
		sdb.close();
		
		return items;
	}
	
	/**
	 * Category Table 에 저장된 카테고리를 String 배열 타입으로 리턴합니다.
	 * @return items;
	 */
	
	public static String[] getCategoryListToString(Context context) {
		String[] items;

		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String matchLunarDate = "SELECT * FROM " + Constants.TABLE_IDEA_CATEGORY;
		
		Cursor cursor = sdb.rawQuery(matchLunarDate, null);
		items = new String[cursor.getCount()];
		int i=0;

		while(cursor.moveToNext()) {
			items[i] = cursor.getString(1);
			i++;
		}
		
		cursor.close();
		sdb.close();
		
		return items;
	}
	
	/**
	 * 해당 키워드가 데이터베이스에 중복이 존재하는지 여부를 체크합니다.
	 * (반드시, 호출 하는 곳에서 db.close() 를 호출해야 합니다.)
	 * 
	 * @param sdb - 데이터베이스 객체
	 * @param keyword - 중복 검사할 키워드
	 * @return
	 */
	public static boolean duplicationKeywordCheck(SQLiteDatabase sdb, String keyword) {		// true : 중복 존재, false : 중복 미존재
		// 중복 키워드 값이 있는지를 확인하기 위한 쿼리
		String compareSql = "SELECT * FROM " + Constants.TABLE_IDEA_KEYWORD +
							" WHERE " + Constants.KEYWORD_COLUMN_KEYWORD + " = '" + keyword + "';";
		
		// 중복 키워드를 확인하기 위한 메소드를 실행한다.
		Cursor cursor = sdb.rawQuery(compareSql, null);
		
		if(cursor.getCount() == 0) {
			cursor.close();
			return false;
		} else {
			cursor.close();
			return true;
		}
	}
	
	/**
	 * 해당 카테고리가 데이터베이스에 중복이 존재하는지 여부를 체크합니다.
	 * (반드시, 호출 하는 곳에서 db.close() 를 호출해야 합니다.)
	 * 
	 * @param sdb - 데이터베이스 객체
	 * @param category - 중복 검사할 카테고리
	 * @return
	 */
	public static boolean duplicationCategoryCheck(SQLiteDatabase sdb, String category) {
		String compareSql = "SELECT * FROM " + Constants.TABLE_IDEA_CATEGORY +
							" WHERE " + Constants.CATEGORY_COLUMN_CATEGORY + " = '" + category + "';";
		Cursor cursor = sdb.rawQuery(compareSql, null);
		
		if(cursor.getCount() == 0) {
			cursor.close();
			return false;
		} else {
			cursor.close();
			return true;
		}
	}
	
	/**
	 * 사용자가 작성한 노트 리스트를 ArrayList<Note> 형태로 전부 가져옵니다.
	 * 
	 * @param context
	 * @return
	 */
	
	public static ArrayList<Note> getNoteList(Context context, String date) {
		ArrayList<Note> items = new ArrayList<Note>();

		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		String noteListSQL = "SELECT * FROM " + Constants.TABLE_IDEA_NOTE +
							 " WHERE " + Constants.NOTE_COLUMN_DATE + "='" + date +"'";
		
		Cursor cursorNoteLst = sdb.rawQuery(noteListSQL, null);
		while(cursorNoteLst.moveToNext()) {
			Note note = new Note();
			note.setNoteDate(cursorNoteLst.getString(1));
			note.setNoteKeyword(cursorNoteLst.getString(2));
			note.setNoteBytes(cursorNoteLst.getBlob(3));
			
			items.add(note);
		}
		
		cursorNoteLst.close();
		sdb.close();
		
		return items;
	}

	public static ArrayList<String> getNoteGroupList(Context context) {
		ArrayList<String> items = new ArrayList<String>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    String todayDate = dateFormat.format(new java.util.Date());
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		String noteGroupSQL = "SELECT * FROM " + Constants.TABLE_IDEA_NOTE + 
							  " GROUP BY " + Constants.NOTE_COLUMN_DATE;
		
		Cursor cursorNoteGroup = sdb.rawQuery(noteGroupSQL, null);
		while(cursorNoteGroup.moveToNext()) {
			if(cursorNoteGroup.getString(1).equals(todayDate)) {
				items.add(0, cursorNoteGroup.getString(1));
			} else {
				items.add(cursorNoteGroup.getString(1));
			}
		}
		
		cursorNoteGroup.close();
		sdb.close();
		
		return items;
		
	}
	
	/**
	 * 해당 달에 쓰여진 노트 정보를 ArrayList<NoteDayInfo> 형태로 가져옵니다.
	 * 
	 * @param context
	 * @param year
	 * @param month
	 * @return
	 */
	public static ArrayList<NoteDayInfo> getWroteNoteToMonth(Context context, String year, String month) {
		ArrayList<NoteDayInfo> items = new ArrayList<NoteDayInfo>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		String getMarkSql = "SELECT * FROM " + Constants.TABLE_IDEA_NOTE + 
							" WHERE SUBSTR(" + Constants.NOTE_COLUMN_DATE + ", 1, 4) = '" + year + "' AND" +
							" SUBSTR(" + Constants.NOTE_COLUMN_DATE + ", 6, 2) = '" + month + "'" +
							" GROUP BY " + Constants.NOTE_COLUMN_DATE;
		Cursor getMarkCursor = sdb.rawQuery(getMarkSql, null);

		while(getMarkCursor.moveToNext()) {
		
			String noteDate = getMarkCursor.getString(1);
			String noteKeyword = getMarkCursor.getString(2);

			NoteDayInfo noteDayInfo = new NoteDayInfo();
			noteDayInfo.setYear(noteDate.substring(0, 4));
			noteDayInfo.setMonth(noteDate.substring(5, 7));
			noteDayInfo.setDay(noteDate.substring(8, 10));
			noteDayInfo.setDate(noteDate);
			
			items.add(noteDayInfo);
		}
		
		getMarkCursor.close();
		sdb.close();
		
		return items;
	}
	
	public static ArrayList<Challenge> getChallengeList(Context context) {
		ArrayList<Challenge> items = new ArrayList<Challenge>();
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		String challengeList = "SELECT * FROM " + Constants.TABLE_IDEA_CHALLENGE;
		Cursor cursor = sdb.rawQuery(challengeList, null);
		
		while(cursor.moveToNext()) {
			Challenge challenge = new Challenge();
			challenge.setTitle(cursor.getString(1));
			challenge.setText(cursor.getString(2));
			challenge.setDiffcult(cursor.getFloat(3));
			challenge.setIconId(context.getResources().getIdentifier(
					Constants.PREFIX_CHALLENGE_ICON + cursor.getInt(4), "drawable", context.getPackageName()));
			challenge.setSucceed(cursor.getInt(5));
			
			items.add(challenge);
		}
		
		cursor.close();
		sdb.close();
		
		return items;
	}
	
	public static void getChallengeState(Context context) {
		SharedPreferences pref = context.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);

//		Log.d(TAG, "1. 초급 기획자 : " + pref.getBoolean(Constants.COMPLETE_CHALLENGE_BEGINNER_PLANNER, false));
//		Log.d(TAG, "2. 중급 기획자 : " + pref.getBoolean(Constants.COMPLETE_CHALLENGE_INTERMEDIATE_PLANNER, false));
//		Log.d(TAG, "3. 고급 기획자 : " + pref.getBoolean(Constants.COMPLETE_CHALLENGE_ADVANCED_PLANNER, false));
//		Log.d(TAG, "4. 근면성실 : " + pref.getBoolean(Constants.COMPLETE_CHALLENGE_HARD_WORK, false));
//		Log.d(TAG, "5. 지성이면 감천 : " + pref.getBoolean(Constants.COMPLETE_CHALLENGE_NO_PAIN_NO_GAIN, false));
//		Log.d(TAG, "6. 아이디어 뱅크 : " + pref.getBoolean(Constants.COMPLETE_CHALLENGE_IDEA_BANK, false));
//		Log.d(TAG, "7. 섞어야 제 맛 : " + pref.getBoolean(Constants.COMPLETE_CHALLENGE_SHAKE, false));
//		Log.d(TAG, "8. 메뉴얼 정복자 : " + pref.getBoolean(Constants.COMPLETE_CHALLENGE_MANUAL_CONQUEROR, false));
//		Log.d(TAG, "9. 질보단 양 : " + pref.getBoolean(Constants.COMPLETE_CHALLENGE_AMOUNT_OF_QUALITY, false));
//		Log.d(TAG, "10. 정신나간 녀석 : " + pref.getBoolean(Constants.COMPLETE_CHALLENGE_MAD, false));
//		Log.d(TAG, "11. 감사합니다. 고객님 : " + pref.getBoolean(Constants.COMPLETE_CHALLENGE_THANKS, false));
//		Log.d(TAG, "12. 시간과 정신의 방 : " + pref.getBoolean(Constants.COMPLETE_CHALLENGE_TIME_AND_ROOM_OF_THE_MIND, false));
//		Log.d(TAG, "13. 철야근무  : " + pref.getBoolean(Constants.COMPLETE_CHALLENGE_WORK_ALL_NIGHT, false));
	}
	
	public static void updateChallengeState(Context context, String title) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		ContentValues updateValues = new ContentValues();
		updateValues.put(Constants.CHALLENGE_COLUMN_SUCCEED, 1);
		sdb.update(Constants.TABLE_IDEA_CHALLENGE, updateValues, 
				Constants.CHALLENGE_COLUMN_TITLE + " = '" + title + "'", null);
		sdb.close();
	}

	
	/**
	 * 두 날짜간의 차이를 구하여 int 형으로 반환한다.
	 * 두 날짜간의 차이를 구할 때, 반드시 날짜 타입은 yyyyMMdd 로 지정하도록 한다.
	 * 
	 * @param formatter (객체를 생성해서 보낸다.)
	 * @param begin 
	 * @param end
	 * @return
	 */
	public static int gapOfDate(SimpleDateFormat formatter, String begin, String end) {
//		Log.d(TAG, "gapOfDate 실행");
		
		try {
			Date beginDate = formatter.parse(begin.replaceAll("-", ""));
			Date endDate = formatter.parse(end.replaceAll("-", ""));
			
			long gap = endDate.getTime() - beginDate.getTime();
			int gapDays = (int) (gap / (24 * 60 * 60 * 1000));
			
			return gapDays;
			
		} catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * 부모 View를 기준으로 자식 View를 재귀 호출하면서 할당된 이미지를 해제한다.
	 * @param root
	 */
	
	public static void recursiveRecycle(View root) {
		if(root == null) {
			return;
		}
		
		if(root instanceof ViewGroup) {
			ViewGroup group = (ViewGroup)root;
			int count = group.getChildCount();
			
			for(int i=0; i < count; i++) {
				recursiveRecycle(group.getChildAt(i));
			}
			
			if(!(root instanceof AdapterView)) {
				group.removeAllViews();
			}
		}
		
		if(root instanceof ImageView) {
			((ImageView)root).setImageDrawable(null);
		}
		
		root.setBackgroundDrawable(null);
		
		root = null;
		
		return;
	}
	
	public static float setTextViewTextSize(TextView text_view, int text_view_width, String content) {
		String split_text = content;
		float textviewwidth = text_view_width;
		Paint paint = text_view.getPaint();
		int break_idx = 0;
		int split_length = 0;
		
		while(split_text.length() > 0) {
			break_idx = paint.breakText(split_text, true, textviewwidth, null);
			
			if(split_text.length() <= break_idx) {
				break;
			}
			
			split_text = split_text.substring(break_idx);
			split_length = split_text.toString().trim().length();
		}
		
		return split_length > 0 ? (float) Math.ceil((split_text.toString().trim().length() / 1.5 + 0.5)) : 0;
	}
	
	public static void sendBroadCasting(Context context, String action) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "sendBroadCasting 호출");
		}
		
		Intent intent = new Intent(action);
		context.sendBroadcast(intent);
	}
	
	// Intent 로 보낼 키워드 String 을 생성
	public static String sendKeyword(String keyword_1, String keyword_2, String keyword_3, int displaySize) {
		StringBuffer sendBuffer = new StringBuffer();
		
		switch(displaySize) {
		case 1:
			sendBuffer.append("[" + keyword_2 + "]");
			break;
		case 2:
			sendBuffer.append("[" + keyword_2 + "], ");
			sendBuffer.append("[" + keyword_3 + "]");
			break;
		case 3:
			sendBuffer.append("[" + keyword_1 + "], ");
			sendBuffer.append("[" + keyword_2 + "], ");
			sendBuffer.append("[" + keyword_3 + "]");
			
			break;
		}
		
		return sendBuffer.toString();
	}
	
	/**
	 * 매일 주기로 업데이트를 실행하는 Alarm 을 등록한다.
	 */
	
	public static void registeDailyAlarm(Context context) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "알람을 등록합니다.");
		}
		
		Intent refreshIntent = new Intent(context, DailyRefreshService.class);
		PendingIntent pendingIntent 
				=	PendingIntent.getActivity(context, 0, refreshIntent, 0);
		
		AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		
		Calendar calendar = Calendar.getInstance();
		Date date = new Date();
		date.setHours(0);
		date.setMinutes(1);
		date.setSeconds(0);
		calendar.setTime(date);
		
		am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
	}
}









