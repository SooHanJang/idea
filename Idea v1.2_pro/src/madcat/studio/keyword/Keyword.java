package madcat.studio.keyword;

public class Keyword {

	private String mKeyword, mCategory;
	
	public Keyword(String category, String keyword) {
		this.mCategory = category;
		this.mKeyword = keyword;
	}
	
	public String getKeyword() {
		return mKeyword;
	}
	
	public String getCategory() {
		return mCategory;
	}
}
