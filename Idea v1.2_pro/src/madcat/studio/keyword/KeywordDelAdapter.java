package madcat.studio.keyword;

import java.util.ArrayList;

import madcat.studio.idea.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class KeywordDelAdapter extends ArrayAdapter<Keyword> {
	
	private Context mContext;
	private ArrayList<Keyword> mItems;
	
	private ArrayList<Keyword> mDelItems;
	private ArrayList<Integer> mListItems;
	
	private LayoutInflater mInflater;
	private int mResId;
	
	public KeywordDelAdapter(Context context, int resId, ArrayList<Keyword> items) {
		super(context, resId, items);
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		this.mDelItems = new ArrayList<Keyword>();
		this.mListItems = new ArrayList<Integer>();
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		
		if(convertView == null) {
			convertView = mInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			holder.linearLayout = (LinearLayout)convertView.findViewById(R.id.keyword_list_del_layout);
			holder.textKeyword = (TextView)convertView.findViewById(R.id.keyword_list_del_row_name);
			holder.textCategory = (TextView)convertView.findViewById(R.id.keyword_list_del_row_category);
			holder.checkKeyword = (CheckBox)convertView.findViewById(R.id.keyword_list_del_row_check);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder)convertView.getTag();
		}
		
		holder.linearLayout.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				holder.checkKeyword.setChecked(!holder.checkKeyword.isChecked());
			}
		});
		
		holder.textKeyword.setText(mItems.get(position).getKeyword());
		holder.textCategory.setText("[" + mItems.get(position).getCategory() + "]");
		
		holder.checkKeyword.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					for(int i=0; i < mListItems.size(); i++) {
						if(mListItems.get(i) == position) {
							return;
						}
					}
//					Log.d(TAG, "checkPosition : " + checkPosition + ", position : " + position);
					mListItems.add(position);
					mDelItems.add(mItems.get(position));
//					mDelItems.add(mItems.get(position));
				} else {
					for(int i=0; i < mListItems.size(); i++) {
						if(mListItems.get(i) == position) {
							mListItems.remove(i);
							mDelItems.remove(mItems.get(position));
							break;
						}
					}
//					mDelItems.remove(mItems.get(position));
				}
			}
		});
		
		boolean reChecked = false;
		for(int i=0; i < mListItems.size(); i++) {
			if(mListItems.get(i) == position) {
				holder.checkKeyword.setChecked(true);
				reChecked = true;
				break;
			}
		}
		
		if(!reChecked) {
			holder.checkKeyword.setChecked(false);
		}
		
		return convertView;
	}
	
	public class ViewHolder {
		LinearLayout linearLayout;
		TextView textKeyword;
		TextView textCategory;
		CheckBox checkKeyword;
	}
	
	public ArrayList<Keyword> getCheckedList() {
		return mDelItems;
	}
}
