package madcat.studio.keyword;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.dialogs.ConfigConfirmDialog;
import madcat.studio.dialogs.KeywordDialog;
import madcat.studio.idea.R;
import madcat.studio.utils.Utils;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

public class KeywordList extends ListActivity implements OnItemSelectedListener {
	
//	private final String TAG										=	"KeywordList";
	
	private final int KEYWORD_LIST_FLAG								=	1;
	private final int KEYWORD_DEL_FLAG								=	2;
	private final int KEYWORD_MOVE_FLAG								=	3;
	
	private final int MODIFY_KEYWORD								=	1;
	private final int DELETE_KEYWORD								=	2;
	
	private int mCtrlFlag											=	KEYWORD_LIST_FLAG;
	private Context mContext;
	private ArrayList<Keyword> mItems;
	private KeywordListAdapter mListAdapter;
	private LinearLayout mDynamicLayout;
	private Spinner mSpinCategory;
	
	private String mSelectedCategory								=	Constants.DEFAULT_ALL_CATEGORY;
	private String mChangedCategory;
	private String[] mCategoryList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.idea_keyword_list);
		
		mContext = this;
		
		// Resource Id 설정
		mDynamicLayout = (LinearLayout)findViewById(R.id.dynamic_del_area);
		mSpinCategory = (Spinner)findViewById(R.id.keyword_spin_category);

		// 사용자가 등록한 키워드를 DB 에서 불러온다.
		mItems = new ArrayList<Keyword>();
		mItems = Utils.getKeywordList(mContext, mSelectedCategory);
		
		// Spinner 에 표시할 카테고리를 불러온다.
		mCategoryList = Utils.getCategoryListToString(mContext);
		
		// Spinner 설정
		ArrayAdapter<String> spinAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, mCategoryList);
		spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinCategory.setAdapter(spinAdapter);
		mSpinCategory.setOnItemSelectedListener(this);
		
		// 유저 키워드 데이터가 없다면, 키워드를 추가하라는 토스트를 출력한다.
		if(mItems.isEmpty()) {
			Toast.makeText(mContext, getString(R.string.toast_title_empty_keyword), Toast.LENGTH_SHORT).show();
		}
		
		mListAdapter = new KeywordListAdapter(mContext, R.layout.idea_keyword_list_row, mItems);
		setListAdapter(mListAdapter);
		
		registerForContextMenu(getListView());
		
		// 키워드 관리 도움말 토스트 설정
		Toast.makeText(mContext, getString(R.string.toast_title_help_manage_keyword), Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		
		menu.setHeaderTitle(getString(R.string.dialog_title_manage_keyword));
		menu.add(0, MODIFY_KEYWORD, Menu.NONE, getString(R.string.menu_title_keyword_modify));
		menu.add(0, DELETE_KEYWORD, Menu.NONE, getString(R.string.menu_title_keyword_del));
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo menuInfo;
		final int index;
		
		switch(item.getItemId()) {
			case MODIFY_KEYWORD:
				menuInfo = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
				index = menuInfo.position;
				Keyword modifyKeyword = mItems.get(index);
				
				final KeywordDialog modifyKeywordDialog = new KeywordDialog(mContext, Constants.DIALOG_KEYWORD_MODIFY, modifyKeyword);
				modifyKeywordDialog.show();
				
				modifyKeywordDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(modifyKeywordDialog.getDialogFlag()) {
							refreshList();
						}
					}
				});
				
				return true;
			case DELETE_KEYWORD:
				menuInfo = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
				index = menuInfo.position;
				
				final ConfigConfirmDialog delConfirmDialog = new ConfigConfirmDialog(mContext, getString(R.string.dialog_title_delete));
				delConfirmDialog.show();
				
				delConfirmDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(delConfirmDialog.getDialogStateFlag()) {
							Keyword deleteKeyword = mItems.get(index);
							
							// 키워드를 데이터베이스에서 삭제합니다.
							SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
							sdb.delete(Constants.TABLE_IDEA_KEYWORD, 
									Constants.KEYWORD_COLUMN_KEYWORD + " = '" + deleteKeyword.getKeyword() + "' AND " +
									Constants.KEYWORD_COLUMN_CATEGORY + " = '" + deleteKeyword.getCategory() + "'", null);
							sdb.close();
							
							refreshList();
						}
					}
				});
				return true;
			default:
				return false;
		}
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// 누를때마다 메뉴가 Infalter 되는 것을 막기 위해, 표시된 메뉴들을 먼저 삭제한다.
		if(mCtrlFlag == KEYWORD_LIST_FLAG) {
			menu.removeItem(R.id.menu_keyword_add);
			menu.removeItem(R.id.menu_keyword_del);
			menu.removeItem(R.id.menu_keyword_set_category);
			
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.keyword_list_menu, menu);
			
			return super.onPrepareOptionsMenu(menu);
		} 
		
		return false;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		
		switch(item.getItemId()) {
			case R.id.menu_keyword_add:
				mCtrlFlag = KEYWORD_LIST_FLAG;
				
				final KeywordDialog addKeywordDialog = new KeywordDialog(mContext, Constants.DIALOG_KEYWORD_ADD);
				addKeywordDialog.show();
				
				addKeywordDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(addKeywordDialog.getDialogFlag()) {
							refreshList();
						}
					}
				});
				break;
			case R.id.menu_keyword_del:
				mCtrlFlag = KEYWORD_DEL_FLAG;
				
				final KeywordDelAdapter delAdapter = new KeywordDelAdapter(mContext, R.layout.idea_keyword_list_del_row, mItems);
				setListAdapter(delAdapter);
				
				// 동적으로 키워드를 삭제할 수 있는 버튼을 추가
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
						 														 ViewGroup.LayoutParams.FILL_PARENT,
						 														 1.0F);
				Button btnDelOk = new Button(mContext);
				Button btnDelCancel = new Button(mContext);
				
				btnDelOk.setText(getString(R.string.btn_title_del_ok));
				btnDelCancel.setText(getString(R.string.btn_title_del_cancel));
				
				btnDelOk.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						if(delAdapter.getCheckedList().size() != 0) {
							final ConfigConfirmDialog deleteDialog = new ConfigConfirmDialog(mContext, getString(R.string.dialog_title_delete));
							deleteDialog.show();
							
							deleteDialog.setOnDismissListener(new OnDismissListener() {
								public void onDismiss(DialogInterface dialog) {
									if(deleteDialog.getDialogStateFlag()) {
										SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
										
										for(int i=0; i < delAdapter.getCheckedList().size(); i++) {
											sdb.delete(Constants.TABLE_IDEA_KEYWORD, 
													Constants.KEYWORD_COLUMN_KEYWORD + " = '" + delAdapter.getCheckedList().get(i).getKeyword() + "' AND " +
													Constants.KEYWORD_COLUMN_CATEGORY + " = '" + delAdapter.getCheckedList().get(i).getCategory() + "'", null);
										}
										
										sdb.close();
										
										mDynamicLayout.removeAllViews();
										refreshList();
									}
								}
							});
						} 
					}
				});
				
				btnDelCancel.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						mCtrlFlag = KEYWORD_LIST_FLAG;
						mDynamicLayout.removeAllViews();
						setListAdapter(mListAdapter);
					}
				});

				// 중복 표시 방지를 위해 먼저 버튼을 삭제한다.
				mDynamicLayout.removeAllViews();
				
				mDynamicLayout.addView(btnDelOk, params);
				mDynamicLayout.addView(btnDelCancel, params);
				
				break;
			case R.id.menu_keyword_set_category:
				mCtrlFlag = KEYWORD_MOVE_FLAG;
				
				final KeywordDelAdapter categoryAdapter = new KeywordDelAdapter(mContext, R.layout.idea_keyword_list_del_row, mItems);
				setListAdapter(categoryAdapter);
				
				// 동적으로 키워드 카테고리를 이동시키는 버튼을 추가
				LinearLayout.LayoutParams categoryParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
						 														 ViewGroup.LayoutParams.FILL_PARENT,
						 														 1.0F);
				Button btnMoveOk = new Button(mContext);
				Button btnMoveCancel = new Button(mContext);
				btnMoveOk.setText(getString(R.string.btn_title_category_ok));
				btnMoveCancel.setText(getString(R.string.btn_title_del_cancel));
				
				btnMoveOk.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						if(categoryAdapter.getCheckedList().size() != 0) {
							
							final String[] categoryList = Utils.getCategoryListToString(mContext);
							
							AlertDialog.Builder builder = new Builder(mContext);
							builder.setTitle(getString(R.string.dialog_title_category_select));
							builder.setSingleChoiceItems(categoryList, 0, new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									mChangedCategory = categoryList[which];
//									Log.d(TAG, "선택된 카테고리는 : " + mChangedCategory);
								}
							}).setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									// 선택된 카테고리 (mChangedCategory) 로 변경 값을 변경
									final ConfigConfirmDialog changedDialog = new ConfigConfirmDialog(mContext, getString(R.string.dialog_title_changed_category));
									changedDialog.show();
									
									changedDialog.setOnDismissListener(new OnDismissListener() {
										public void onDismiss(DialogInterface dialog) {
											if(changedDialog.getDialogStateFlag()) {
												SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
												ContentValues updateValues = new ContentValues();
												updateValues.put(Constants.KEYWORD_COLUMN_CATEGORY, mChangedCategory);
												
												for(int i=0; i < categoryAdapter.getCheckedList().size(); i++) {
													sdb.update(Constants.TABLE_IDEA_KEYWORD, updateValues,
															Constants.KEYWORD_COLUMN_KEYWORD + " = '" + categoryAdapter.getCheckedList().get(i).getKeyword() + "' AND " +
															Constants.KEYWORD_COLUMN_CATEGORY + " = '" + categoryAdapter.getCheckedList().get(i).getCategory() + "'", null);
												}
												
												sdb.close();
												
												mDynamicLayout.removeAllViews();
												refreshList();
											}
										}
									});
								}
							}).setNegativeButton(getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									
								}
							});
							
							builder.show();
							
						}
					}
				});
				
				btnMoveCancel.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						mCtrlFlag = KEYWORD_LIST_FLAG;
						mDynamicLayout.removeAllViews();
						setListAdapter(mListAdapter);
					}
				});
				
				// 중복 표시 방지를 위해 먼저 버튼을 삭제한다.
				mDynamicLayout.removeAllViews();
				
				mDynamicLayout.addView(btnMoveOk, categoryParams);
				mDynamicLayout.addView(btnMoveCancel, categoryParams);
				
				break;
			default:
				break;
		}
		
		return result;
	}
	
	private void refreshList() {
		mCtrlFlag = KEYWORD_LIST_FLAG;
		
		mDynamicLayout.removeAllViews();
		
		mListAdapter.clear();
		mItems = Utils.getKeywordList(mContext, mSelectedCategory);
		mListAdapter = new KeywordListAdapter(mContext, R.layout.idea_keyword_list_row, mItems);
		setListAdapter(mListAdapter);
	}
	
	@Override
	public void onBackPressed() {
		switch(mCtrlFlag) {
		case KEYWORD_LIST_FLAG:
			super.onBackPressed();
			overridePendingTransition(R.anim.fade, R.anim.fade_out);
			break;
		default:
			mDynamicLayout.removeAllViews();
			refreshList();
			break;
		}
	}

	public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
		mSelectedCategory = mCategoryList[position];
		refreshList();
	}
	public void onNothingSelected(AdapterView<?> arg0) {}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
}
