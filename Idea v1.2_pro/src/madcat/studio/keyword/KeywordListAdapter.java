package madcat.studio.keyword;

import java.util.ArrayList;

import madcat.studio.idea.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class KeywordListAdapter extends ArrayAdapter<Keyword> {
	
	private ArrayList<Keyword> mItems;
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private int mResId;

	public KeywordListAdapter(Context context, int resId, ArrayList<Keyword> items) {
		super(context, resId, items);
		
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mLayoutInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			holder.textKeyword = (TextView)convertView.findViewById(R.id.keyword_list_row_name);
			holder.textCategory = (TextView)convertView.findViewById(R.id.keyword_list_row_category);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder)convertView.getTag();
		}

		holder.textKeyword.setText(mItems.get(position).getKeyword());
		holder.textCategory.setText("[" + mItems.get(position).getCategory() + "]");
		
		return convertView;
	}
	
	public class ViewHolder {
		public TextView textKeyword;
		public TextView textCategory;
	}
}
