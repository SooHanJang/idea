package madcat.studio.note.convert;

import java.io.Serializable;
import java.util.ArrayList;

public class ConvertedNoteView implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private byte[] mConvertedBitmapBytes = null;
	private ArrayList<ConvertedNoteEditText> mConvertedNoteEditTexts = null;
	
	public ConvertedNoteView() {
		
	}
	
	public ConvertedNoteView(byte[] convertedBitmapBytes, ArrayList<ConvertedNoteEditText> convertedNoteEditTexts) {
		mConvertedBitmapBytes = convertedBitmapBytes;
		mConvertedNoteEditTexts = new ArrayList<ConvertedNoteEditText>(convertedNoteEditTexts);
	}
	
	public void setConvertedBitmapBytes(byte[] convertedBitmapBytes) {
		mConvertedBitmapBytes = convertedBitmapBytes;
	}
	
	public void setConvertedEditTexts(ArrayList<ConvertedNoteEditText> convertedNoteEditTexts) {
		mConvertedNoteEditTexts = new ArrayList<ConvertedNoteEditText>(convertedNoteEditTexts);
	}
	
	public byte[] getConvertedBitmapBytes() {
		return mConvertedBitmapBytes;
	}
	
	public ArrayList<ConvertedNoteEditText> getConvertedNoteEditTexts() {
		return mConvertedNoteEditTexts;
	}
}