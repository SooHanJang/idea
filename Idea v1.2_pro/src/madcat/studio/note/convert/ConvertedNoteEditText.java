package madcat.studio.note.convert;

import java.io.Serializable;

public class ConvertedNoteEditText implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int mX, mY;
	private String mText;
	private float mTextSize;
	private int mTextColor;
	private int mTextStyle;			//0 : NORMAL, 1 : BOLD, 2 : ITALIC, 3 : BOTH
	
	public ConvertedNoteEditText(int x, int y, String text, float textSize, int textColor, int textStyle) {
		// TODO Auto-generated constructor stub
		mX = x;
		mY = y;
		mText = text;
		mTextSize = textSize;
		mTextColor = textColor;
		mTextStyle = textStyle;
	}
	
	public int    getX()			{ return mX; }
	public int    getY()			{ return mY; }
	public String getText()		{ return mText; }
	public float  getTextSize()	{ return mTextSize; }
	public int    getTextColor()	{ return mTextColor; }
	public int    getTextStyle()	{ return mTextStyle; }
}
