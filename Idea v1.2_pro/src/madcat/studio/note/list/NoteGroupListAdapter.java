package madcat.studio.note.list;

import java.util.ArrayList;

import madcat.studio.idea.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NoteGroupListAdapter extends ArrayAdapter<String> {
	
//	private final String TAG										=	"NoteGroupListAdapter";
	
	private Context mContext;
	private int mResId;
	private LayoutInflater mLayoutInflater;
	private ArrayList<String> mItems;
	
	public NoteGroupListAdapter(Context context, int resId, ArrayList<String> items) {
		super(context, resId, items);
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null) {
			convertView = mLayoutInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			holder.noteDate = (TextView)convertView.findViewById(R.id.manage_note_group_list_date);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder)convertView.getTag();
		}
		
		holder.noteDate.setText(mItems.get(position) + mContext.getString(R.string.suffix_note_group_row));
		
		return convertView;
	}
	
	public class ViewHolder {
		public TextView noteDate;
	}
}
