package madcat.studio.note.list;

import java.util.ArrayList;

import madcat.studio.idea.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NoteGroupListDelAdapter extends ArrayAdapter<String> {
	
	private Context mContext;
	private int mResId;
	private ArrayList<String> mItems;
	private LayoutInflater mInflater;
	
	private ArrayList<String> mDelItems;
	private ArrayList<Integer> mListItems;
	
	public NoteGroupListDelAdapter(Context context, int resId, ArrayList<String> items) {
		super(context, resId, items);
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		this.mDelItems = new ArrayList<String>();
		this.mListItems = new ArrayList<Integer>();
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final boolean checkFlag	=	false;
		
		if(convertView == null) {
			convertView = mInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			holder.linearLayout = (LinearLayout)convertView.findViewById(R.id.manage_note_group_list_del_layout);
			holder.textNotes = (TextView)convertView.findViewById(R.id.manage_note_group_list_del_notes);
			holder.checkKeyword = (CheckBox)convertView.findViewById(R.id.manage_note_group_list_del_row_check);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder)convertView.getTag();
		}

		holder.linearLayout.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				holder.checkKeyword.setChecked(!holder.checkKeyword.isChecked());
			}
		});
		
		holder.textNotes.setText(mItems.get(position) + mContext.getString(R.string.suffix_note_group_row));
		
		holder.checkKeyword.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					for(int i=0; i < mListItems.size(); i++) {
						if(mListItems.get(i) == position) {
							return;
						}
					}
					mListItems.add(position);
					mDelItems.add(mItems.get(position));
				} else {
					for(int i=0; i < mListItems.size(); i++) {
						if(mListItems.get(i) == position) {
							mListItems.remove(i);
							mDelItems.remove(mItems.get(position));
							break;
						}
					}
				}
			}
		});
		
		boolean reChecked = false;
		for(int i=0; i < mListItems.size(); i++) {
			if(mListItems.get(i) == position) {
				holder.checkKeyword.setChecked(true);
				reChecked = true;
				break;
			}
		}
		
		if(!reChecked) {
			holder.checkKeyword.setChecked(false);
		}
		
		return convertView;
	}
	
	public class ViewHolder {
		LinearLayout linearLayout;
		TextView textNotes;
		CheckBox checkKeyword;
	}
	
	public ArrayList<String> getCheckedList() {
		return mDelItems;
	}
}
