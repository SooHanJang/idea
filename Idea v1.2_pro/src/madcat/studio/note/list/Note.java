package madcat.studio.note.list;

public class Note {

	private String mNoteKeyword;
	private String mNoteDate;
	private byte[] mNoteBytes;
	
	public String getNoteKeyword() {	return mNoteKeyword;	}
	public void setNoteKeyword(String noteKeyword) {	this.mNoteKeyword = noteKeyword;	}
	
	public String getNoteDate() {	return mNoteDate;	}
	public void setNoteDate(String noteDate) {	this.mNoteDate = noteDate;	}
	
	public byte[] getNoteBytes() {	return mNoteBytes; }
	public void setNoteBytes(byte[] noteBytes) {	this.mNoteBytes = noteBytes;	}
}	
