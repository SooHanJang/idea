package madcat.studio.note.list;

import java.util.ArrayList;
import java.util.Calendar;

import madcat.studio.constants.Constants;
import madcat.studio.idea.R;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class NoteCalendar extends Activity implements OnItemClickListener, OnClickListener {
    
//    private static final String TAG										=	"NoteCalendar";
    
    public static int SUNDAY									        =	1;
    public static int MONDAY       										=	2;
    public static int TUESDAY										    =	3;
    public static int WEDNSESDAY										=	4;
    public static int THURSDAY											=	5;
    public static int FRIDAY											=	6;
    public static int SATURDAY											=	7;
     
    private Context mContext;
    private SharedPreferences mConfigPref;
    
    private TextView mTvCalendarTitle;
    private GridView mGvCalendar;
     
    private ArrayList<NoteDayInfo> mDayList;
    private ArrayList<NoteDayInfo> mMarkNoteList;
    private NoteCalendarAdapter mCalendarAdapter;
     
    Calendar mLastMonthCalendar;
    Calendar mThisMonthCalendar;
    Calendar mNextMonthCalendar;
    
    private int mDayOfMonthStartGap;
    private int mLastYear, mLastMonth, mLastMaximDay;
    private int mNextYear, mNextMonth;
    
    private int mWroteNoteIndex;
    
    // 정신나간 녀석 타이틀 설정을 위한 변수
	private int mMadCount = 0;
	private MessagePool mMessagePool;
	private boolean mMadSafeFlag										=	false;
	private Thread mMadSafeThread										=	null;
    
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.idea_manage_note_calendar);
        mContext = this;
        mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
        
        // 정신나간 녀석 타이틀 설정
        mMessagePool = (MessagePool)getApplicationContext();
        
        if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_MAD, false)) {
        	mMadCount = mMessagePool.getMadCount();
        }
        
        ImageButton btnLastMonth = (ImageButton)findViewById(R.id.calendar_btn_prev);
        ImageButton btnNextMonth = (ImageButton)findViewById(R.id.calendar_btn_next);
        ImageButton btnSetType = (ImageButton)findViewById(R.id.calendar_btn_setting);
         
        mTvCalendarTitle = (TextView)findViewById(R.id.calendar_title);
        mGvCalendar = (GridView)findViewById(R.id.calendar_display_calendar);
                  
        btnLastMonth.setOnClickListener(this);
        btnNextMonth.setOnClickListener(this);
        btnSetType.setOnClickListener(this);
        mTvCalendarTitle.setOnClickListener(this);
        mGvCalendar.setOnItemClickListener(this);
 
        mDayList = new ArrayList<NoteDayInfo>();
    }
 
	@Override
	protected void onStart() {
		super.onStart();
		
		// 전환 버튼 Safe 모드
		if(mMadSafeThread != null && mMadSafeThread.isAlive()) {
			mMadSafeThread.interrupt();
		}
		
		mMadSafeThread = new Thread(new Runnable() {
			public void run() {
				mMadSafeFlag = false;
				SystemClock.sleep(Constants.BUTTON_MAD_SAFE_DELAY);
				mMadSafeFlag = true;
			}
		});
		
		mMadSafeThread.start();
	}
	
    @Override
    protected void onResume() {
        super.onResume();
        
        // 이번달 의 캘린더 인스턴스를 생성한다.
        mThisMonthCalendar = Calendar.getInstance();
        mThisMonthCalendar.set(Calendar.DAY_OF_MONTH, 1);
        
        getCalendar(mThisMonthCalendar);
    }
    
    /**
     * 해당 달과 GridView Position 위치의 차이를 저장하기 위한 setter, getter 함수
     * 
     */
    
    public int getDayOfMonthStartGap() { return mDayOfMonthStartGap; }
	public void setDayOfMonthStartGap(int dayOfMonthStartPosition) { this.mDayOfMonthStartGap = dayOfMonthStartPosition; }
 
    /**
     * 달력을 셋팅한다.
     * 
     * @param calendar 달력에 보여지는 이번달의 Calendar 객체
     */
    private void getCalendar(Calendar calendar) {
        int lastMonthStartDay;
        int dayOfMonth;
        int thisMonthLastDay;
         
        mDayList.clear();
         
        // 이번달 시작일의 요일을 구한다. 시작일이 일요일인 경우 인덱스를 1(일요일)에서 8(다음주 일요일)로 바꾼다.)
        dayOfMonth = calendar.get(Calendar.DAY_OF_WEEK);
        thisMonthLastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
         
        calendar.add(Calendar.MONTH, -1);
 
        // 지난달의 마지막 일자를 구한다.
        lastMonthStartDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        
        // 지난달에 해당하는 년도와 날짜 그리고 마지막 날을 전역변수로 설정한다.
        mLastYear = calendar.get(Calendar.YEAR);
        mLastMonth = calendar.get(Calendar.MONTH)+1;
        mLastMaximDay = lastMonthStartDay;
        
        calendar.add(Calendar.MONTH, 1);
        
        // 다음 달에 해당하는 년도와 날짜를 전역변수로 설정한다. (반드시 calendar 에 add 한 이 지점에서 전역 설정을 해야함.)
        mNextMonth = calendar.get(Calendar.MONTH)+2;
        mNextYear = calendar.get(Calendar.YEAR);
        
        if(mNextMonth > 12) {
        	mNextYear++;
        	mNextMonth = 1;
        }
        
        if(dayOfMonth == SUNDAY) {
            dayOfMonth += 7;
        }
         
        lastMonthStartDay -= (dayOfMonth-1)-1;
         
 
        // 캘린더 타이틀(년월 표시)을 세팅한다. 
        mTvCalendarTitle.setText(mThisMonthCalendar.get(Calendar.YEAR) + "년 "
                + (mThisMonthCalendar.get(Calendar.MONTH) + 1) + "월");
 
        NoteDayInfo day;
         
        setDayOfMonthStartGap(dayOfMonth-2);
         
        // 지난 달
        for(int i=0; i<dayOfMonth-1; i++) {
            int date = lastMonthStartDay+i;
            day = new NoteDayInfo();
            
            day.setYear(Integer.toString(mLastYear));
            day.setMonth(Integer.toString(mLastMonth));
            day.setDay(Integer.toString(date));
            day.setInMonth(false);
             
            mDayList.add(day);
        }
        
        // 이번 달
        for(int i=1; i <= thisMonthLastDay; i++) {
            day = new NoteDayInfo();
            
            day.setYear(Integer.toString(mThisMonthCalendar.get(Calendar.YEAR)));
            day.setMonth(Integer.toString(mThisMonthCalendar.get(Calendar.MONTH) + 1));
            day.setDay(Integer.toString(i));
            day.setInMonth(true);
             
            mDayList.add(day);
        }
        
        // 다음 달
        for(int i=1; i<42-(thisMonthLastDay+dayOfMonth-1)+1; i++) {
            day = new NoteDayInfo();
            
            day.setYear(Integer.toString(mNextYear));
            day.setMonth(Integer.toString(mNextMonth));
            day.setDay(Integer.toString(i));
            day.setInMonth(false);
            mDayList.add(day);
        }
        
        initCalendarAdapter();
    }
    
    /**
     * param 값에 따른 Calendar 객체를 반환합니다.
     * 
     * @param calendar
     * @param year
     * @param month
     * @return
     */
    
    private Calendar getMoveMonth(Calendar calendar, int year, int month) {
    	calendar.set(year, month, 1);
    	calendar.add(Calendar.MONTH, -1);
    	mTvCalendarTitle.setText(mThisMonthCalendar.get(Calendar.YEAR) + "년 "
                + (mThisMonthCalendar.get(Calendar.MONTH) + 1) + "월");
    	
    	return calendar;
    }
    
    /**
     * 지난달의 Calendar 객체를 반환합니다.
     * 
     * @return LastMonthCalendar
     */
    private Calendar getLastMonth(Calendar calendar) {
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1);
        calendar.add(Calendar.MONTH, -1);
        mTvCalendarTitle.setText(mThisMonthCalendar.get(Calendar.YEAR) + "년 "
                + (mThisMonthCalendar.get(Calendar.MONTH) + 1) + "월");
        return calendar;
    }
 
    /**
     * 다음달의 Calendar 객체를 반환합니다.
     * 
     * @param calendar
     * @return NextMonthCalendar
     */
    private Calendar getNextMonth(Calendar calendar) {
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1);
        calendar.add(Calendar.MONTH, +1);
        mTvCalendarTitle.setText(mThisMonthCalendar.get(Calendar.YEAR) + "년 "
                + (mThisMonthCalendar.get(Calendar.MONTH) + 1) + "월");
        return calendar;
    }
     
    public void onItemClick(AdapterView<?> parent, View v, int position, long arg3)  {
    	
    	/*
    	 * 이번 달 클릭 : dayPositionIndex > 0 && dayPositionIndex <= mThisMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)
    	 * 지난 달 클릭 : dayPositionIndex <= 0
    	 * 다음 달 클릭 : dayPositionIndex > mThisMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)
    	 * 
    	 * Log 절대 지우지 말것!
    	 */
    	
    	int dayPositionIndex = position - getDayOfMonthStartGap();
    	int clickYear = 0, clickMonth = 0, clickDay = 0;
    	
    	if(dayPositionIndex > 0 && dayPositionIndex <= mThisMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
    		clickYear = mThisMonthCalendar.get(Calendar.YEAR);
    		clickMonth = mThisMonthCalendar.get(Calendar.MONTH)+1;
    		clickDay = position - getDayOfMonthStartGap();
    	} else if(dayPositionIndex <= 0) {
    		clickYear = mLastYear;
    		clickMonth = mLastMonth;
    		clickDay = mLastMaximDay + dayPositionIndex;
    	} else if(dayPositionIndex > mThisMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
    		clickYear = mNextYear;
    		clickMonth = mNextMonth;
    		clickDay = dayPositionIndex - mThisMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    	}
    	
//    	Log.d(TAG, "선택된 날짜 : " + clickYear + "년 " + clickMonth + "월 " + clickDay + "일");
    	
    	for(int i=0; i < mMarkNoteList.size(); i++) {
    		if(clickYear == Integer.parseInt(mMarkNoteList.get(i).getYear()) && 
    				clickMonth == Integer.parseInt(mMarkNoteList.get(i).getMonth()) &&
    				clickDay == Integer.parseInt(mMarkNoteList.get(i).getDay())) {
    			mWroteNoteIndex = i;
    			
    			Intent noteListIntent = new Intent(NoteCalendar.this, NoteList.class);
    			noteListIntent.putExtra(Constants.PUT_SEND_NOTE_WROTE_DATE, mMarkNoteList.get(mWroteNoteIndex).getDate());
    			startActivity(noteListIntent);
    			overridePendingTransition(R.anim.fade, R.anim.hold);
    			
    		} 
    	}
    }
    
    public void onClick(View v) {
        switch(v.getId()) {
        case R.id.calendar_title:
        	dialogDatePickerShow(mThisMonthCalendar);
        	break;
        case R.id.calendar_btn_prev:
            mThisMonthCalendar = getLastMonth(mThisMonthCalendar);
            getCalendar(mThisMonthCalendar);
            break;
        case R.id.calendar_btn_next:
            mThisMonthCalendar = getNextMonth(mThisMonthCalendar);
            getCalendar(mThisMonthCalendar);
            break;
        case R.id.calendar_btn_setting:
//        	Log.d(TAG, "flag : " + mMadSafeFlag);
        	
        	if(mMadSafeFlag) {
	        	if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_MAD, false)) {
	        		mMessagePool.setMadCount(++mMadCount);
	        	}
	        	
				Intent intent = new Intent(NoteCalendar.this, NoteGroupList.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				finish();
        	}
        	break;
        default:
        	break;
        }
    }
    
    private void initCalendarAdapter() {
        mCalendarAdapter = new NoteCalendarAdapter(this, R.layout.idea_manage_note_calendar_day, mDayList, 
        		mThisMonthCalendar.get(Calendar.YEAR), (mThisMonthCalendar.get(Calendar.MONTH)+1));
        mGvCalendar.setAdapter(mCalendarAdapter);
        
        String compareMonth = null;
        
        if((mThisMonthCalendar.get(Calendar.MONTH)+1) < 10) {
        	compareMonth = "0" + (mThisMonthCalendar.get(Calendar.MONTH)+1);
        } else {
        	compareMonth = String.valueOf((mThisMonthCalendar.get(Calendar.MONTH)+1));
        }
        
        mMarkNoteList = Utils.getWroteNoteToMonth(mContext, String.valueOf(mThisMonthCalendar.get(Calendar.YEAR)), compareMonth);
    }
    
    /**
     * DatePickDialog를 화면에 표시하며, 설정된 날짜 값에 따라 달력을 이동합니다.
     * 
     * @param calendar
     */
    
    private void dialogDatePickerShow(final Calendar calendar) {
    	
    	int todayOfYear = calendar.get(calendar.YEAR);
    	int todayOfMonth = calendar.get(calendar.MONTH);
    	int todayOfDay = calendar.get(calendar.DAY_OF_MONTH);
    	
    	/*
    	 * DatePicker Click 이벤트를 처리하는 리스너
    	 */
    	DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				Toast.makeText(mContext, year + "년 " + (monthOfYear+1) + "월 " + dayOfMonth + "일", Toast.LENGTH_SHORT).show();
				mThisMonthCalendar = getMoveMonth(calendar, year, (monthOfYear+1));
				getCalendar(mThisMonthCalendar);
			}
		};
    	
		DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, dateSetListener, todayOfYear, todayOfMonth, todayOfDay);
		datePickerDialog.show();
    }
    
    @Override
	protected void onDestroy() {
    	Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
    	
		super.onDestroy();
	}
    
    @Override
    public void onBackPressed() {
    	mMessagePool.setMadCount(0);
    	
    	super.onBackPressed();
    	overridePendingTransition(R.anim.fade, R.anim.fade_out);
    }
}









