package madcat.studio.note.list;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.dialogs.ConfirmDialog;
import madcat.studio.idea.R;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

public class NoteGroupList extends ListActivity implements OnClickListener {

//	private final String TAG											=	"NoteGroupList";
	
	private final int VIEW_NOTES_GROUP									=	1;
	private final int DELETE_NOTES_GROUP								=	2;
	
	private final int NOTE_GROUP_LIST_FLAG								=	1;
	private final int NOTE_GROUP_DEL_FLAG								=	2;
	
	private boolean mAnimFlag											=	false;
	private int mCtrlFlag												=	NOTE_GROUP_LIST_FLAG;
	
	private Context mContext;
	private ArrayList<String> mItems;
	private SharedPreferences mConfigPref;
	private NoteGroupListAdapter mListAdapter;
	
	private LinearLayout mDynamicLayout;
	private ImageButton mBtnViewType, mBtnDelOk, mBtnDelCancel;
	
	// 정신나간 녀석 타이틀 설정을 위한 변수
	private int mMadCount = 0;
	private MessagePool mMessagePool;
	private boolean mMadSafeFlag										=	false;
	private Thread mMadSafeThread										=	null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.idea_manage_note_group_list);
		
		// 기본 설정
		mContext = this;
		mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		// 정신나간 녀석 타이틀 설정
		mMessagePool = (MessagePool)mContext.getApplicationContext();
		
		if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_MAD, false)) {
			mMadCount = mMessagePool.getMadCount();
			
			switch(mMadCount) {
			case 10:
				Toast.makeText(mContext, getString(R.string.toast_title_mad_count_10), Toast.LENGTH_SHORT).show();
				break;
			case 20:
				Toast.makeText(mContext, getString(R.string.toast_title_mad_count_20), Toast.LENGTH_SHORT).show();
				break;
			case 30:
				Toast.makeText(mContext, getString(R.string.toast_title_mad_count_30), Toast.LENGTH_SHORT).show();
				break;
			case 40:
				Toast.makeText(mContext, getString(R.string.toast_title_mad_count_40), Toast.LENGTH_SHORT).show();
				break;
			case 50:
				// '정신나간 녀석' 도전과제 성공
				SharedPreferences.Editor editor = mConfigPref.edit();
				editor.putBoolean(Constants.COMPLETE_CHALLENGE_MAD, true);
				editor.commit();
				
				Utils.updateChallengeState(mContext, Constants.CHALLENGE_MAD);
				Toast.makeText(mContext, getString(R.string.complete_challenge_mad), Toast.LENGTH_SHORT).show();
				break;
			}
		}

		// Resource Id 설정
		mDynamicLayout = (LinearLayout)findViewById(R.id.manage_note_group_dynamic_del_area);
		mBtnDelOk = (ImageButton)findViewById(R.id.manage_note_group_del_ok);
		mBtnDelCancel = (ImageButton)findViewById(R.id.manage_note_group_del_cancel);
		
		mBtnViewType = (ImageButton)findViewById(R.id.manage_note_group_view_type);
		
		// Event Listener 설정
		mBtnViewType.setOnClickListener(this);
		
		// Adapter Item 설정
		mItems = Utils.getNoteGroupList(mContext);
		
		// Note List 가 비어있었을 시 토스트 생성
		if(mItems.isEmpty()) {
			if(!mMessagePool.getNoteEmptyFlag()) {
				Toast.makeText(mContext, getString(R.string.toast_title_empty_notes),Toast.LENGTH_SHORT).show();
				
				Thread emptyThread = new Thread(new Runnable() {
					public void run() {
						mMessagePool.setNoteEmptyFlag(true);
						SystemClock.sleep(Constants.BUTTON_SAFE_DELAY);
						mMessagePool.setNoteEmptyFlag(false);
					}
				});
				
				emptyThread.start();
			}
		}
		
		mListAdapter = new NoteGroupListAdapter(mContext, R.layout.idea_manage_note_group_list_row, mItems);
		setListAdapter(mListAdapter);
		
		registerForContextMenu(getListView());
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		refreshList();
		
		// 전환 버튼 Safe 모드
		if(mMadSafeThread != null && mMadSafeThread.isAlive()) {
			mMadSafeThread.interrupt();
		}
		
		mMadSafeThread = new Thread(new Runnable() {
			public void run() {
//				Log.d(TAG, "111 : " + mMadSafeFlag);
				
				mMadSafeFlag = false;
				SystemClock.sleep(Constants.BUTTON_MAD_SAFE_DELAY);
				mMadSafeFlag = true;
				
//				Log.d(TAG, "222 : " + mMadSafeFlag);
			}
		});
		
		mMadSafeThread.start();
		
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		boolean result = super.onPrepareOptionsMenu(menu);
		
//		Log.d(TAG, "onPrepareOptionMenu call");
		
		menu.removeItem(R.id.menu_note_del);
		
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.note_list_menu, menu);
		
		return result;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		
//		Log.d(TAG, "longclick interrupt");
		
		switch(item.getItemId()) {
		case R.id.menu_note_del:
//			Log.d(TAG, "Note Delete");
			
			mCtrlFlag = NOTE_GROUP_DEL_FLAG;
			
			mDynamicLayout.setVisibility(View.VISIBLE);
			
			final NoteGroupListDelAdapter delAdapter = new NoteGroupListDelAdapter(mContext, R.layout.idea_manage_note_group_list_del_row, mItems);
			setListAdapter(delAdapter);
			
			mBtnDelOk.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					if(delAdapter.getCheckedList().size() != 0) {
						final ConfirmDialog deleteDialog = new ConfirmDialog(mContext, R.drawable.dialog_title_note_delete);
						deleteDialog.show();
						
						deleteDialog.setOnDismissListener(new OnDismissListener() {
							public void onDismiss(DialogInterface dialog) {
								if(deleteDialog.getDialogStateFlag()) {
									SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
									
									for(int i=0; i < delAdapter.getCheckedList().size(); i++) {
										sdb.delete(Constants.TABLE_IDEA_NOTE, 
												Constants.NOTE_COLUMN_DATE + " = '" + delAdapter.getCheckedList().get(i) + "'", null);
									}
									
									sdb.close();
									
									// List Adapter 를 재설정
									mDynamicLayout.setVisibility(View.GONE);
									refreshList();
								}
							}
						});
					}
				}
			});
			
			mBtnDelCancel.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					mCtrlFlag = NOTE_GROUP_LIST_FLAG;
					mDynamicLayout.setVisibility(View.GONE);
					setListAdapter(mListAdapter);
				}
			});

			break;
		}
		
		return result;
	}
	
	public void refreshList() {
		mCtrlFlag = NOTE_GROUP_LIST_FLAG;
		
		mListAdapter.clear();
		mItems = Utils.getNoteGroupList(mContext);
		mListAdapter = new NoteGroupListAdapter(mContext, R.layout.idea_manage_note_group_list_row, mItems);
		setListAdapter(mListAdapter);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
//		Log.d(TAG, "동작");
		
		Intent intent = new Intent(NoteGroupList.this, NoteList.class);
		intent.putExtra(Constants.PUT_SEND_NOTE_WROTE_DATE, mItems.get(position));
		startActivity(intent);
		overridePendingTransition(R.anim.fade, R.anim.hold);
		
		super.onListItemClick(l, v, position, id);
	}
	
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.manage_note_group_view_type:
//				Log.d(TAG, "mMadSafeFlag : " + mMadSafeFlag);
				
				if(mMadSafeFlag) {
				
					mMessagePool.setMadCount(++mMadCount);
					
					Intent intent = new Intent(NoteGroupList.this, NoteCalendar.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(intent);
					overridePendingTransition(R.anim.fade, R.anim.hold);
					finish();
				} 
				break;
			default:
				break;
		}
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		switch(mCtrlFlag) {
		case NOTE_GROUP_LIST_FLAG:
			if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_MAD, false)) {
				mMessagePool.setMadCount(0);
			}
			
			super.onBackPressed();
			overridePendingTransition(R.anim.fade, R.anim.fade_out);
			break;
		default:
			mDynamicLayout.setVisibility(View.GONE);
			refreshList();
			break;
		}
	}
}
