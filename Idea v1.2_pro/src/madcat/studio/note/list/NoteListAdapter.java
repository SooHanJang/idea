package madcat.studio.note.list;

import java.util.ArrayList;

import madcat.studio.idea.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NoteListAdapter extends ArrayAdapter<Note> {
	
	private Context mContext;
	private int mResId;
	private LayoutInflater mLayoutInflater;
	private ArrayList<Note> mItems;
	
	public NoteListAdapter(Context context, int resId, ArrayList<Note> items) {
		super(context, resId, items);
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mLayoutInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			holder.textKeyword = (TextView)convertView.findViewById(R.id.manage_note_list_keyword);
			holder.textDate = (TextView)convertView.findViewById(R.id.manage_note_list_date);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder)convertView.getTag();
		}
		
		holder.textKeyword.setText(mItems.get(position).getNoteKeyword());
		holder.textKeyword.setSelected(true);
		holder.textDate.setText(mItems.get(position).getNoteDate());
		
		return convertView;
	}
	
	public class ViewHolder {
		public TextView textKeyword;
		public TextView textDate;
	}

}
