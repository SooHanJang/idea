package madcat.studio.note.list;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.dialogs.ConfirmDialog;
import madcat.studio.dialogs.ContextListDialog;
import madcat.studio.idea.R;
import madcat.studio.note.NoteActivity;
import madcat.studio.utils.Utils;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

public class NoteList extends ListActivity {
	
//	private final String TAG										=	"NoteList";
	
	private final int NOTE_LIST_FLAG								=	1;
	private final int NOTE_DEL_FLAG									=	2;
	
	private int mCtrlFlag											=	NOTE_LIST_FLAG;
	
	private Context mContext;
	private ArrayList<Note> mItems;
	private NoteListAdapter mListAdapter;
	
	private LinearLayout mDynamicLayout;
	private ImageButton mBtnDelOk, mBtnDelCancel;
	
	private String mNoteDate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.idea_manage_note_list);
		
		mContext = this;
		getIntenter();
		
		// Resource Id 설정
		mDynamicLayout = (LinearLayout)findViewById(R.id.manage_note_dynamic_del_area);
		
		mBtnDelOk = (ImageButton)findViewById(R.id.manage_note_del_ok);
		mBtnDelCancel = (ImageButton)findViewById(R.id.manage_note_del_cancel);
		
		mItems = Utils.getNoteList(mContext, mNoteDate);
		
		mListAdapter = new NoteListAdapter(mContext, R.layout.idea_manage_note_list_row, mItems);
		setListAdapter(mListAdapter);
		
		registerForContextMenu(getListView());
		
		Toast.makeText(mContext, getString(R.string.toast_title_guide_manage_note), Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		final int index = ((AdapterView.AdapterContextMenuInfo)menuInfo).position;
		
		final ContextListDialog listDialog = new ContextListDialog(mContext, R.style.PopupDialog);
		listDialog.show();
		
		listDialog.setOnDismissListener(new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				switch(listDialog.getDialogFlag()) {
				case 1:
					Intent noteViewIntent = new Intent(NoteList.this, NoteActivity.class);
					noteViewIntent.putExtra(Constants.PUT_CALLER_ACTIVITY, Constants.CALLER_MANAGE_IDEA);
					noteViewIntent.putExtra(Constants.PUT_SEND_NOTE_WROTE_DATE, mItems.get(index).getNoteDate());
					noteViewIntent.putExtra(Constants.PUT_SEND_NOTE_WROTE_KEYWORD, mItems.get(index).getNoteKeyword());
					startActivity(noteViewIntent);
					overridePendingTransition(R.anim.fade, R.anim.hold);
					break;
				case 2:
					final ConfirmDialog delNoteConfirmDialog = new ConfirmDialog(mContext, R.drawable.dialog_title_note_delete);
					delNoteConfirmDialog.show();
					
					delNoteConfirmDialog.setOnDismissListener(new OnDismissListener() {
						public void onDismiss(DialogInterface dialog) {
							if(delNoteConfirmDialog.getDialogStateFlag()) {
								// 삭제하려는 노트 날짜와 오늘 날짜가 같으면, 환경 설정값을 초기화
								SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
								String today = dateFormat.format(new java.util.Date());
								
								// Database 에서 노트를 삭제한다.
								SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
								sdb.delete(Constants.TABLE_IDEA_NOTE, 
										Constants.NOTE_COLUMN_KEYWORD + " = '" + mItems.get(index).getNoteKeyword() + "' AND " +
										Constants.NOTE_COLUMN_DATE + " = '" + mItems.get(index).getNoteDate() + "'", null);
								
								sdb.close();
		
								// List Adapter 를 재설정
								refreshList();
							}
						}
					});
					
					break;
				}
			}
		});
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		boolean result = super.onPrepareOptionsMenu(menu);
		
//		Log.d(TAG, "onPrepareOptionMenu call");
		
		menu.removeItem(R.id.menu_note_del);
		
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.note_list_menu, menu);
		
		return result;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		
//		Log.d(TAG, "longclick interrupt");
		
		switch(item.getItemId()) {
		case R.id.menu_note_del:
//			Log.d(TAG, "Note Delete");
			
			mCtrlFlag = NOTE_DEL_FLAG;
			
			mDynamicLayout.setVisibility(View.VISIBLE);
			
			final NoteListDelAdapter delAdapter = new NoteListDelAdapter(mContext, R.layout.idea_manage_note_list_del_row, mItems);
			setListAdapter(delAdapter);
			
			mBtnDelOk.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					if(delAdapter.getCheckedList().size() != 0) {
						final ConfirmDialog deleteDialog = new ConfirmDialog(mContext, R.drawable.dialog_title_note_delete);
						deleteDialog.show();
						
						deleteDialog.setOnDismissListener(new OnDismissListener() {
							public void onDismiss(DialogInterface dialog) {
								if(deleteDialog.getDialogStateFlag()) {
									SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
									
									for(int i=0; i < delAdapter.getCheckedList().size(); i++) {
										sdb.delete(Constants.TABLE_IDEA_NOTE, 
												Constants.NOTE_COLUMN_KEYWORD + " = '" + delAdapter.getCheckedList().get(i).getNoteKeyword() + "' AND " +
												Constants.NOTE_COLUMN_DATE + " = '" + delAdapter.getCheckedList().get(i).getNoteDate() + "'", null);
									}
									
									sdb.close();
									
									// List Adapter 를 재설정
									mDynamicLayout.setVisibility(View.GONE);
									refreshList();
								}
							}
						});
					} 
				}
			});
			
			mBtnDelCancel.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					mCtrlFlag = NOTE_LIST_FLAG;
					mDynamicLayout.setVisibility(View.GONE);
					setListAdapter(mListAdapter);
				}
			});

			break;
		}
		
		return result;
	}
	
	public void refreshList() {
		mCtrlFlag = NOTE_LIST_FLAG;
		
		mListAdapter.clear();
		mItems = Utils.getNoteList(mContext, mNoteDate);
		mListAdapter = new NoteListAdapter(mContext, R.layout.idea_manage_note_list_row, mItems);
		setListAdapter(mListAdapter);
	}

	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		switch(mCtrlFlag) {
		case NOTE_LIST_FLAG:
			super.onBackPressed();
			overridePendingTransition(R.anim.fade, R.anim.fade_out);
			break;
		default:
			mDynamicLayout.setVisibility(View.GONE);
			refreshList();
			break;
		}
	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		
		if(intent != null) {
			mNoteDate = intent.getExtras().getString(Constants.PUT_SEND_NOTE_WROTE_DATE);
		}
	}
}
