package madcat.studio.note.list;

public class NoteDayInfo {
	private String year;
	private String day;
	private String month;
	private String keyword;
	private String date;
    
	private boolean inMonth;
    
    public String getYear() {	return year;	}
    public void setYear(String year) {	this.year = year;	}
    
    public String getMonth() {	return month;	}
    public void setMonth(String month) {	this.month = month;	}
    
    public String getDay() {	return day;	}
    public void setDay(String day) {	this.day = day;	}
    
    public String getDate() {	return date;	}
    public void setDate(String date) {	this.date = date;	}
 
    /**
     * 이번달의 날짜인지 정보를 반환한다.
     * 
     * @return inMonth(true/false)
     */
    public boolean isInMonth() {
        return inMonth;
    }
 
    /**
     * 이번달의 날짜인지 정보를 저장한다.
     * 
     * @param inMonth(true/false)
     */
    public void setInMonth(boolean inMonth) {
        this.inMonth = inMonth;
    }
}
