package madcat.studio.note.list;

import java.util.ArrayList;

import madcat.studio.idea.R;
import madcat.studio.utils.Utils;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class NoteCalendarAdapter extends BaseAdapter {

//	private final String TAG										=	"CalendarAdapter";
	
	private ArrayList<NoteDayInfo> mDayList;
    private Context mContext;
    private int mResource;
    private LayoutInflater mLiInflater;
    private int mTodayYear, mTodayMonth;
    private ArrayList<NoteDayInfo> mMarkDayInfo;
 
    /**
     * Adpater 생성자
     * 
     * @param context
     *            컨텍스트
     * @param textResource
     *            레이아웃 리소스
     * @param dayList
     *            날짜정보가 들어있는 리스트
     * @param todayMonth          
     *  		   이번달 날짜
     */
    public NoteCalendarAdapter(Context context, int textResource, ArrayList<NoteDayInfo> dayList, int todayYear, int todayMonth) {
        this.mContext = context;
        this.mDayList = dayList;
        this.mResource = textResource;
        this.mLiInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mTodayYear = todayYear;
        this.mTodayMonth = todayMonth;
        
        getMarkFromDatabase();
    }
    
    public int getCount() {
        return mDayList.size();
    }
 
    public Object getItem(int position) {
        return mDayList.get(position);
    }
 
    public long getItemId(int position) {
        return 0;
    }
    
    private void getMarkFromDatabase() {
    	// 데이터베이스에서 String 형태의 월을 비교하기 위해, 10보다 작은 경우에만 0을 월 앞에 추가한다.
    	String compareMonth = null;
    	
    	if(mTodayMonth < 10) {
    		compareMonth = "0" + mTodayMonth;
    	} else {
    		compareMonth = String.valueOf(mTodayMonth);
    	}

    	// 데이터베이스에서 해당 년도와 달에 해당하는 노트를 읽어온다.
    	mMarkDayInfo = Utils.getWroteNoteToMonth(mContext, String.valueOf(mTodayYear), compareMonth);
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
        NoteDayInfo day = mDayList.get(position);
 
        DayViewHolder dayViewHolder;
        
        if(convertView == null) {
            convertView = mLiInflater.inflate(mResource, null);
 
            if(position % 7 == 6) {
                convertView.setLayoutParams(new GridView.LayoutParams(getCellWidthDP()+getRestCellWidthDP(), getCellHeightDP()));
            } else {
                convertView.setLayoutParams(new GridView.LayoutParams(getCellWidthDP(), getCellHeightDP()));    
            }
            
            dayViewHolder = new DayViewHolder();
 
            dayViewHolder.llBackground = (RelativeLayout)convertView.findViewById(R.id.day_cell_ll_background);
            dayViewHolder.tvDay = (TextView) convertView.findViewById(R.id.day_cell_tv_day);
            dayViewHolder.markImage = (ImageView) convertView.findViewById(R.id.day_cell_mark_image);
            
            convertView.setTag(dayViewHolder);
        } else {
            dayViewHolder = (DayViewHolder) convertView.getTag();
        }
        
        if(day != null) {
            dayViewHolder.tvDay.setText(day.getDay());
 
            if(day.isInMonth()) {
                if(position % 7 == 0) {
                    dayViewHolder.tvDay.setTextColor(Color.RED);
                } else if(position % 7 == 6) {
                    dayViewHolder.tvDay.setTextColor(Color.BLUE);
                } else {
                    dayViewHolder.tvDay.setTextColor(Color.BLACK);
                }
            } else {
                dayViewHolder.tvDay.setTextColor(Color.GRAY);
            }
            
            if(mMarkDayInfo != null) {
	            for(int i=0; i < mMarkDayInfo.size(); i++) {
	            	if((Integer.parseInt(day.getYear()) == Integer.parseInt(mMarkDayInfo.get(i).getYear())) && 
	            			(Integer.parseInt(day.getMonth()) == Integer.parseInt(mMarkDayInfo.get(i).getMonth())) && 
	            			(Integer.parseInt(day.getDay()) == Integer.parseInt(mMarkDayInfo.get(i).getDay()))) {
	            		dayViewHolder.markImage.setVisibility(View.VISIBLE);
	            	}
	            }
            }
        }
        
        return convertView;
    }
 
    public class DayViewHolder {
        public RelativeLayout llBackground;
        public TextView tvDay;
        public ImageView markImage;
    }
 
    private int getCellWidthDP() {
    	int cellWidth = (mContext.getResources().getDisplayMetrics().widthPixels) / 7;
         
        return cellWidth;
    }
     
    private int getRestCellWidthDP() {
    	int cellWidth = (mContext.getResources().getDisplayMetrics().widthPixels) % 7;
         
        return cellWidth;
    }
     
    private int getCellHeightDP() {
    	int cellHeight = (mContext.getResources().getDisplayMetrics().widthPixels) / 6;
         
        return cellHeight;
    }
}
