package madcat.studio.note;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.dialogs.NoSaveNoteExit;
import madcat.studio.idea.R;
import madcat.studio.note.convert.ConvertedNoteView;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class NoteActivity extends Activity {
	/**
	 * 
	 */
	private static final long serialVersionUID										=	1L;
	
//	private final String TAG														=	"NoteActivity";
	private static final int MAX_PAGE_NUMBER										=	Constants.MAX_NOTE_PAGE;
	
	private boolean mLoadDbFlag														=	false;
	
	private Context mContext = null;
	private MessagePool mMessagePool;

	private LinearLayout mMainLayout = null;
	private LinearLayout mMainMenuBar = null, mTextMenuBar = null, mDrawMenuBar = null, mEraseMenuBar = null;
	private FrameLayout mSubMenuBar = null;
	
	private TextView mKeywordViewer = null, mPageViewer = null;
	private ImageButton mPrevPageButton = null, mNextPageButton = null;
	private ImageButton mAddPageButton = null, mDeletePageButton = null;
	
	private RadioGroup mTextColorGroup = null, mDrawColorGroup = null, mDrawWidthGroup = null, mEraseWidthGroup = null;
	private CheckBox mTextStyleBoldButton = null, mTextStyleItalicButton = null, mTextStyleUnderlineButton = null;
	private ImageButton mTextToolButton = null, mDrawToolButton = null, mEraseToolButton = null;
	
	private NoteView mNoteView = null;

	private boolean mIsTextSelected = false, mIsDrawSelected = false, mIsEraseSelected = false;
	
	private String mKeyword = null, mStartDay = null, mCallerActivity = null;
	private SharedPreferences mConfigPref = null;
	
	private boolean mPageToastFlag = false;
	
	private boolean mSaveExitFlag										=	true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.idea_note_main_layout);
		
		mContext = this;
		mMessagePool = (MessagePool)getApplicationContext();
		
		// SharedPreference 초기화 설정
		mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		// 노트를 시작한 오늘 날짜 생성
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		mStartDay = dateFormat.format(new java.util.Date());
		
		// 노트를 호출 한 곳의 인텐트 정보를 가져온다.
		getIntenter();
		
		mMainLayout = (LinearLayout)findViewById(R.id.note_mainLayout);
		
		mKeywordViewer = (TextView)findViewById(R.id.note_edit_keywordViewer);
		mPageViewer = (TextView)findViewById(R.id.note_edit_pageViewer);
		mPrevPageButton = (ImageButton)findViewById(R.id.note_edit_pagePrev);
		mNextPageButton = (ImageButton)findViewById(R.id.note_edit_pageNext);
		
		mNoteView = (NoteView)findViewById(R.id.note_edit_noteview);
		
		mAddPageButton = (ImageButton)findViewById(R.id.note_edit_addPage);
		mDeletePageButton = (ImageButton)findViewById(R.id.note_edit_delPage);
		
		mMainMenuBar = (LinearLayout)findViewById(R.id.note_edit_main_menubar);
		mSubMenuBar = (FrameLayout)findViewById(R.id.note_edit_submenubar_layout);
		
		mTextMenuBar = (LinearLayout)findViewById(R.id.note_edit_text_submenubar);
		mTextColorGroup = (RadioGroup)findViewById(R.id.note_edit_text_color_group);
		mTextStyleBoldButton = (CheckBox)findViewById(R.id.note_edit_text_style_bold);
		mTextStyleItalicButton = (CheckBox)findViewById(R.id.note_edit_text_style_italic);
		mTextStyleUnderlineButton = (CheckBox)findViewById(R.id.note_edit_text_style_underline);
		
		mDrawMenuBar = (LinearLayout)findViewById(R.id.note_edit_draw_submenubar);
		mDrawColorGroup = (RadioGroup)findViewById(R.id.note_edit_draw_color_group);
		mDrawWidthGroup = (RadioGroup)findViewById(R.id.note_edit_draw_width_group);
		
		mEraseMenuBar = (LinearLayout)findViewById(R.id.note_edit_erase_submenubar);
		mEraseWidthGroup = (RadioGroup)findViewById(R.id.note_edit_erase_width_group);
		
		mTextToolButton = (ImageButton)findViewById(R.id.note_edit_text);
		mDrawToolButton = (ImageButton)findViewById(R.id.note_edit_draw);
		mEraseToolButton = (ImageButton)findViewById(R.id.note_edit_eraser);
		
		ImageButtonEventHandler imageButtonEventHandler = new ImageButtonEventHandler();
		
		mPrevPageButton.setOnClickListener(imageButtonEventHandler);
		mNextPageButton.setOnClickListener(imageButtonEventHandler);
		
		mAddPageButton.setOnClickListener(imageButtonEventHandler);
		mDeletePageButton.setOnClickListener(imageButtonEventHandler);
		
		mTextToolButton.setOnClickListener(imageButtonEventHandler);
		mDrawToolButton.setOnClickListener(imageButtonEventHandler);
		mEraseToolButton.setOnClickListener(imageButtonEventHandler);
		
		RadioGroupEventHandler radioGroupEventHandler = new RadioGroupEventHandler();
		
		mTextColorGroup.setOnCheckedChangeListener(radioGroupEventHandler);
		mDrawColorGroup.setOnCheckedChangeListener(radioGroupEventHandler);
		mDrawWidthGroup.setOnCheckedChangeListener(radioGroupEventHandler);
		mEraseWidthGroup.setOnCheckedChangeListener(radioGroupEventHandler);
		
		CheckBoxEventHandler checkBoxEventHandler = new CheckBoxEventHandler();
		
		mTextStyleBoldButton.setOnCheckedChangeListener(checkBoxEventHandler);
		mTextStyleItalicButton.setOnCheckedChangeListener(checkBoxEventHandler);
		mTextStyleUnderlineButton.setOnCheckedChangeListener(checkBoxEventHandler);
		
		mNoteView.initialize();
		
//		Log.d(TAG, "비교할 startDay : " + mStartDay);
//		Log.d(TAG, "노트가 생성되었다면 생성된 날짜는? " + mConfigPref.getString(Constants.CONFIG_WROTE_NOTE_DATE, ""));
		
		if(mCallerActivity.equals(Constants.CALLER_CREATE_IDEA)) {
			loadDataFromDB();
			
			mMessagePool.setNoteWritingFlag(true);
			mIsDrawSelected = true;
			mDrawToolButton.setBackgroundResource(R.drawable.note_draw_selected);
			setMode(NoteView.DRAW_MODE);
		} else if(mCallerActivity.equals(Constants.CALLER_MANAGE_IDEA)) {
			// 노트 보기에서 실행이 되었을 때는, 툴 레이아웃을 전부 화면에서 감춘다.
			loadDataFromDB();
			setMode(NoteView.VIEW_MODE);
		}
		
		refreshPageViewer();
		
//		Log.d(TAG, "onCreate");
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mMainLayout.requestFocusFromTouch();
		
		if(mSaveExitFlag) {
			saveDataToDB();
		}
		
		finish();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
		finish();
	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		
		if(intent != null) {
			mCallerActivity = intent.getExtras().getString(Constants.PUT_CALLER_ACTIVITY);
			
			if(mCallerActivity.equals(Constants.CALLER_CREATE_IDEA)) {
				mKeyword = intent.getExtras().getString(Constants.PUT_SEND_KEYWORD);
			} else if(mCallerActivity.equals(Constants.CALLER_MANAGE_IDEA)) {
				mStartDay = intent.getExtras().getString(Constants.PUT_SEND_NOTE_WROTE_DATE);
				mKeyword = intent.getExtras().getString(Constants.PUT_SEND_NOTE_WROTE_KEYWORD);
			}
		}
	}
	
	private void loadDataFromDB() {
//		Log.d(TAG, "노트 불러오기 시작");
		
		ArrayList<ConvertedNoteView> convertedNoteViews = new ArrayList<ConvertedNoteView>();

		//Read From DB Process...
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		String loadSQL = "SELECT * FROM " + Constants.TABLE_IDEA_NOTE + " WHERE " + Constants.NOTE_COLUMN_KEYWORD + " = '" + mKeyword + 
						 "' AND " + Constants.NOTE_COLUMN_DATE + " = '" + mStartDay + "'";						 
		
		Cursor loadCursor = sdb.rawQuery(loadSQL, null);
		loadCursor.moveToFirst();
		
		if(loadCursor.getCount() != 0) {
			mStartDay = loadCursor.getString(1);
			mKeyword = loadCursor.getString(2);
			convertedNoteViews = (ArrayList<ConvertedNoteView>) Utils.deSerializeObject(loadCursor.getBlob(3));
			
			// 키워드 TextView 설정
			Display display = ((WindowManager)getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
	    	int displayWidth = display.getWidth();
			float textSize = Utils.setTextViewTextSize(mKeywordViewer, displayWidth, mKeyword);
			
			mKeywordViewer.setText(mKeyword);
			mKeywordViewer.setTextSize(TypedValue.COMPLEX_UNIT_SP, Constants.DEFAULT_KEYWORD_TEXTVIEW_SIZE - textSize);
			
			// Database 에서 데이터를 로드 하였다고 알림
			mLoadDbFlag = true;
			
			mNoteView.setConvertedNoteViews(convertedNoteViews);
			mMainLayout.requestFocusFromTouch();
		} else {
			Toast.makeText(mContext, getString(R.string.toast_title_new_write_note), Toast.LENGTH_SHORT).show();
			setKeywordViewer(mKeyword);
		}
		
		loadCursor.close();
		sdb.close();
		
//		Log.d(TAG, "노트 불러오기 완료");
	}
	
	
	
	private void saveDataToDB() {
//		Log.d(TAG, "노트 저장 시작");
		
		//Write to DB Process...
//		Log.d(TAG, "저장할 날짜 : " + mStartDay + ", mLoadDbFlag : " + mLoadDbFlag);
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		// 같은 날짜의 키워드 노트가 존재 한다면, 해당 노트를 불러온 뒤 테이블에서 데이터를 삭제한다. (덮어쓰기가 아닌 지우고 새로 저장하기)
		if(mLoadDbFlag) {
			Utils.deleteNote(mContext, mStartDay, mKeyword);
		}
		
		byte[] objectBytes = Utils.serializeObject(mNoteView.getConvertedNoteViews());
		
		ContentValues noteValues = new ContentValues();
		noteValues.put(Constants.NOTE_COLUMN_DATE, mStartDay);
		noteValues.put(Constants.NOTE_COLUMN_KEYWORD, mKeyword);
		noteValues.put(Constants.NOTE_COLUMN_GRAPHICS, objectBytes);
		
		sdb.insert(Constants.TABLE_IDEA_NOTE, null, noteValues);
		
//		Log.d(TAG, "노트 저장 완료");
		
		checkNoteChallenge(sdb);
		sdb.close();
	}
	
	private void setKeywordViewer(String keyword) {
		Display display = ((WindowManager)getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
    	int displayWidth = display.getWidth();
		float textSize = Utils.setTextViewTextSize(mKeywordViewer, displayWidth, mKeyword);
		
		mKeywordViewer.setText(keyword);
		mKeywordViewer.setTextSize(TypedValue.COMPLEX_UNIT_SP, Constants.DEFAULT_KEYWORD_TEXTVIEW_SIZE - textSize);
	}
	
	private void refreshPageViewer() {
		mPageViewer.setText(mNoteView.getCurrentPageNumber() + " / " + mNoteView.getTotalPageNumber());
	}
	
	private void setMode(int mode) {
		mNoteView.setMode(mode);
		
		switch (mode) {
		case NoteView.VIEW_MODE:
			mMainMenuBar.setBackgroundColor(Color.parseColor("#32A4E0"));
			mMainMenuBar.setVisibility(View.GONE);
			mSubMenuBar.setVisibility(View.GONE);
			mTextMenuBar.setVisibility(View.GONE);
			mDrawMenuBar.setVisibility(View.GONE);
			mEraseMenuBar.setVisibility(View.GONE);
			break;
		case NoteView.IDLE_MODE:
			mMainMenuBar.setBackgroundColor(Color.parseColor("#32A4E0"));
			mMainMenuBar.setVisibility(View.VISIBLE);
			mSubMenuBar.setVisibility(View.GONE);
			mTextMenuBar.setVisibility(View.GONE);
			mDrawMenuBar.setVisibility(View.GONE);
			mEraseMenuBar.setVisibility(View.GONE);
			break;
		case NoteView.DRAW_MODE:
			mMainMenuBar.setBackgroundResource(R.drawable.note_edit_menu_background);
			mMainMenuBar.setVisibility(View.VISIBLE);
			mSubMenuBar.setVisibility(View.VISIBLE);
			mTextMenuBar.setVisibility(View.GONE);
			mDrawMenuBar.setVisibility(View.VISIBLE);
			mEraseMenuBar.setVisibility(View.GONE);
			break;
		case NoteView.ERASE_MODE:
			mMainMenuBar.setBackgroundResource(R.drawable.note_edit_menu_background);
			mMainMenuBar.setVisibility(View.VISIBLE);
			mSubMenuBar.setVisibility(View.VISIBLE);
			mTextMenuBar.setVisibility(View.GONE);
			mDrawMenuBar.setVisibility(View.GONE);
			mEraseMenuBar.setVisibility(View.VISIBLE);
			break;
		case NoteView.TEXT_MODE:
			mMainMenuBar.setBackgroundResource(R.drawable.note_edit_menu_background);
			mMainMenuBar.setVisibility(View.VISIBLE);
			mSubMenuBar.setVisibility(View.VISIBLE);
			mTextMenuBar.setVisibility(View.VISIBLE);
			mDrawMenuBar.setVisibility(View.GONE);
			mEraseMenuBar.setVisibility(View.GONE);
			break;
		}
	}
	
	private static final int TOUCH_DELAY = 500;
	private long mPreTouchEventOccurredTime = 0;
	
	private class ImageButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			mMainLayout.requestFocusFromTouch();
			
			switch (v.getId()) {
			case R.id.note_edit_pagePrev:
				if ((System.currentTimeMillis() - mPreTouchEventOccurredTime) > TOUCH_DELAY) {
					mPreTouchEventOccurredTime = System.currentTimeMillis();
					mNoteView.movePage(NoteView.MOVE_PREV);
					refreshPageViewer();
				}
				break;
			case R.id.note_edit_pageNext:
				if ((System.currentTimeMillis() - mPreTouchEventOccurredTime) > TOUCH_DELAY) {
					mPreTouchEventOccurredTime = System.currentTimeMillis();
					mNoteView.movePage(NoteView.MOVE_NEXT);
					refreshPageViewer();
				}
				break;
			case R.id.note_edit_addPage:
				if ((System.currentTimeMillis() - mPreTouchEventOccurredTime) > TOUCH_DELAY) {
					if (mNoteView.getTotalPageNumber() < MAX_PAGE_NUMBER) {
						mPreTouchEventOccurredTime = System.currentTimeMillis();
						mNoteView.addPage();
						refreshPageViewer();
					} else {
						if(!mPageToastFlag) {
							Toast.makeText(mContext, getString(R.string.toast_title_max_note_page), Toast.LENGTH_SHORT).show();
							
							Thread pageThread = new Thread(new Runnable() {
								public void run() {
									mPageToastFlag = true;
									SystemClock.sleep(Constants.BUTTON_SAFE_DELAY);
									mPageToastFlag = false;
								}
							});
							
							pageThread.start();
						}
					}
				}
				break;
			case R.id.note_edit_delPage:
				mNoteView.deletePage();
				refreshPageViewer();
				break;
			case R.id.note_edit_text:
				if (mIsTextSelected) {
					mTextToolButton.setBackgroundResource(R.drawable.note_text);
					setMode(NoteView.IDLE_MODE);
				} else {
					if (mIsDrawSelected) {
						mIsDrawSelected = !mIsDrawSelected;
						mDrawToolButton.setBackgroundResource(R.drawable.note_draw);
					} else if (mIsEraseSelected) {
						mIsEraseSelected = !mIsEraseSelected;
						mEraseToolButton.setBackgroundResource(R.drawable.note_erase);
					}
					
					mTextToolButton.setBackgroundResource(R.drawable.note_text_selected);
					setMode(NoteView.TEXT_MODE);
				}
				
				mIsTextSelected = !mIsTextSelected;
				break;
			case R.id.note_edit_draw:
				if (mIsDrawSelected) {
					mDrawToolButton.setBackgroundResource(R.drawable.note_draw);
					setMode(NoteView.IDLE_MODE);
				} else {
					if (mIsTextSelected) {
						mIsTextSelected = !mIsTextSelected;
						mTextToolButton.setBackgroundResource(R.drawable.note_text);
					} else if (mIsEraseSelected) {
						mIsEraseSelected = !mIsEraseSelected;
						mEraseToolButton.setBackgroundResource(R.drawable.note_erase);
					}
					
					mDrawToolButton.setBackgroundResource(R.drawable.note_draw_selected);
					setMode(NoteView.DRAW_MODE);
				}
				
				mIsDrawSelected = !mIsDrawSelected;
				break;
			case R.id.note_edit_eraser:
				if (mIsEraseSelected) {
					mEraseToolButton.setBackgroundResource(R.drawable.note_erase);
					setMode(NoteView.IDLE_MODE);
				} else {
					if (mIsTextSelected) {
						mIsTextSelected = !mIsTextSelected;
						mTextToolButton.setBackgroundResource(R.drawable.note_text);
					} else if (mIsDrawSelected) {
						mIsDrawSelected = !mIsDrawSelected;
						mDrawToolButton.setBackgroundResource(R.drawable.note_draw);
					}
					
					mEraseToolButton.setBackgroundResource(R.drawable.note_erase_selected);
					setMode(NoteView.ERASE_MODE);
				}
				
				mIsEraseSelected = !mIsEraseSelected;
				break;
			}
		}
	}
	
	private class CheckBoxEventHandler implements CompoundButton.OnCheckedChangeListener {
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			mMainLayout.requestFocusFromTouch();
			
			switch (buttonView.getId()) {
			case R.id.note_edit_text_style_bold:
				mNoteView.setTextBoldStyle(isChecked);
				break;
			case R.id.note_edit_text_style_italic:
				mNoteView.setTextItalicStyle(isChecked);
				break;
			case R.id.note_edit_text_style_underline:
				mNoteView.setTextUnderlineStyle(isChecked);
				break;
			}
		}
	}

	private class RadioGroupEventHandler implements RadioGroup.OnCheckedChangeListener {
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			mMainLayout.requestFocusFromTouch();
			
			switch (group.getId()) {
			case R.id.note_edit_text_color_group:
				switch (checkedId) {
				case R.id.note_edit_text_color_black:
					mNoteView.setTextColor(Color.BLACK);
					break;
				case R.id.note_edit_text_color_red:
					mNoteView.setTextColor(Color.RED);
					break;
				case R.id.note_edit_text_color_blue:
					mNoteView.setTextColor(Color.BLUE);
					break;
				case R.id.note_edit_text_color_yellow:
					mNoteView.setTextColor(Color.YELLOW);
					break;
				}
				break;
			case R.id.note_edit_draw_color_group:
				switch (checkedId) {
				case R.id.note_edit_draw_color_black:
					mNoteView.setDrawColor(Color.BLACK);
					break;
				case R.id.note_edit_draw_color_red:
					mNoteView.setDrawColor(Color.RED);
					break;
				case R.id.note_edit_draw_color_blue:
					mNoteView.setDrawColor(Color.BLUE);
					break;
				case R.id.note_edit_draw_color_yellow:
					mNoteView.setDrawColor(Color.YELLOW);
					break;
				}
				break;
			case R.id.note_edit_draw_width_group:
				switch (checkedId) {
				case R.id.note_edit_draw_width_thin:
					mNoteView.setDrawStrokeWidth(NoteView.DRAW_WIDTH_THIN);
					break;
				case R.id.note_edit_draw_width_normal:
					mNoteView.setDrawStrokeWidth(NoteView.DRAW_WIDTH_NORMAL);
					break;
				case R.id.note_edit_draw_width_thick:
					mNoteView.setDrawStrokeWidth(NoteView.DRAW_WIDTH_THICK);
					break;
				}
				break;
			case R.id.note_edit_erase_width_group:
				switch (checkedId) {
				case R.id.note_edit_erase_width_thin:
					mNoteView.setEraseStrokeWidth(NoteView.ERASE_WIDTH_THIN);
					break;
				case R.id.note_edit_erase_width_normal:
					mNoteView.setEraseStrokeWidth(NoteView.ERASE_WIDTH_NORMAL);
					break;
				case R.id.note_edit_erase_width_thick:
					mNoteView.setEraseStrokeWidth(NoteView.ERASE_WIDTH_THICK);
					break;
				}
			}
		}
	}
	
	// 노트와 관련된 도전과제를 체크하는 함수
	private void checkNoteChallenge(SQLiteDatabase sdb) {
		SharedPreferences.Editor editor = mConfigPref.edit();
		
		// 초급,중급,고급 기획자 도전과제를 체크한다.
		String noteCountSQl = "SELECT * FROM " + Constants.TABLE_IDEA_NOTE + " ORDER BY " + Constants.NOTE_COLUMN_DATE + " ASC";
		Cursor countCursor = sdb.rawQuery(noteCountSQl, null);
		int noteCount = countCursor.getCount();
		
		// 근면성실과 지성이면 감천 도전과제를 체크한다.
		if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_HARD_WORK, false)
				|| !mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_NO_PAIN_NO_GAIN, false)) {
			
			// 연속된 날짜를 비교하기 전 미리 복사를 해둔다.
			ArrayList<String> dateItems = new ArrayList<String>();
			
			while(countCursor.moveToNext()) {
				dateItems.add(countCursor.getString(1));
			}
			
			// 날짜의 연속성을 파악한다.
			SimpleDateFormat gapFormat = new SimpleDateFormat("yyyyMMdd");
			int gapCount = 1;
			
//			Log.d(TAG, "dateItems size : " + dateItems.size());
			
			for(int i=0; i < dateItems.size(); i++) {
				if((i+1) < dateItems.size()) {
					if(Utils.gapOfDate(gapFormat, dateItems.get(i), dateItems.get(i+1)) == 1) {
						gapCount++;
					} else {
						gapCount = 1;
					}

					// 지성이면 감천 타이틀 성공 시,
					if(gapCount == Constants.MAX_NO_PAIN_NO_GAIN) {
						if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_NO_PAIN_NO_GAIN, false)) {
							Utils.updateChallengeState(mContext, Constants.CHALLENGE_NO_PAIN_NO_GAIN);
							
							editor.putBoolean(Constants.COMPLETE_CHALLENGE_NO_PAIN_NO_GAIN, true);
							editor.commit();
							
							Toast.makeText(mContext, getString(R.string.complete_challenge_no_pain_no_gain), Toast.LENGTH_SHORT).show();
							
						}
					// 근면성실 타이틀 성공 시,
					} else if(gapCount == Constants.MAX_HARD_WORK) {
						if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_HARD_WORK, false)) {
							Utils.updateChallengeState(mContext, Constants.CHALLENGE_HARD_WORK);
							
							editor.putBoolean(Constants.COMPLETE_CHALLENGE_HARD_WORK, true);
							editor.commit();
							
							Toast.makeText(mContext, getString(R.string.complete_challenge_hard_work), Toast.LENGTH_SHORT).show();
						}
					}
				}
			}
		}
		
		// 사용된 커서를 닫아준다.
		countCursor.close();

		// 초급 기획자 도전과제 체크
		if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_BEGINNER_PLANNER, false)) {
			// 초급 기획자 도전과제를 성공하였다면,
			if(noteCount == Constants.MAX_BEGINNER_PLANNER) {
				Utils.updateChallengeState(mContext, Constants.CHALLENGE_BEGINNER_PLANNER);

				editor.putBoolean(Constants.COMPLETE_CHALLENGE_BEGINNER_PLANNER, true);
				editor.commit();
				
				Toast.makeText(mContext, getString(R.string.complete_challenge_beginner_planner), Toast.LENGTH_SHORT).show();
			}
		}
		
		// 중급 기획자 도전과제 체크
		if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_INTERMEDIATE_PLANNER, false)) {
			// 중급 기획자 도전과제를 성공하였다면,
			if(noteCount == Constants.MAX_INTERMEDIATE_PLANNER) {
				Utils.updateChallengeState(mContext, Constants.CHALLENGE_INTERMEDIATE_PLANNER);

				editor.putBoolean(Constants.COMPLETE_CHALLENGE_INTERMEDIATE_PLANNER, true);
				editor.commit();
				
				Toast.makeText(mContext, getString(R.string.complete_challenge_intermediate_planner), Toast.LENGTH_SHORT).show();
			}
		}
		
		// 고급 기획자 도전과제 체크
		if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_ADVANCED_PLANNER, false)) {
			// 고급 기획자 도전과제를 성공하였다면,
			if(noteCount == Constants.MAX_ADVANCED_PLANNER) {
				Utils.updateChallengeState(mContext, Constants.CHALLENGE_ADVANCED_PLANNER);

				editor.putBoolean(Constants.COMPLETE_CHALLENGE_ADVANCED_PLANNER, true);
				editor.commit();
				
				Toast.makeText(mContext, getString(R.string.complete_challenge_advanced_planner), Toast.LENGTH_SHORT).show();
			}
		}
		
		// 아이디어 뱅크 도전과제 체크
		if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_IDEA_BANK, false)) {
			// 아이디어 뱅크
			if(noteCount == Constants.MAX_IDEA_BANK) {
				Utils.updateChallengeState(mContext, Constants.CHALLENGE_IDEA_BANK);

				editor.putBoolean(Constants.COMPLETE_CHALLENGE_IDEA_BANK, true);
				editor.commit();
				
				Toast.makeText(mContext, getString(R.string.complete_challenge_idea_bank), Toast.LENGTH_SHORT).show();
			}
		}
		
		// 질보단 양 도전과제 체크
		if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_AMOUNT_OF_QUALITY, false)) {
			// 질보단 양
			if(mNoteView.getTotalPageNumber() == Constants.MAX_AMOUNT_OF_QUALITY) {
				Utils.updateChallengeState(mContext, Constants.CHALLENGE_AMOUNT_OF_QUALITY);
				
				editor.putBoolean(Constants.COMPLETE_CHALLENGE_AMOUNT_OF_QUALITY, true);
				editor.commit();
				
				Toast.makeText(mContext, getString(R.string.complete_challenge_amount_of_quality), Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(mCallerActivity.equals(Constants.CALLER_CREATE_IDEA)) {
			final NoSaveNoteExit noteSaveDialog = new NoSaveNoteExit(mContext, R.style.PopupDialog);
			noteSaveDialog.show();
			
			noteSaveDialog.setOnDismissListener(new OnDismissListener() {
				public void onDismiss(DialogInterface dialog) {

					switch(noteSaveDialog.getDialogFlag()) {
					case 1:			//	NO SAVE AND EXIT
						mMessagePool.setNoteWritingFlag(false);
						mSaveExitFlag = false;

						onPause();
						
						break;
					case 2:			// SAVE AND EXIT
						mMessagePool.setNoteWritingFlag(false);
						mSaveExitFlag = true;

						onPause();
						
						break;
					default:
						break;
					}
				}
			});
		} else if(mCallerActivity.equals(Constants.CALLER_MANAGE_IDEA)) {
			super.onBackPressed();
			overridePendingTransition(R.anim.fade, R.anim.fade_out);
		}
	}
}
