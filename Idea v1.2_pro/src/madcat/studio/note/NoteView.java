package madcat.studio.note;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.idea.R;
import madcat.studio.note.convert.ConvertedNoteEditText;
import madcat.studio.note.convert.ConvertedNoteView;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class NoteView extends RelativeLayout {
//	private static final String TAG = "NoteView";
	
	private static final int MAX_PAGE_NUMBER = Constants.MAX_NOTE_PAGE;
	
	public static final int VIEW_MODE	= 0;
	public static final int IDLE_MODE	= 1;
	public static final int DRAW_MODE	= 2;
	public static final int ERASE_MODE	= 3;
	public static final int TEXT_MODE	= 4;
	
	public static final float DRAW_WIDTH_THIN		= 4;
	public static final float DRAW_WIDTH_NORMAL		= 8;
	public static final float DRAW_WIDTH_THICK		= 12;
	
	public static final float ERASE_WIDTH_THIN		= 15;
	public static final float ERASE_WIDTH_NORMAL	= 30;
	public static final float ERASE_WIDTH_THICK		= 45;
	
	private Context mContext = null;
	
	//NOTE PROPERTIES.
	private ArrayList<ConvertedNoteView> mConvertedNoteViews = null;
	private int mIndex = 0;
	
	//GRAPHICS PROPERTIES.
	private Path mPath = null;
	private Paint mDrawPaint = null;
	private Paint mErasePaint = null;
	
	private Bitmap mBitmap = null;
	private Paint mBitmapPaint = null;
	private Canvas mBitmapCanvas = null;
	
	private int mBackgroundColor = Color.WHITE;
	
	//TEXTURES PROPERTIES.
	private ArrayList<NoteEditText> mNoteEditTexts = null;
	
	private float mTextSize = 20;
	private int mTextColor = Color.BLACK;
	private boolean mTextBold = false;
	private boolean mTextItalic = false;
	private boolean mTextUnderLine = false;
	
	//FLAGS.
	private int mMode = IDLE_MODE;
	private boolean mErased = false;
	
	public NoteView(Context context) {
		// TODO Auto-generated constructor stub
		super(context);
		mContext = context;
	}
	
	public NoteView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}
	
	public NoteView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
	}
	
	public void setInputMode(int mode) {
		mMode = mode;
	}
	
	public void initialize() {
		//NOTE PROPERTIES INITIALIZE.
	    if (mConvertedNoteViews == null)
	    	mConvertedNoteViews = new ArrayList<ConvertedNoteView>();
		
		//GRAPHICS PROPERTIES INITIALIZE.
		mPath = new Path();
		if (mDrawPaint == null) {
			mDrawPaint = new Paint();
			
			mDrawPaint.setAntiAlias(true);
			mDrawPaint.setDither(true);
			mDrawPaint.setStyle(Paint.Style.STROKE);
			mDrawPaint.setStrokeJoin(Paint.Join.ROUND);
			mDrawPaint.setStrokeCap(Paint.Cap.ROUND);
			
			mDrawPaint.setColor(Color.BLACK);
		    mDrawPaint.setStrokeWidth(DRAW_WIDTH_NORMAL);
		}
		
		if (mErasePaint == null) {
			mErasePaint = new Paint();
			
			mErasePaint.setAntiAlias(true);
	    	mErasePaint.setDither(true);
	    	mErasePaint.setStyle(Paint.Style.STROKE);
	    	mErasePaint.setStrokeJoin(Paint.Join.ROUND);
	    	mErasePaint.setStrokeCap(Paint.Cap.ROUND);
			
	    	mErasePaint.setColor(Color.BLACK);
	    	mErasePaint.setStrokeWidth(ERASE_WIDTH_NORMAL);
			
	    	mErasePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
		}
		
		if (mBitmapPaint == null) {
			mBitmapPaint = new Paint();
	    	
	    	mBitmapPaint.setAntiAlias(true);
	    	mBitmapPaint.setDither(true);
	    	mBitmapPaint.setStyle(Paint.Style.STROKE);
	    	mBitmapPaint.setStrokeJoin(Paint.Join.ROUND);
	    	mBitmapPaint.setStrokeCap(Paint.Cap.ROUND);
			
	    	mBitmapPaint.setColor(Color.WHITE);
	    	mBitmapPaint.setStrokeWidth(6);
		}
		
		//TEXTURES PROPERTIES INITIALIZE.
		if (mNoteEditTexts == null)
			mNoteEditTexts = new ArrayList<NoteEditText>();
		
		setBackgroundColor(mBackgroundColor);
		
		//Add Page.
		if (mConvertedNoteViews.isEmpty())
			mConvertedNoteViews.add(new ConvertedNoteView());

		mIndex = 0;
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		// TODO Auto-generated method stub
		if (mBitmap == null) {
			mBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
			mBitmapCanvas = new Canvas(mBitmap);
		}
		
		super.onLayout(changed, l, t, r, b);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		canvas.drawColor(mBackgroundColor);
		canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
			
		if (mMode != ERASE_MODE)
			canvas.drawPath(mPath, mDrawPaint);
	}
	
	private float DISTANCE_TOLERANCE = 5;
	private float preX, preY;
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		float x = event.getX();
	    float y = event.getY();
	    
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			switch (mMode) {
			case VIEW_MODE:
			case IDLE_MODE:
				break;
			case DRAW_MODE:
			case ERASE_MODE:
				mPath.moveTo(x, y);
				
				preX = x;
				preY = y;
				
				invalidate();
				break;
			case TEXT_MODE:
				preX = event.getX();
				preY = event.getY();
				break;
			}
			break;
		case MotionEvent.ACTION_MOVE:
			switch (mMode) {
			case DRAW_MODE:
				if (Math.abs(x - preX) >= DISTANCE_TOLERANCE || Math.abs(y - preY) >= DISTANCE_TOLERANCE) {
					mPath.quadTo(preX, preY, x, y);
					
					invalidate();
					
					preX = x;
					preY = y;
				}
				break;
			case ERASE_MODE:
				if (Math.abs(x - preX) >= DISTANCE_TOLERANCE || Math.abs(y - preY) >= DISTANCE_TOLERANCE) {
					mPath.lineTo(x, y);
					mBitmapCanvas.drawPath(mPath, mErasePaint);
					
					invalidate();
					
					mPath.reset();
					mPath.moveTo(x, y);
					
					preX = x;
					preY = y;
				}
				break;
			}
			break;
		case MotionEvent.ACTION_UP:
			switch (mMode) {
			case DRAW_MODE:
				mPath.lineTo(x, y);
				mBitmapCanvas.drawPath(mPath, mDrawPaint);
				mPath.reset();
				
				invalidate();
				break;
			case ERASE_MODE:
				mPath.lineTo(x, y);
				mBitmapCanvas.drawPath(mPath, mErasePaint);
				mPath.reset();
				
				invalidate();
				break;
			case TEXT_MODE:
				boolean hasFocused = false;
				
				for (NoteEditText editText : mNoteEditTexts) {
					if (editText.hasFocus()) {
						hasFocused = true;
					}
				}
				
				if (!hasFocused) {
					float dx = Math.abs(preX - event.getX());
					float dy = Math.abs(preY - event.getY());
					
					if (dx < 20 && dy < 20) {
						NoteEditText editText = new NoteEditText(mContext, (int)x, (int)y);
						
						editText.setTextSize(mTextSize);
						editText.setTextColor(mTextColor);
						
						if (mTextBold) {
							if (mTextItalic) {
								if (mTextUnderLine) {
									editText.setTextStyle(NoteEditText.BOLD|NoteEditText.ITALIC|NoteEditText.UNDER_LINE);
								} else {
									editText.setTextStyle(NoteEditText.BOLD|NoteEditText.ITALIC);
								}
							} else {
								if (mTextUnderLine) {
									editText.setTextStyle(NoteEditText.BOLD|NoteEditText.UNDER_LINE);
								} else {
									editText.setTextStyle(NoteEditText.BOLD);
								}
							}
						} else {
							if (mTextItalic) {
								if (mTextUnderLine) {
									editText.setTextStyle(NoteEditText.ITALIC|NoteEditText.UNDER_LINE);
								} else {
									editText.setTextStyle(NoteEditText.ITALIC);
								}
							} else {
								if (mTextUnderLine) {
									editText.setTextStyle(NoteEditText.UNDER_LINE);
								} else {
									editText.setTextStyle(NoteEditText.NORMAL);
								}
							}
						}
						
						addNoteEditText(editText);
					}
				} else {
					clearFocus();
				}
				break;
			}
			break;
		}
		return true;
	}
	
	//RESOURCE SET/GET METHODS.
	public void setConvertedNoteViews(ArrayList<ConvertedNoteView> convertedNoteViews) {
		clearPage();
		initialize();
		
		if (mConvertedNoteViews != null)
			mConvertedNoteViews = null;

		mConvertedNoteViews = new ArrayList<ConvertedNoteView>(convertedNoteViews);
		
		loadPage();
	}

	public ArrayList<ConvertedNoteView> getConvertedNoteViews() {
		savePage();
		return mConvertedNoteViews;
	}
	
	//VIEW SET/GET/IS METHODS.
	public void setMode(int mode) {
		mMode = mode;
		
		switch (mode) {
		case VIEW_MODE:
		case IDLE_MODE:
			setEnabled(false);
			for (NoteEditText editText : mNoteEditTexts) { editText.setEnabled(false); }
			break;
		case DRAW_MODE:
			setEnabled(true);
			for (NoteEditText editText : mNoteEditTexts) { editText.setEnabled(false); }
			break;
		case ERASE_MODE:
			setEnabled(true);
			for (NoteEditText editText : mNoteEditTexts) { editText.setEnabled(false); }
			break;
		case TEXT_MODE:
			setEnabled(true);
			for (NoteEditText editText : mNoteEditTexts) { editText.setEnabled(true); }
			break;
		}
	}

	public int getMode()						{ return mMode; }
	public int getCurrentPageNumber()			{ return mIndex + 1; }
	public int getTotalPageNumber()				{ return mConvertedNoteViews.size(); }
	
	//GRAPHICS SET/GET/IS METHODS.
	public void setDrawColor(int color) 		{ mDrawPaint.setColor(color); }
	public void setDrawStrokeWidth(float width) { mDrawPaint.setStrokeWidth(width); }
	public void setDrawStyle(int style) 		{ 	}
	
	public int   getDrawColor()					{ return mDrawPaint.getColor(); }
	public float getDrawStrokeWidth()			{ return mDrawPaint.getStrokeWidth(); }
	public int   getDrawStyle() 				{ return 0; }
	
	public void  setEraseStrokeWidth(float width)	{ mErasePaint.setStrokeWidth(width); }
	public float getEraseStrokeWidth()				{ return mErasePaint.getStrokeWidth(); }
	
	public boolean isErased()						{ return mErased; }
	
	//TEXTURES SET/GET METHODS.
	public void setTextSize(float textSize)					{ mTextSize = textSize; }
	public void setTextColor(int textColor)					{ mTextColor = textColor; }
	public void setTextBoldStyle(boolean bold)				{ mTextBold = bold; }
	public void setTextItalicStyle(boolean italic)			{ mTextItalic = italic; }
	public void setTextUnderlineStyle(boolean underLine)	{ mTextUnderLine = underLine; }
	
	public float   getTextSize()					{ return mTextSize; }
	public int     getTextColor()					{ return mTextColor; }
	public boolean isTextBoldStyle()				{ return mTextBold; }
	public boolean isTextItalicStyle()				{ return mTextItalic; }
	public boolean isTextUnderLineStyle()			{ return mTextUnderLine; }
	
	//VIEW ADD/REMOVE METHODS.
	public void addAllNoteEditText(ArrayList<NoteEditText> editTexts) {
		if (editTexts != null) {
			for (NoteEditText editText : editTexts)
				addView(editText);
		}
	}
	
	public void addNoteEditText(NoteEditText editText) {
		// TODO Auto-generated method stub
		if (editText != null) {
			mNoteEditTexts.add(editText);
			addView(editText);
			editText.requestFocusFromTouch();
		}
	}
	
	public void removeAllNoteEditText() {
		if (mNoteEditTexts != null) {
			removeAllViews();
		}
	}
	
	public void removeNoteEditText(NoteEditText editText) {
		if (mNoteEditTexts != null) {
			mNoteEditTexts.remove(editText);
			removeView(editText);
		}
	}
	
	//PAGE CONTROL METHODS.
	private void clearPage() {
		if (mBitmap != null) {
			mBitmap.recycle();
			mBitmap = null;
		}
		
		if (mBitmapCanvas != null)
			mBitmapCanvas = null;
		
		if (mNoteEditTexts != null) {
			mNoteEditTexts.clear();
			removeAllNoteEditText();
		}
	}
	
	private void savePage() {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		mBitmap.compress(CompressFormat.PNG, 100, outputStream);
		byte[] bitmapBytes = outputStream.toByteArray();
		
		try {
			outputStream.close();
		} catch (IOException e) {
		} finally {
			outputStream = null;
		}
		
		ArrayList<ConvertedNoteEditText> convertedNoteEditTexts = new ArrayList<ConvertedNoteEditText>();
		
		for (NoteEditText editText : mNoteEditTexts)
			convertedNoteEditTexts.add(new ConvertedNoteEditText((int)editText.getX(), (int)editText.getY(), editText.getString(), editText.getTextSize(), editText.getTextColor(), editText.getTextStyle()));
		
		mConvertedNoteViews.get(mIndex).setConvertedBitmapBytes(bitmapBytes);
		mConvertedNoteViews.get(mIndex).setConvertedEditTexts(convertedNoteEditTexts);
	}
	
	private void loadPage() {
		byte[] bitmapBytes =  mConvertedNoteViews.get(mIndex).getConvertedBitmapBytes();
		mBitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length).copy(Bitmap.Config.ARGB_8888, true);
		mBitmapCanvas = new Canvas(mBitmap);
		
		for (ConvertedNoteEditText convertedNoteEditText : mConvertedNoteViews.get(mIndex).getConvertedNoteEditTexts())
			mNoteEditTexts.add(new NoteEditText(mContext, convertedNoteEditText.getX(), convertedNoteEditText.getY(), convertedNoteEditText.getText(), convertedNoteEditText.getTextSize(), convertedNoteEditText.getTextColor(), convertedNoteEditText.getTextStyle()));

		addAllNoteEditText(mNoteEditTexts);
	}
	
	
	public boolean addPage() {
		if (mConvertedNoteViews.size() < MAX_PAGE_NUMBER) {
			//Save Current Page
			savePage();
			//Clean Up Page
			clearPage();
			//Page Initialize
			mBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
			mBitmapCanvas = new Canvas(mBitmap);
			
			//Save New Page
			if (mIndex++ == (mConvertedNoteViews.size() - 1)) {
				mConvertedNoteViews.add(new ConvertedNoteView());
			} else {
				mConvertedNoteViews.add(mIndex, new ConvertedNoteView());
			}
			
			//Re-Draw Page
			invalidate();
			
			return true;
		} else {
			return false;	//NO MORE ADD PAGE.
		}
	}
	
	public boolean deletePage() {
		if (mConvertedNoteViews.size() > 1) {
			//Remove Current Page
			mConvertedNoteViews.remove(mIndex);
			
			//Clean Up Page
			clearPage();
			
			//Load Prev or Next Page
			if (mIndex == mConvertedNoteViews.size())
				mIndex--;
			
			loadPage();
			
			//Re-Draw Page
			invalidate();
			
			return true;
		} else {
			clearPage();
			
			if (!mNoteEditTexts.isEmpty())
				mNoteEditTexts.clear();
			
			//Page Initialize
			mBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
			mBitmapCanvas = new Canvas(mBitmap);
			
			//Re-Draw Page
			invalidate();
			
			return false;	//NO MORE DELETE PAGE.
		}
	}
	
	public static final boolean MOVE_PREV = true;
	public static final boolean MOVE_NEXT = false;
	
	public boolean movePage(boolean move) {
		//Save Current Page
		savePage();
		
		if (move) {		//MOVE TO PREVIOUS PAGE
			if (mIndex > 0) {
				//Clean Up View
				clearPage();
				
				//Load Prev Page
				mIndex--;
				loadPage();
				
				//Re-Draw Page
				invalidate();
				
				return true;
			} else {
				return false;
			}
		} else {		//MOVE TO NEXT PAGE
			if (mIndex < mConvertedNoteViews.size() - 1) {
				//Clean Up View
				clearPage();
				
				//Load Next Page
				mIndex++;
				loadPage();
				
				//Re-Draw Page
				invalidate();
				
				return true;
			} else {
				return false;
			}
		}
	}
	
//	private void isBitmapEmpty() {
//		long currentTime = System.currentTimeMillis();
//		
//		int[] bitmapPixels = new int[mBitmap.getWidth() * mBitmap.getHeight()];
//		mBitmap.getPixels(bitmapPixels, 0, mBitmap.getWidth(), 0, 0, mBitmap.getWidth(), mBitmap.getHeight());
//		
//		int check = 0;
//		
//		for (int i = 0; i < bitmapPixels.length; i++) {
//			if (bitmapPixels[i] == 0) {
//				check++;
//			}
//		}
//		
//		Log.d("Check", bitmapPixels.length+"");
//		Log.d("Check", check+"");
//		
//		if (bitmapPixels.length == check)
//			Log.d("Check", "Is Blank");
//		else 
//			Log.d("Check", "Is Not Blank");
//		
//		Log.d("Check", "Spent Time : " + (System.currentTimeMillis() - currentTime) + "ms");
//	}
	
	public class NoteEditText extends EditText implements OnClickListener, OnFocusChangeListener {
		private int mX, mY;
		private float mSize;
		private int mColor;
		private int mStyle;
		
		public NoteEditText(Context context, int x, int y) {
			// TODO Auto-generated constructor stub
			super(context);
			
			setXY(x, y);
			setPadding(5, 5, 5, 5);
			setBackgroundColor(Color.TRANSPARENT);
			
			setOnClickListener(this);
			setOnFocusChangeListener(this);
		}
		
		public NoteEditText(Context context, int x, int y, String text, float textSize, int textColor, int textStyle) {
			this(context, x, y);
			
			setTextSize(textSize);
			setTextColor(textColor);
			setTextStyle(textStyle);
			
			if (mStyle == UNDER_LINE || mStyle == BOLD_UNDER_LINE || mStyle == ITALIC_UNDER_LINE || mStyle == BOLD_ITALIC_UNDER_LINE) {
				setUnderLineText(text);
			} else {
				setText(text);
			}
		}
		
		public void setXY(int x, int y) {
			mX = x;
			mY = y;
			setMargin();
		}
		
		public void setX(int x) {
			mX = x;
			setMargin();
		}
		
		public void setY(int y) {
			mY = y;
			setMargin();
		}
		
		private void setMargin() {
			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.setMargins(mX, mY, 0, 0);
			setLayoutParams(params);
		}
		
		@Override
		public void setTextSize(float size) {
			// TODO Auto-generated method stub
			super.setTextSize(size);
			mSize = size;
		}
		
		@Override
		public void setTextColor(int color) {
			super.setTextColor(color);
			mColor = color;
		}
		
		public static final int NORMAL 					= 0;
		public static final int BOLD 					= 1;
		public static final int ITALIC 					= 2;
		public static final int UNDER_LINE				= 4;
		
		public static final int BOLD_ITALIC			= 3;
		public static final int BOLD_UNDER_LINE		= 5;
		public static final int ITALIC_UNDER_LINE		= 6;
		public static final int BOLD_ITALIC_UNDER_LINE	= 7;
		
		public void setTextStyle(int style) {
			mStyle = style;
			
			switch (mStyle) {
			case NORMAL:
				setTypeface(getTypeface(), Typeface.NORMAL);
				break;
			case BOLD:
			case BOLD_UNDER_LINE:
				setTypeface(getTypeface(), Typeface.BOLD);
				break;
			case ITALIC:
			case ITALIC_UNDER_LINE:
				setTypeface(getTypeface(), Typeface.ITALIC);
				break;
			case BOLD_ITALIC:
			case BOLD_ITALIC_UNDER_LINE:
				setTypeface(getTypeface(), Typeface.BOLD|Typeface.ITALIC);
				break;
			}
		}
		
		public float    getX()			{ return mX; }
		public float    getY()			{ return mY; }
		public String getString()		{ return getText().toString(); }
		public float  getTextSize()		{ return mSize; }
		public int    getTextColor()	{ return mColor; }
		public int    getTextStyle()	{ return mStyle; }
		
		private void setUnderLineText(CharSequence text) {
			SpannableString content = new SpannableString(text);
			content.setSpan(new UnderlineSpan(), 0, text.length(), 0);
			setText(content, BufferType.SPANNABLE);
		}
		
		public void onClick(View v) {
			// TODO Auto-generated method stub
			requestFocusFromTouch();
		}
		
		public void onFocusChange(View v, boolean hasFocus) {
			// TODO Auto-generated method stub
			InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
			
			if (hasFocus) {
				imm.showSoftInput(this, 0);
				String text = getText().toString();
				int textLength = text.length();
				
				if (textLength > 0) {
					if (mStyle == ITALIC || mStyle == BOLD_ITALIC || mStyle == ITALIC_UNDER_LINE || mStyle == BOLD_ITALIC_UNDER_LINE) {
						setText(text.subSequence(0, textLength - 1));
						setSelection(textLength - 1);
					} else if (mStyle == UNDER_LINE || mStyle == BOLD_UNDER_LINE) {
						setText(text);
					} else {
						setSelection(textLength);
					}
				}
				
				setBackgroundResource(R.drawable.note_edittext_background);
			} else {
				imm.hideSoftInputFromWindow(getWindowToken(), 0);
				String text = getText().toString();
				
				if (text.trim().equals("")) {
					removeNoteEditText(this);
				} else {
					if (mStyle == ITALIC || mStyle == BOLD_ITALIC) {
						setText(text + " ");
					} else if (mStyle == UNDER_LINE || mStyle == BOLD_UNDER_LINE) {
						setUnderLineText(text);
					} else if (mStyle == ITALIC_UNDER_LINE || mStyle == BOLD_ITALIC_UNDER_LINE) {
						setUnderLineText(text+" ");
					}
				}
				
				setBackgroundColor(Color.TRANSPARENT);
			}
		}
		
		@Override
		public boolean onKeyPreIme(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
			if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP)
				this.clearFocus();
			
			return super.onKeyPreIme(keyCode, event);
		}
	}
}
