package madcat.studio.idea;

import madcat.studio.constants.Constants;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class Help extends Activity implements OnClickListener {
	
//	private final String TAG										=	"Help";
	private int mPageCount											=	1;
	
	private ImageButton mBtnPrev, mBtnNext;
	private ImageView mTextImage, mPageNumberImage;
	private Context mContext;
	
	private SharedPreferences mConfigPref;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.idea_help);
		
		mContext = this;
		mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		mBtnPrev = (ImageButton)findViewById(R.id.help_prev);
		mBtnNext = (ImageButton)findViewById(R.id.help_next);
		mTextImage = (ImageView)findViewById(R.id.help_text_image);
		mPageNumberImage = (ImageView)findViewById(R.id.help_page_number_image);
		
		mBtnPrev.setOnClickListener(this);
		mBtnNext.setOnClickListener(this);
	}
	
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.help_next:
			if(mPageCount < 7) {
				mPageCount++;
			}
			
			switch(mPageCount) {
				case 1:
					mTextImage.setBackgroundResource(R.drawable.help_1st_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_1st_page_number);
					mBtnPrev.setVisibility(View.INVISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 2:
					mTextImage.setBackgroundResource(R.drawable.help_2nd_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_2nd_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 3:
					mTextImage.setBackgroundResource(R.drawable.help_3rd_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_3rd_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 4:
					mTextImage.setBackgroundResource(R.drawable.help_4th_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_4th_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 5:
					mTextImage.setBackgroundResource(R.drawable.help_5th_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_5th_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 6:
					mTextImage.setBackgroundResource(R.drawable.help_6th_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_6th_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 7:
					mTextImage.setBackgroundResource(R.drawable.help_7th_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_7th_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.INVISIBLE);
					
					// 메뉴얼 정복자 도전 과제 체크
					if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_MANUAL_CONQUEROR, false)) {

						Utils.updateChallengeState(mContext, Constants.CHALLENGE_MANUAL_CONQUEROR);
						
						SharedPreferences.Editor editor = mConfigPref.edit();
						editor.putBoolean(Constants.COMPLETE_CHALLENGE_MANUAL_CONQUEROR, true);
						editor.commit();
						
						Toast.makeText(mContext, getString(R.string.complete_challenge_manual_conqueror), Toast.LENGTH_LONG).show();
					}
					
					break;
				default:
					break;
			}
			break;
		case R.id.help_prev:
			if(mPageCount > 0) {
				mPageCount--;
			}
			
			switch(mPageCount) {
				case 1:
					mTextImage.setBackgroundResource(R.drawable.help_1st_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_1st_page_number);
					mBtnPrev.setVisibility(View.INVISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 2:
					mTextImage.setBackgroundResource(R.drawable.help_2nd_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_2nd_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 3:
					mTextImage.setBackgroundResource(R.drawable.help_3rd_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_3rd_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 4:
					mTextImage.setBackgroundResource(R.drawable.help_4th_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_4th_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 5:
					mTextImage.setBackgroundResource(R.drawable.help_5th_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_5th_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 6:
					mTextImage.setBackgroundResource(R.drawable.help_6th_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_6th_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 7:
					mTextImage.setBackgroundResource(R.drawable.help_7th_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_7th_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.INVISIBLE);
				default:
					break;
			}
			
			break;
		}
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
