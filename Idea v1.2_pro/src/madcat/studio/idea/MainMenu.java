package madcat.studio.idea;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.dialogs.TutorialDialog;
import madcat.studio.dialogs.UpdateDialog;
import madcat.studio.keyword.Keyword;
import madcat.studio.note.NoteActivity;
import madcat.studio.note.list.NoteGroupList;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import madcat.studio.widget.IdeaWidget;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainMenu extends Activity implements OnClickListener, OnTouchListener {
	
	private final String TAG										=	"MainMenu";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private int DEFAULT_TEXT_COLOR									=	Color.rgb(50, 165, 224);
	private int MAX_REFRESH_COUNT									=	99999;
	
	private Context mContext;
	private ImageButton mBtnCreateIdea, mBtnManageIdea, mBtnConfig, mBtnHelp, mBtnRefresh, mBtnChallenge;
	private ImageView mImageTopClosed;
	private ImageView[] mKeywordPin;
	private TextView[] mTextKeyword;
	private SharedPreferences mConfigPref;
	
	private String mSendKeyword;
	
	private boolean mTodayRunCheck									=	false;
	
	private int mTodayRefreshCount									=	0;
	private int mDisplayKeywordSize;
	
	private ArrayList<String> mTodayKeyword;
	private String[] mSelectedCategory;
	private ArrayList<Keyword>[] mLoadKeywordText;
	private ArrayList<Integer> mLoadRandomIndex;

	// 키워드 터치 변수
	private boolean mTouchFlag_1									=	false;
	private boolean mTouchFlag_2									=	false;
	private boolean mTouchFlag_3									=	false;
	private int mCalculateIndex										=	0;
	
	// 도전과제 (섞어야 제 맛) 변수
	private int mChallengeRefreshCount								=	0;
	
	// 도전과제 (시간과 정신의 방) 변수
	private Thread mChallengeTimeThread								=	null;
	private boolean mChallengeTimeFlag								=	false;
	private int mChallengeTimeCount = 0;
	private Handler mChallengeTimeHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if(mChallengeTimeCount == Constants.MAX_CHALLENGE_TIME) {
				Toast.makeText(mContext, getString(R.string.complete_challenge_keyword_time_view), Toast.LENGTH_SHORT).show();
				mChallengeTimeFlag = true;
			}
		};
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onCreate 호출");
		}
		
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.idea_mainmenu);
		
		// 로딩에서 넘어온 당일 처음 실행인지에 대한 플래그 값을 저장
		this.getIntenter();
		
		mContext = this;
		mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		mTextKeyword = new TextView[Constants.MAX_KEYWORD_TEXTVIEW];
		mKeywordPin = new ImageView[Constants.MAX_KEYWORD_TEXTVIEW];
		
		for(int i=0; i < Constants.MAX_KEYWORD_TEXTVIEW; i++) {
			// TextView 배열 Resource Id 설정
			int resId = getResources().getIdentifier("mainmenu_keyword_" + (i+1), "id", getPackageName());
			mTextKeyword[i] = (TextView)findViewById(resId);
			mTextKeyword[i].setPaintFlags(mTextKeyword[i].getPaintFlags() | Paint.FAKE_BOLD_TEXT_FLAG);
			mTextKeyword[i].setOnTouchListener(this);
			
			// 핀 이미지 ResourceId 설정
			resId = getResources().getIdentifier("mainmenu_keyword_pin_" + (i+1), "id", getPackageName());
			mKeywordPin[i] = (ImageView)findViewById(resId);
		}
		
		// 랜덤 값을 저장하기 위한 ArrayList 와 오늘의 키워드를 저장하기 위한 ArrayList 객체 생성
		mLoadRandomIndex = new ArrayList<Integer>();
		mTodayKeyword = new ArrayList<String>();
		
		// 2번 키워드 애니메이션 설정
		Animation anim_mid = AnimationUtils.loadAnimation(MainMenu.this, R.anim.roate_keword_mid);
		mTextKeyword[1].setAnimation(anim_mid);
		
		// 다른 Rsource Id 설정
		mBtnCreateIdea = (ImageButton)findViewById(R.id.mainmenu_create_idea);
		mBtnManageIdea = (ImageButton)findViewById(R.id.mainmenu_manage_idea);
		mBtnConfig = (ImageButton)findViewById(R.id.mainmenu_config);
		mBtnHelp = (ImageButton)findViewById(R.id.mainmenu_help);
		mBtnRefresh = (ImageButton)findViewById(R.id.mainmenu_refresh);
		mBtnChallenge = (ImageButton)findViewById(R.id.mainmenu_challenge);
		mImageTopClosed = (ImageView)findViewById(R.id.mainmenu_keyword_top_closed);
		
		// 리스너 설정
		mBtnCreateIdea.setOnClickListener(this);
		mBtnManageIdea.setOnClickListener(this);
		mBtnConfig.setOnClickListener(this);
		mBtnHelp.setOnClickListener(this);
		mBtnRefresh.setOnClickListener(this);
		mBtnChallenge.setOnClickListener(this);
		
//		Log.d(TAG, "오늘 실행 여부 : " + mTodayRunCheck);
		
		if(!mTodayRunCheck) {																			//	처음 실행
			setKeyword();
			
			// 새로 고침 횟수 초기화, 노트 작성 초기화
			SharedPreferences.Editor editor = mConfigPref.edit();
			editor.putInt(Constants.CONFIG_REFRESH_COUNT, 0);
			editor.commit();
		} else {																						//	오늘은 이미 실행 되었음
			// 환경설정에서 키워드를 불러와서 복구 한 뒤, TextView 에 설정
			for(int i=0; i < Constants.MAX_KEYWORD_TEXTVIEW; i++) {
				mTextKeyword[i].setText(mConfigPref.getString(Constants.CONFIG_KEYWORD + (i+1), Constants.DEFAULT_KEYWORD));
			}
			
			// 새로 고침 횟수 복구
			mTodayRefreshCount = mConfigPref.getInt(Constants.CONFIG_REFRESH_COUNT, 0);
		}
		
		// 튜토리얼 다이얼로그 설정
		if(!mConfigPref.getBoolean(Constants.CONFIG_TUTORIAL_CHECK, false)) {
			SharedPreferences.Editor editor = mConfigPref.edit();
			editor.putBoolean(Constants.CONFIG_UPDATE_2_27, true);
			editor.commit();
			
			TutorialDialog tutorialDialog = new TutorialDialog(mContext, R.style.PopupDialog);
			tutorialDialog.show();
		} else {
			if(!mConfigPref.getBoolean(Constants.CONFIG_UPDATE_2_27, false)) {
				UpdateDialog updateDialog = new UpdateDialog(mContext, R.style.PopupDialog);
				updateDialog.show();
			}
		}
	}
	
	@Override
	protected void onStart() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onStart 호출");
		}
		
		super.onStart();
//		Log.d(TAG, "onStart 호출");
		
		mDisplayKeywordSize = mConfigPref.getInt(Constants.CONFIG_KEYWORD_NUMBER, 3);
		
		// 라이프 사이클에 대비해 키워드 값을 저장된 환경 설정 값에서 불러들인다.
		for(int i=0; i < Constants.MAX_KEYWORD_TEXTVIEW; i++) {
			mTextKeyword[i].setText(mConfigPref.getString(Constants.CONFIG_KEYWORD + (i+1), ""));
		}
		
//		Log.d(TAG, "표시되는 키워드 갯수 : " + mDisplayKeywordSize);
		
		// 키워드의 선택 갯수에 따라 화면에 표시하는 뷰를 변경 
		switch(mDisplayKeywordSize) {
			case 1:
//				Log.d(TAG, "키워드 갯수 1개 설정");
				
				// 1번 키워드 감춤
				mTextKeyword[0].setAnimation(null);
				mTextKeyword[0].setVisibility(View.INVISIBLE);
				mImageTopClosed.setVisibility(View.VISIBLE);
				
				// 3번 키워드 감춤
				mTextKeyword[2].setText(getString(R.string.etc_keyword_closed));
				
				break;
			case 2:
//				Log.d(TAG, "키워드 갯수 2개 설정");
				
				// 1번 키워드 감춤
				mTextKeyword[0].setAnimation(null);
				mTextKeyword[0].setVisibility(View.INVISIBLE);
				mImageTopClosed.setVisibility(View.VISIBLE);
				
				// 3번 키워드 보임
				mTextKeyword[2].setVisibility(View.VISIBLE);
				mTextKeyword[2].setText(mConfigPref.getString(Constants.CONFIG_KEYWORD + (3), Constants.DEFAULT_KEYWORD));
				
				break;
			case 3:
//				Log.d(TAG, "키워드 갯수 3개 설정");

				// 1번 키워드 보임
				Animation anim_top = AnimationUtils.loadAnimation(MainMenu.this, R.anim.roate_keword_top);
				mTextKeyword[0].setVisibility(View.VISIBLE);
				mTextKeyword[0].setAnimation(anim_top);
				mImageTopClosed.setVisibility(View.GONE);
				
				// 3번 키워드 보임
				mTextKeyword[2].setVisibility(View.VISIBLE);
				mTextKeyword[2].setText(mConfigPref.getString(Constants.CONFIG_KEYWORD + (3), Constants.DEFAULT_KEYWORD));
				
				break;
		}

//		Log.d(TAG, "해제해야 할 키워드 상태 : " + mCalculateIndex);
		
		initKeywordState();
		
		// 키워드 새로고침 도전과제 설정
		mChallengeRefreshCount = 0;
		
		// 시간과 정신의 방 도전과제 설정
		if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_TIME_AND_ROOM_OF_THE_MIND, false)) {
			if(mChallengeTimeThread != null && mChallengeTimeThread.isAlive()) {
				mChallengeTimeThread.interrupt();
			}
			
			mChallengeTimeFlag = false;
			
			mChallengeTimeThread = new Thread(new Runnable() {
				public void run() {
					while(!mChallengeTimeFlag) {
						mChallengeTimeHandler.sendMessage(mChallengeTimeHandler.obtainMessage());
						SystemClock.sleep(1000);
						mChallengeTimeCount++;
						
						// 시간과 정신의 방 도전과제 성공
						if(mChallengeTimeCount == Constants.MAX_CHALLENGE_TIME) {
							SharedPreferences.Editor editor = mConfigPref.edit();
							editor.putBoolean(Constants.COMPLETE_CHALLENGE_TIME_AND_ROOM_OF_THE_MIND, true);
							editor.commit();
							
							// 도전과제 db 를  Update
							Utils.updateChallengeState(mContext, Constants.CHALLENGE_TIME_AND_ROOM_OF_THE_MIND);
						}
					}
				}
			});
			
			mChallengeTimeThread.start();
		}
	}
	
	@Override
	protected void onPause() {
		Log.d(TAG, "onPause 호출");
		
		super.onPause();
		mChallengeTimeFlag = true;
		mChallengeTimeCount = 0;
	}
	
	
	
	private void setKeyword() {
//		Log.d(TAG, "setKeyword 호출");
		
		// 1. 사용자가 선택한 카테고리를 불러온다.
		mSelectedCategory = new String[Constants.MAX_KEYWORD_TEXTVIEW];
		for(int i=0; i < mSelectedCategory.length; i++) {
			mSelectedCategory[i] = mConfigPref.getString(Constants.CONFIG_KEYWORD_CATEGORY + (i+1), Constants.DEFAULT_ALL_CATEGORY);
//			Log.d(TAG, "선택한 카테고리 : " + mSelectedCategory[i]);
		}
		
		// 2. 카테고리에 따른 텍스트를 불러온다.
		mLoadKeywordText = new ArrayList[Constants.MAX_KEYWORD_TEXTVIEW];
		for(int i=0; i < mLoadKeywordText.length; i++) {
			mLoadKeywordText[i] = Utils.getKeywordList(mContext, mSelectedCategory[i]);
//			Log.d(TAG, mSelectedCategory[i] + " 카테고리 내 키워드 사이즈[" + i + "] : " + mLoadKeywordText[i].size());
		}
		
		// 랜덤 값과 오늘의 키워드 값을 저장할 ArrayList 를 클리어
		mLoadRandomIndex.clear();
		mTodayKeyword.clear();
		
		if(mSelectedCategory[0].equals(mSelectedCategory[1]) && mSelectedCategory[0].equals(mSelectedCategory[2])) {	//	Clear
//			Log.d(TAG, "3개가 같다");
			mLoadRandomIndex = Utils.getKeywordIndexToArray(mLoadRandomIndex, mLoadKeywordText[0].size(), 3);
			
			for(int i=0; i < mLoadRandomIndex.size(); i++) {
				mTodayKeyword.add(mLoadKeywordText[i].get(mLoadRandomIndex.get(i)).getKeyword());
//				Log.d(TAG, "오늘의 단어[" + (i+1) + "] = " + mTodayKeyword.get(i));
			}
			
		} else if(mSelectedCategory[0].equals(mSelectedCategory[1])) {
//			Log.d(TAG, "1번, 2번 카테고리가 같다.");
			mLoadRandomIndex = Utils.getKeywordIndexToArray(mLoadRandomIndex, mLoadKeywordText[0].size(), 2);
			mLoadRandomIndex.add(Utils.getRandomIndex(mLoadKeywordText[2].size()));			//	3번 키워드 랜덤 값 추가
			
			for(int i=0; i < mLoadRandomIndex.size(); i++) {
				mTodayKeyword.add(mLoadKeywordText[i].get(mLoadRandomIndex.get(i)).getKeyword());
//				Log.d(TAG, "오늘의 단어[" + (i+1) + "] = " + mTodayKeyword.get(i));
			}
			
		} else if(mSelectedCategory[0].equals(mSelectedCategory[2])) {
//			Log.d(TAG, "1번 3번 카테고리가 같다.");
			mLoadRandomIndex = Utils.getKeywordIndexToArray(mLoadRandomIndex, mLoadKeywordText[0].size(), 2);
			mLoadRandomIndex.add(1, Utils.getRandomIndex(mLoadKeywordText[1].size()));		//	2번 키워드 랜덤 값 추가
			
			for(int i=0; i < mLoadRandomIndex.size(); i++) {
				mTodayKeyword.add(mLoadKeywordText[i].get(mLoadRandomIndex.get(i)).getKeyword());
//				Log.d(TAG, "오늘의 단어[" + (i+1) + "] = " + mTodayKeyword.get(i));
			}

		} else if(mSelectedCategory[1].equals(mSelectedCategory[2])) {
//			Log.d(TAG, "2번 3번 카테고리가 같다.");
			mLoadRandomIndex.add(Utils.getRandomIndex(mLoadKeywordText[0].size()));			//	1번 키워드 랜덤 값 추가
			mLoadRandomIndex = Utils.getKeywordIndexToArray(mLoadRandomIndex, mLoadKeywordText[1].size(), 2);
			
			for(int i=0; i < mLoadRandomIndex.size(); i++) {
				mTodayKeyword.add(mLoadKeywordText[i].get(mLoadRandomIndex.get(i)).getKeyword());
//				Log.d(TAG, "오늘의 단어[" + (i+1) + "] = " + mTodayKeyword.get(i));
			}
		} else {
//			Log.d(TAG, "세 카테고리가 모두 다르다.");
			for(int i=0; i < Constants.MAX_KEYWORD_TEXTVIEW; i++) {
				mLoadRandomIndex.add(Utils.getRandomIndex(mLoadKeywordText[i].size()));
				mTodayKeyword.add(mLoadKeywordText[i].get(mLoadRandomIndex.get(i)).getKeyword());
//				Log.d(TAG, "오늘의 단어[" + (i+1) + "] = " + mTodayKeyword.get(i));
			}
		}
		
		// 키워드를 고정 한 뒤, 같은 값이 있는지 판단 존재한다면 다시 랜덤을 돌린다.
		switch(mCalculateIndex) {
		case 2:		//	1번 키워드 고정
//			Log.d(TAG, "1번 키워드 고정");
			
			if(mTextKeyword[0].getText().equals(mTodayKeyword.get(1)) || mTextKeyword[0].getText().equals(mTodayKeyword.get(2))) {
//				Log.d(TAG, "같은 키워드가 존재");
				setKeyword();
			}
			
			mTodayKeyword.set(0, mTextKeyword[0].getText().toString());
			
			break;
		case 3:		//	2번 키워드 고정
//			Log.d(TAG, "2번 키워드 고정");
			
			if(mTextKeyword[1].getText().equals(mTodayKeyword.get(0)) || mTextKeyword[1].getText().equals(mTodayKeyword.get(2))) {
//				Log.d(TAG, "같은 키워드가 존재");
				setKeyword();
			} 
			
			mTodayKeyword.set(1, mTextKeyword[1].getText().toString());
			
			break;
		case 4:		//	3번 키워드 고정
//			Log.d(TAG, "3번 키워드 고정");
			
			if(mTextKeyword[2].getText().equals(mTodayKeyword.get(0)) || mTextKeyword[2].getText().equals(mTodayKeyword.get(1))) {
//				Log.d(TAG, "같은 키워드가 존재");
				setKeyword();
			}
			
			mTodayKeyword.set(2, mTextKeyword[2].getText().toString());
			break;
			
		case 5:		//	1, 2번 키워드 고정
//			Log.d(TAG, "1, 2번 키워드 고정");
			
			if(mTextKeyword[0].getText().equals(mTodayKeyword.get(2)) || mTextKeyword[1].getText().equals(mTodayKeyword.get(2))) {
//				Log.d(TAG, "같은 키워드가 존재");
				setKeyword();
			}
			
			mTodayKeyword.set(0, mTextKeyword[0].getText().toString());
			mTodayKeyword.set(1, mTextKeyword[1].getText().toString());
			
			break;
		case 6:		//	1, 3번 고정
//			Log.d(TAG, "1, 3번 키워드 고정");
			
			if(mTextKeyword[0].getText().equals(mTodayKeyword.get(1)) || mTextKeyword[2].getText().equals(mTodayKeyword.get(1))) {
//				Log.d(TAG, "같은 키워드가 존재");
				setKeyword();
			}
			
			mTodayKeyword.set(0, mTextKeyword[0].getText().toString());
			mTodayKeyword.set(2, mTextKeyword[2].getText().toString());
			
			break;
		case 7:		//	2, 3번 고정
//			Log.d(TAG, "2, 3번 키워드 고정");
			
			if(mTextKeyword[1].getText().equals(mTodayKeyword.get(0)) || mTextKeyword[2].getText().equals(mTodayKeyword.get(0))) {
//				Log.d(TAG, "같은 키워드가 존재");
				setKeyword();
			}
			
			mTodayKeyword.set(1, mTextKeyword[1].getText().toString());
			mTodayKeyword.set(2, mTextKeyword[2].getText().toString());
			
			break;
		case 9:		//	전부 고정
//			Log.d(TAG, "전부 고정");
			
			mTodayKeyword.set(0, mTextKeyword[0].getText().toString());
			mTodayKeyword.set(1, mTextKeyword[1].getText().toString());
			mTodayKeyword.set(2, mTextKeyword[2].getText().toString());
			
			return;
		default:	//	전부 고정이 안되었을 경우
//			Log.d(TAG, "전부 고정 안됨");
			break;
		}
		

		// TextView 에 키워드를 설정한 뒤, 오늘의 키워드를 저장해 놓는다.
//		Log.d(TAG, "SharedPreference 에 오늘의 키워드를 저장");
		
		SharedPreferences.Editor editor = mConfigPref.edit();

		for(int i=0; i < mTextKeyword.length; i++) {
			mTextKeyword[i].setText(mTodayKeyword.get(i));
			editor.putString(Constants.CONFIG_KEYWORD + (i+1), mTodayKeyword.get(i));
		}
		
		editor.commit();
	}
	
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.mainmenu_create_idea:
				// 현재 표시된 키워드를 가지고 보낼 키워드를 생성한다
				mSendKeyword = Utils.sendKeyword(mTextKeyword[0].getText().toString(), 
						mTextKeyword[1].getText().toString(), 
						mTextKeyword[2].getText().toString(), mDisplayKeywordSize);
				
				Intent intentNote = new Intent(MainMenu.this, NoteActivity.class);
				intentNote.putExtra(Constants.PUT_CALLER_ACTIVITY, Constants.CALLER_CREATE_IDEA);
				intentNote.putExtra(Constants.PUT_SEND_KEYWORD, mSendKeyword);
				startActivity(intentNote);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				break;
			case R.id.mainmenu_manage_idea:
				MessagePool messagePool = (MessagePool)getApplicationContext();
				messagePool.setNoteEmptyFlag(false);
				
				Intent intentManageNote = new Intent(MainMenu.this, NoteGroupList.class);
				startActivity(intentManageNote);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				
				break;
			case R.id.mainmenu_config:
				Intent intentConfig = new Intent(MainMenu.this, madcat.studio.idea.Config.class);
				startActivity(intentConfig);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				break;
			case R.id.mainmenu_help:
				Intent intentHelp = new Intent(MainMenu.this, Help.class);
				startActivity(intentHelp);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				break;
			case R.id.mainmenu_refresh:
				if(mCalculateIndex != 9) {
					if(mTodayRefreshCount < MAX_REFRESH_COUNT) {
						// 키워드를 갱신한다.
						setKeyword();
						mSendKeyword = Utils.sendKeyword(mTextKeyword[0].getText().toString(), 
								mTextKeyword[1].getText().toString(), 
								mTextKeyword[2].getText().toString(), mDisplayKeywordSize);
						
						// 새로고침 횟수를 한번 증가시킨다.
						mTodayRefreshCount++;
						
						// 도전과제 체크를 위한 새로고침 횟수를 한번 증가시킨다.
						mChallengeRefreshCount++;
						
						// 증가 시킨 새로고침 횟수를 환경변수에 저장한다.
						SharedPreferences.Editor editor = mConfigPref.edit();
						editor.putInt(Constants.CONFIG_REFRESH_COUNT, mTodayRefreshCount);
						editor.commit();
	
						// 현재 키워드 표시 갯수가 3개가 아니라면, Closed 로 변경시켜 표시한다.
						if(mDisplayKeywordSize == 1) {
							mTextKeyword[2].setText(getString(R.string.etc_keyword_closed));
						}
						
						// '섞어야 제 맛' 도전 과제를 체크
						if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_SHAKE, false)) {
							if(mChallengeRefreshCount == Constants.MAX_REFREHS_CHALLENGE_COUNT) {
								Toast.makeText(mContext, getString(R.string.complete_challenge_refresh), Toast.LENGTH_SHORT).show();
								editor.putBoolean(Constants.COMPLETE_CHALLENGE_SHAKE, true);
								editor.commit();
								
								// 도전과제 db 를 Update
								Utils.updateChallengeState(mContext, Constants.CHALLENGE_SHAKE);
							}
						}
						
						Utils.sendBroadCasting(mContext, Constants.ACTION_KEYWORD_REFRESH);
					} 
				}
				break;
			case R.id.mainmenu_challenge:
				Intent intentChallenge = new Intent(MainMenu.this, ChallengeList.class);
				startActivity(intentChallenge);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				break;
			default:
				break;
		}
		
		saveRefreshKeyword();
	}
	
	private void getIntenter() {
		Intent intent = getIntent();

		if(DEVELOPE_MODE) {
			Log.d(TAG, "넘어온 인텐트 값 : " + intent);
		}
		
		if(intent != null) {
			String caller = intent.getExtras().getString(Constants.PUT_CALLER_ACTIVITY);
			
			if(caller.equals(Loading.class.toString())) {
				mTodayRunCheck = intent.getExtras().getBoolean(Constants.PUT_TODAY_RUN_CHECK);
			} else if(caller.equals(IdeaWidget.class.toString())) {
				mTodayRunCheck = true;
			}
		}
	}

	public boolean onTouch(View v, MotionEvent event) {
		switch(v.getId()) {
			case R.id.mainmenu_keyword_1:
				if(mDisplayKeywordSize == 1 || mDisplayKeywordSize == 2) {
					return true;
				} else {
					if(!mTouchFlag_1) {
						mTextKeyword[0].setTextColor(Color.RED);
						mKeywordPin[0].setVisibility(View.VISIBLE);
						
						mTouchFlag_1 = true;
						
						mCalculateIndex += 2;
						
					} else {
						mTextKeyword[0].setTextColor(DEFAULT_TEXT_COLOR);
						mKeywordPin[0].setVisibility(View.GONE);
						
						mTouchFlag_1 = false;
						
						mCalculateIndex -= 2;
						
					}
					
					break;
				}
			case R.id.mainmenu_keyword_2:
				if(mDisplayKeywordSize == 1) {
					return true;
				} else {
					if(!mTouchFlag_2) {
						mTextKeyword[1].setTextColor(Color.RED);
						mKeywordPin[1].setVisibility(View.VISIBLE);
						
						mTouchFlag_2 = true;
						mCalculateIndex += 3;
					} else {
						mTextKeyword[1].setTextColor(DEFAULT_TEXT_COLOR);
						mKeywordPin[1].setVisibility(View.GONE);
						
						mTouchFlag_2 = false;
						mCalculateIndex -= 3;
					}
					
					break;
				}
			case R.id.mainmenu_keyword_3:
				if(mDisplayKeywordSize == 1) {
					return true;
				} else {
					if(!mTouchFlag_3) {
						mTextKeyword[2].setTextColor(Color.RED);
						mKeywordPin[2].setVisibility(View.VISIBLE);
						
						mTouchFlag_3 = true;
						mCalculateIndex += 4;
						
					} else {
						mTextKeyword[2].setTextColor(DEFAULT_TEXT_COLOR);
						mKeywordPin[2].setVisibility(View.GONE);
						
						mTouchFlag_3 = false;
						mCalculateIndex -= 4;
					}
					
					break;
				}
		}
		
		return false;
	}
	
	private void initKeywordState() {
//		Log.d(TAG, "initKeywordState 호출");
		
		for(int i=0; i < Constants.MAX_KEYWORD_TEXTVIEW; i++) {
			mTextKeyword[i].setTextColor(DEFAULT_TEXT_COLOR);
			mKeywordPin[i].setVisibility(View.GONE);
		}
		
		mTouchFlag_1 = false;
		mTouchFlag_2 = false;
		mTouchFlag_3 = false;
		
		mCalculateIndex = 0;
	}
	
	private void saveRefreshKeyword() {
//		Log.d(TAG, "saveRefreshKeyword 호출");

		SharedPreferences.Editor editor = mConfigPref.edit();

		switch(mDisplayKeywordSize) {
		case 2:
			editor.putString(Constants.CONFIG_KEYWORD_2, mTextKeyword[1].getText().toString());
			editor.putString(Constants.CONFIG_KEYWORD_3, mTextKeyword[2].getText().toString());
			break;
		case 3:
			editor.putString(Constants.CONFIG_KEYWORD_1, mTextKeyword[0].getText().toString());
			editor.putString(Constants.CONFIG_KEYWORD_2, mTextKeyword[1].getText().toString());
			editor.putString(Constants.CONFIG_KEYWORD_3, mTextKeyword[2].getText().toString());
			break;
		}
		
		editor.commit();
		
	}
	
	@Override
	protected void onDestroy() {
		Log.d(TAG, "onDestroy 호출");
		
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
