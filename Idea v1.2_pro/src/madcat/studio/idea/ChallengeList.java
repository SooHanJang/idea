package madcat.studio.idea;

import java.util.ArrayList;

import madcat.studio.challenge.Challenge;
import madcat.studio.challenge.ChallengeAdapter;
import madcat.studio.constants.Constants;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Window;

public class ChallengeList extends ListActivity {

	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private ArrayList<Challenge> mItems;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.idea_challenge_list);
		
		// �⺻ ����
		mContext = this;
		mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);

		mItems = Utils.getChallengeList(mContext);
		
		ChallengeAdapter adapter = new ChallengeAdapter(mContext, R.layout.idea_challenge_list_row, mItems);
		setListAdapter(adapter);
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
