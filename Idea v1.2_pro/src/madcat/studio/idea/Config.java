package madcat.studio.idea;

import java.io.File;
import java.util.ArrayList;

import madcat.studio.category.CategoryList;
import madcat.studio.constants.Constants;
import madcat.studio.dialogs.ChoiceCategoryDialog;
import madcat.studio.dialogs.ConfigConfirmDialog;
import madcat.studio.keyword.KeywordList;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.view.Window;
import android.widget.Toast;

public class Config extends PreferenceActivity implements OnPreferenceClickListener {
	
//	private final String TAG										=	"Config";
	
	private final int DATA_BACKUP_FLAG								=	1;
	private final int DATA_RESTORE_FLAG								=	2;
	
	private Context mContext;
	
	private Preference mPrefManageKeyword;
	private Preference mPrefManageCategory;
	private Preference mPrefKeywordNumber;
	private Preference mPrefKeywordCategory;
	private Preference mPrefBackup, mPrefRestore;
	
	private SharedPreferences mConfigPref;
	
	private int mTempKeywordIndex;
	
	private ArrayList<String> mFilterFileName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.menu_config);
		
		mContext = this;
		mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		// 환경설정 Resource Id 설정
		mPrefManageKeyword = (Preference)findPreference("pref_keyword_managing_keyword");
		mPrefManageCategory = (Preference)findPreference("pref_keyword_managing_category");
		mPrefKeywordNumber = (Preference)findPreference("pref_keyword_modify_number");
		mPrefKeywordCategory = (Preference)findPreference("pref_keyword_modify_category");
		mPrefBackup = (Preference)findPreference("pref_data_backup");
		mPrefRestore = (Preference)findPreference("pref_data_restore");
		
		// 리스너 설정
		mPrefManageKeyword.setOnPreferenceClickListener(this);
		mPrefManageCategory.setOnPreferenceClickListener(this);
		mPrefKeywordNumber.setOnPreferenceClickListener(this);
		mPrefKeywordCategory.setOnPreferenceClickListener(this);
		mPrefBackup.setOnPreferenceClickListener(this);
		mPrefRestore.setOnPreferenceClickListener(this);
		
		// 기본 환경 설정 값 불러오기
		int loadKeywordNumber = mConfigPref.getInt(Constants.CONFIG_KEYWORD_NUMBER, 3);
		mPrefKeywordNumber.setSummary(getString(R.string.prefix_keyword_number) + loadKeywordNumber + getString(R.string.suffix_keyword_unit));
	}

	public boolean onPreferenceClick(Preference preference) {
		if(preference.getKey().equals("pref_keyword_managing_keyword")) {
			Intent intent = new Intent(Config.this, KeywordList.class);
			startActivity(intent);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		} else if(preference.getKey().equals("pref_keyword_managing_category")) {
			Intent intent = new Intent(Config.this, CategoryList.class);
			startActivity(intent);
			overridePendingTransition(R.anim.fade, R.anim.hold);
		} else if(preference.getKey().equals("pref_keyword_modify_number")) {
			showModifyKeywordNumberDialog();
		} else if(preference.getKey().equals("pref_keyword_modify_category")) {
			ChoiceCategoryDialog choiceCategoryDialog = new ChoiceCategoryDialog(mContext);
			choiceCategoryDialog.show();
		} else if(preference.getKey().equals("pref_data_backup")) {
			showBackupDialog();
		} else if(preference.getKey().equals("pref_data_restore")) {
			showRestoreDialog();
		}
		
		return false;
	}
	
	private void showModifyKeywordNumberDialog() {
		final String items[] = {"1개", "2개", "3개"};
		int loadItemIndex = mConfigPref.getInt(Constants.CONFIG_KEYWORD_NUMBER, 3) - 1;
		int tempItemIndex = 0;
		
		AlertDialog.Builder builder = new Builder(mContext);
		builder.setTitle(getString(R.string.config_title_modify_keyword_number));
		builder.setSingleChoiceItems(items, loadItemIndex, new OnClickListener() {
			public void onClick(DialogInterface dialog, int index) {
				mTempKeywordIndex = index;
			}
		}).setPositiveButton(getString(R.string.dialog_ok), new OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				int selectKeywordNumber = mTempKeywordIndex + 1;
				
				// 선택한 키워드의 개수를 저장
				SharedPreferences.Editor editor = mConfigPref.edit();
				editor.putInt(Constants.CONFIG_KEYWORD_NUMBER, selectKeywordNumber);
				editor.commit();
				
				mPrefKeywordNumber.setSummary(getString(R.string.prefix_keyword_number) + selectKeywordNumber + getString(R.string.suffix_keyword_unit));
				Utils.sendBroadCasting(mContext, Constants.ACTION_KEYWORD_NUMBER);
				
			}
		}).setNegativeButton(getString(R.string.dialog_cancel), new OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				
			}
		});
		
		builder.show();
	}
	
	// 데이터 백업 다이얼로그를 호출
	private void showBackupDialog() {
		final ConfigConfirmDialog backupDialog = new ConfigConfirmDialog(mContext, getString(R.string.dialog_title_config_backup));
		backupDialog.show();
		
		backupDialog.setOnDismissListener(new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				if(backupDialog.getDialogStateFlag()) {
					DataBackUpRestoreAsync dataBackupAsync = new DataBackUpRestoreAsync(mContext, DATA_BACKUP_FLAG);
					dataBackupAsync.execute();
				}
			}
		});
	}
	
	// 데이터 복원 다이얼로그를 호출
	private void showRestoreDialog() {
		String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		String targetPath = sdCardPath + Constants.IDEA_NOTE_BACKUP_PATH;
		
		// 백업 폴더에 저장된 파일을 읽어와 List에 표시해 줄 item 을 생성
		File fileList = new File(targetPath);
		
		if(fileList.listFiles() == null) {
			Toast.makeText(mContext, getString(R.string.toast_title_restore_file_is_empty), Toast.LENGTH_SHORT).show();
		} else {
			// 먼저 디렉토리에 있는 파일명을 읽어온다.
			ArrayList<String> fileName = new ArrayList<String>();
			mFilterFileName = new ArrayList<String>();
			
			for(File file : fileList.listFiles()) {
				fileName.add(file.getName());
			}
			
			// 조건에 따라, 정확한 복원 파일을 필터링 해서 저장한다.
			mFilterFileName = Utils.filterFileName(fileName);
			
			// 리스트 다이얼로그에 표시할 String 배열을 생성한다.
			final String[] listFileItems = Utils.splitFileName(mContext, mFilterFileName);
			
			// 사용자에게 복원 파일 목록을 보여줄 다이얼로그를 생성한다.
			AlertDialog.Builder builder = new Builder(mContext);
			builder.setTitle(getString(R.string.dialog_title_select_restore_file));
			builder.setItems(listFileItems, new OnClickListener() {
				public void onClick(DialogInterface dialog, final int which) {
					// 해당 파일로 복구할 것인지를 물어보는 ConfirmDialog를 생성한다.
					final ConfigConfirmDialog finalRestoreDialog = 
						new ConfigConfirmDialog(mContext, listFileItems[which] + getString(R.string.suffix_restore_message));
					finalRestoreDialog.show();
					
					finalRestoreDialog.setOnDismissListener(new OnDismissListener() {
						public void onDismiss(DialogInterface dialog) {
							if(finalRestoreDialog.getDialogStateFlag()) {
								// 선택된 파일로 데이터를 복원한다.
								DataBackUpRestoreAsync dataRestoreAsync = new DataBackUpRestoreAsync(mContext, DATA_RESTORE_FLAG);
								dataRestoreAsync.setRestoreFileName(mFilterFileName.get(which));
								dataRestoreAsync.execute();
							}
						}
					});
				}
			});
			
			builder.show();
		}
	}
	
	class DataBackUpRestoreAsync extends AsyncTask<Void, Void, Void> {
		int ctrlFlag;
		Context context;
		ProgressDialog progressDialog;
		boolean succeedFlag;
		String restoreFileName										=	null;
		
		public DataBackUpRestoreAsync(Context context, int flag) {
			this.context = context;
			this.ctrlFlag = flag;
			this.succeedFlag = false;
		}
		
		/**
		 * 복구 플래그에서만 호출하도록 합니다. 
		 * @param fileName
		 */
		public void setRestoreFileName(String fileName) {
			this.restoreFileName = fileName;
		}
		
		
		@Override
		protected void onPreExecute() {
			String message = null;
			
			if(ctrlFlag == DATA_BACKUP_FLAG) {
				message = context.getString(R.string.progress_dialog_data_backup);
			} else if(ctrlFlag == DATA_RESTORE_FLAG){
				message = context.getString(R.string.progress_dialog_data_restore);
			}
			
			progressDialog = ProgressDialog.show(mContext, "", message);
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			if(ctrlFlag == DATA_BACKUP_FLAG) {
				try {
					// 사용자가 설정한 환경설정 값들을 DB 에 먼저 저장
					SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
					String dupSql = "SELECT * FROM " + Constants.TABLE_IDEA_BACKUP;
					Cursor backupCursor = sdb.rawQuery(dupSql, null);
					
					if(backupCursor.getCount() != 0) {
						sdb.delete(Constants.TABLE_IDEA_BACKUP, null, null);
					}
					
					ContentValues insertValues = new ContentValues();
					insertValues.put(Constants.BACKUP_COLUMN_DATE, mConfigPref.getString(Constants.CONFIG_TODAY_DATE, ""));
					insertValues.put(Constants.BACKUP_COLUMN_KEYSIZE, mConfigPref.getInt(Constants.CONFIG_KEYWORD_NUMBER, 3));
					insertValues.put(Constants.BACKUP_COLUMN_NOTE_THEME_INDEX, mConfigPref.getInt(Constants.CONFIG_NOTE_THEME_INDEX, 0));
					insertValues.put(Constants.BACKUP_COLUMN_KEYWORD_A, 
							mConfigPref.getString(Constants.CONFIG_KEYWORD_1, Constants.DEFAULT_KEYWORD));
					insertValues.put(Constants.BACKUP_COLUMN_KEYWORD_B, 
							mConfigPref.getString(Constants.CONFIG_KEYWORD_2, Constants.DEFAULT_KEYWORD));
					insertValues.put(Constants.BACKUP_COLUMN_KEYWORD_C, 
							mConfigPref.getString(Constants.CONFIG_KEYWORD_3, Constants.DEFAULT_KEYWORD));
					insertValues.put(Constants.BACKUP_COLUMN_CATEGORY_A, 
							mConfigPref.getString(Constants.CONFIG_KEYWORD_CATEGORY_1, Constants.DEFAULT_ALL_CATEGORY));
					insertValues.put(Constants.BACKUP_COLUMN_CATEGORY_B, 
							mConfigPref.getString(Constants.CONFIG_KEYWORD_CATEGORY_2, Constants.DEFAULT_ALL_CATEGORY));
					insertValues.put(Constants.BACKUP_COLUMN_CATEGORY_C, 
							mConfigPref.getString(Constants.CONFIG_KEYWORD_CATEGORY_3, Constants.DEFAULT_ALL_CATEGORY));
					
					sdb.insert(Constants.TABLE_IDEA_BACKUP, null, insertValues);
					backupCursor.close();
					sdb.close();

					// 데이터베이스를 백업
					succeedFlag = Utils.dataBackUp(mContext);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if(ctrlFlag == DATA_RESTORE_FLAG){
				// 데이터베이스를 복구
				succeedFlag = Utils.dataRestore(mContext, restoreFileName);
				
				// 사용자의 환경 설정 값을 복원
				SharedPreferences.Editor editor = mConfigPref.edit();
				
				SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
				String configSql = "SELECT * FROM " + Constants.TABLE_IDEA_BACKUP;
				Cursor configCursor = sdb.rawQuery(configSql, null);
				
				while(configCursor.moveToNext()) {
					editor.putInt(Constants.CONFIG_KEYWORD_NUMBER, configCursor.getInt(2));
					editor.putInt(Constants.CONFIG_NOTE_THEME_INDEX, configCursor.getInt(3));
					
					if(configCursor.getString(1).equals(mConfigPref.getString(Constants.CONFIG_TODAY_DATE, ""))) {
						editor.putString(Constants.CONFIG_KEYWORD_1, configCursor.getString(4));
						editor.putString(Constants.CONFIG_KEYWORD_2, configCursor.getString(5));
						editor.putString(Constants.CONFIG_KEYWORD_3, configCursor.getString(6));
					}
					
					editor.putString(Constants.CONFIG_KEYWORD_CATEGORY_1, configCursor.getString(7));
					editor.putString(Constants.CONFIG_KEYWORD_CATEGORY_2, configCursor.getString(8));
					editor.putString(Constants.CONFIG_KEYWORD_CATEGORY_3, configCursor.getString(9));
				}
				
				// 오늘자 노트가 생성 되었는지 여부를 체크
				String compareSql = "SELECT * FROM " + Constants.TABLE_IDEA_NOTE + 
									" WHERE " + Constants.NOTE_COLUMN_DATE + " = '" + mConfigPref.getString(Constants.CONFIG_TODAY_DATE, "") + "'";
				Cursor compareCursor = sdb.rawQuery(compareSql, null);
				
				editor.commit();
				compareCursor.close();
				configCursor.close();
				
				// 사용자 도전과제를 복원
				String challengeSql = "SELECT * FROM " + Constants.TABLE_IDEA_CHALLENGE;
				Cursor challengeCursor = sdb.rawQuery(challengeSql, null);
				challengeCursor.moveToFirst();
				
				while(challengeCursor.moveToNext()){
					boolean checkFlag = false;
					
					switch(challengeCursor.getInt(5)) {
						case 0:
							checkFlag = false;
							break;
						case 1:
							checkFlag = true;
							break;
					}
					
					if(challengeCursor.getString(1).equals(Constants.CHALLENGE_BEGINNER_PLANNER)) {
						editor.putBoolean(Constants.COMPLETE_CHALLENGE_BEGINNER_PLANNER, checkFlag);
					} else if(challengeCursor.getString(1).equals(Constants.CHALLENGE_INTERMEDIATE_PLANNER)) {
						editor.putBoolean(Constants.COMPLETE_CHALLENGE_INTERMEDIATE_PLANNER, checkFlag);
					} else if(challengeCursor.getString(1).equals(Constants.CHALLENGE_ADVANCED_PLANNER)) {
						editor.putBoolean(Constants.COMPLETE_CHALLENGE_ADVANCED_PLANNER, checkFlag);
					} else if(challengeCursor.getString(1).equals(Constants.CHALLENGE_HARD_WORK)) {
						editor.putBoolean(Constants.COMPLETE_CHALLENGE_HARD_WORK, checkFlag);
					} else if(challengeCursor.getString(1).equals(Constants.CHALLENGE_NO_PAIN_NO_GAIN)) {
						editor.putBoolean(Constants.COMPLETE_CHALLENGE_NO_PAIN_NO_GAIN, checkFlag);
					} else if(challengeCursor.getString(1).equals(Constants.CHALLENGE_IDEA_BANK)) {
						editor.putBoolean(Constants.COMPLETE_CHALLENGE_IDEA_BANK, checkFlag);
					} else if(challengeCursor.getString(1).equals(Constants.CHALLENGE_SHAKE)) {
						editor.putBoolean(Constants.COMPLETE_CHALLENGE_SHAKE, checkFlag);
					} else if(challengeCursor.getString(1).equals(Constants.CHALLENGE_MANUAL_CONQUEROR)) {
						editor.putBoolean(Constants.COMPLETE_CHALLENGE_MANUAL_CONQUEROR, checkFlag);
					} else if(challengeCursor.getString(1).equals(Constants.CHALLENGE_AMOUNT_OF_QUALITY)) {
						editor.putBoolean(Constants.COMPLETE_CHALLENGE_AMOUNT_OF_QUALITY, checkFlag);
					} else if(challengeCursor.getString(1).equals(Constants.CHALLENGE_MAD)) {
						editor.putBoolean(Constants.COMPLETE_CHALLENGE_MAD, checkFlag);
					} else if(challengeCursor.getString(1).equals(Constants.CHALLENGE_THANKS)) {
						editor.putBoolean(Constants.COMPLETE_CHALLENGE_THANKS, checkFlag);
					} else if(challengeCursor.getString(1).equals(Constants.CHALLENGE_TIME_AND_ROOM_OF_THE_MIND)) {
						editor.putBoolean(Constants.COMPLETE_CHALLENGE_TIME_AND_ROOM_OF_THE_MIND, checkFlag);
					} else if(challengeCursor.getString(1).equals(Constants.CHALLENGE_WORK_ALL_NIGHT)) {
						editor.putBoolean(Constants.COMPLETE_CHALLENGE_WORK_ALL_NIGHT, checkFlag);
					}
				}
				
				editor.commit();
				challengeCursor.close();
				
				sdb.close();
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			
			if(succeedFlag) {
				if(ctrlFlag == DATA_BACKUP_FLAG) {
					Toast.makeText(context, context.getString(R.string.toast_title_succeed_backup), Toast.LENGTH_SHORT).show();
				} else if(ctrlFlag == DATA_RESTORE_FLAG) {
					// 키워드 갯수 Summary 수정
					mPrefKeywordNumber.setSummary(getString(R.string.prefix_keyword_number) + 
							mConfigPref.getInt(Constants.CONFIG_KEYWORD_NUMBER, 3) + getString(R.string.suffix_keyword_unit));
					
					Toast.makeText(context, context.getString(R.string.toast_title_succeed_restore), Toast.LENGTH_SHORT).show();
				}
			} else {
				if(ctrlFlag == DATA_BACKUP_FLAG) {
					Toast.makeText(context, context.getString(R.string.toast_title_fail_backup), Toast.LENGTH_SHORT).show();
				} else if(ctrlFlag == DATA_RESTORE_FLAG) {
					Toast.makeText(context, context.getString(R.string.toast_title_fail_restore), Toast.LENGTH_SHORT).show();
				}
			}
		}
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
