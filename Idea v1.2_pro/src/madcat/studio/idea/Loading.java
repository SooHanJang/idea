package madcat.studio.idea;

import java.text.SimpleDateFormat;

import madcat.studio.constants.Constants;
import madcat.studio.database.LoadDatabase;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

public class Loading extends Activity {
	
	private final String TAG										=	"Loading";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private final long LOADING_DELAY								=	1000;
	
	private boolean mTodayRunCheck									=	false;
	private SharedPreferences mConfigPref;
	
	private int mChallengeDayBreak;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.idea_loading);
        
        // 오늘 날짜를 구한 뒤, 저장된 날짜와 다르면 새로 저장한다.
        mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String todayDate = dateFormat.format(new java.util.Date());
        
        // 철야근무 체크를 위한 현재 시간 값 가져오기
        dateFormat.applyPattern("HHmm");
        mChallengeDayBreak = Integer.parseInt(dateFormat.format(new java.util.Date()));
        
//        Log.d(TAG, "mChallengeDayBreak : " + mChallengeDayBreak);
        
        if(!todayDate.equals(mConfigPref.getString(Constants.CONFIG_TODAY_DATE, ""))) {
//        	Log.d(TAG, "오늘 처음 실행 합니다.");
        	
        	SharedPreferences.Editor editor = mConfigPref.edit();
        	editor.putString(Constants.CONFIG_TODAY_DATE, todayDate);
        	editor.commit();
        	
        	mTodayRunCheck = false;									//	오늘 처음 실행한다면 false
        } else {
//        	Log.d(TAG, "오늘은 이미 실행 하였습니다.");
        	mTodayRunCheck = true;									//	당일 실행이 두번 째 이상이라면 true
        }
        
    	InitAppDataAsync initAppData = new InitAppDataAsync(this, mConfigPref);
    	initAppData.execute();
        
    }
    
    /**
     * 처음 앱을 실행 하였을 시, 데이터베이스를 생성하는 AsyncTask
     * 
     * @author Acsha
     *
     */
    class InitAppDataAsync extends AsyncTask<Void, Void, Void> {

    	boolean firstAppFlag										=	true;
    	boolean succeedLoadDbFlag									=	true;
    	SharedPreferences configPref;
    	ProgressDialog progressDialog;
    	Context context;
    	
    	boolean update_2012_05_21_flag								=	false;
    	
    	public InitAppDataAsync(Context context, SharedPreferences configPref) {
    		this.context = context;
    		this.configPref = configPref;
    		firstAppFlag = configPref.getBoolean(Constants.CONFIG_APP_FIRST_START, true);
    		update_2012_05_21_flag = configPref.getBoolean(Constants.UPDATE_2012_05_21, false);
    		
//    		firstAppFlag = true;									//	어플을 초기화 하는 코드 (주의 요망)
    	}
    	
    	@Override
    	protected void onPreExecute() {
    		if(firstAppFlag) {
    			progressDialog = ProgressDialog.show(context, "", getString(R.string.progress_dialog_init_app));
    		}
    	}
    	
    	@Override
    	protected Void doInBackground(Void... params) {
    		if(firstAppFlag) {
    			// 데이터 베이스를 생성하는 메소드를 호출한다.
    			succeedLoadDbFlag = LoadDatabase.loadDataBase(getResources().getAssets(), context);
    		}
    		
    		loadingDelay(LOADING_DELAY);
    		return null;
    	}
    	
    	@Override
    	protected void onPostExecute(Void result) {
    		if(firstAppFlag) {
	    		if(progressDialog.isShowing()) {
	    			progressDialog.dismiss();
	    		}
	    		
	    		// 데이터베이스 생성에 실패하였다면, 어플을 종료한다.
	    		if(!succeedLoadDbFlag) {
	    			Toast.makeText(context, getString(R.string.toast_title_fail_load_db), Toast.LENGTH_SHORT).show();
	    			finish();
	    		}
	    		
	    		editSharedPreference(configPref);
	    		
	    		// 키워드 업데이트를 위한 알람을 등록
	    		Utils.registeDailyAlarm(context);
    		}
    		
    		// 업데이트 내역을 이곳에서 처리한다.
    		
    		if(!update_2012_05_21_flag) {
    			if(DEVELOPE_MODE) {
    				Log.d(TAG, "2012_05_21 업데이트 완료");
    			}
    			
    			Utils.registeDailyAlarm(context);
    			
    			SharedPreferences.Editor editor = configPref.edit();
    			editor.putBoolean(Constants.UPDATE_2012_05_21, true);
    			editor.commit();
    		}
    		
    		startMainMenu();
    	}
    }
    
    private void loadingDelay(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    
    private void startMainMenu() {
        // 철야근무 도전과제 체크
    	if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_WORK_ALL_NIGHT, false)) {
    		// '철야근무' 도전과제 성공
	        if(mChallengeDayBreak >=  200 && mChallengeDayBreak <= 300) {
	        	SharedPreferences.Editor editor = mConfigPref.edit();
	        	editor.putBoolean(Constants.COMPLETE_CHALLENGE_WORK_ALL_NIGHT, true);
	        	editor.commit();
	        	
	        	// 도전과제 DB Update
	        	Utils.updateChallengeState(this, Constants.CHALLENGE_WORK_ALL_NIGHT);
	        	
	        	Toast.makeText(this, getString(R.string.complete_challenge_daybreak), Toast.LENGTH_SHORT).show();
	        }
        }
    	
    	// 감사합니다 고객님 도전과제 체크
    	if(!mConfigPref.getBoolean(Constants.COMPLETE_CHALLENGE_THANKS, false)) {
        	SharedPreferences.Editor editor = mConfigPref.edit();
        	editor.putBoolean(Constants.COMPLETE_CHALLENGE_THANKS, true);
        	editor.commit();
        	
        	Utils.updateChallengeState(this, Constants.CHALLENGE_THANKS);
        	
        	Toast.makeText(this, getString(R.string.complete_challenge_thanks), Toast.LENGTH_SHORT).show();
        }
    	
    	Intent intent = new Intent(Loading.this, MainMenu.class);
    	intent.putExtra(Constants.PUT_CALLER_ACTIVITY, Loading.class.toString());
    	intent.putExtra(Constants.PUT_TODAY_RUN_CHECK, mTodayRunCheck);
		startActivity(intent);
		overridePendingTransition(R.anim.fade, R.anim.hold);
		finish();
    }
    
    /**
     * App 이 더 이상 처음 실행하는 것이 아님으로 수정 
     * @param pref
     */
    private void editSharedPreference(SharedPreferences pref) {
    	SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean(Constants.CONFIG_APP_FIRST_START, false);
		editor.commit();
    }
    
    @Override
    protected void onDestroy() {
    	Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
    	
    	super.onDestroy();
    }
    
    @Override
    public void onBackPressed() {}
}







