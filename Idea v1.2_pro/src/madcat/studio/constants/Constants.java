package madcat.studio.constants;

public class Constants {
	
	// 개발자 관련 변수
	public static final boolean DEVELOPE_MODE										=	false;
	
	// 업데이트 관련 변수
	public static final String UPDATE_2012_05_21									=	"UPDATE_2012_05_21";

	// 환경 설정, SharedPreference 관련 변수
	public static final String CONFIG_PREF_NAME										=	"IDEA_CONFIG_PREF";
	public static final String CONFIG_KEYWORD_NUMBER								=	"IDEA_CONFIG_KEYWORD_NUMBER";
	public static final String CONFIG_APP_FIRST_START								=	"IDEA_CONFIG_APP_FIRST_START";
	public static final String CONFIG_TODAY_DATE									=	"IDEA_CONFIG_TODAY_DATE";
	public static final String CONFIG_REFRESH_COUNT									=	"IDEA_CONFIG_REFRESH_COUNT";
	public static final String CONFIG_NOTE_THEME_INDEX								=	"IDEA_CONFIG_NOTE_THEME_INDEX";
	public static final String CONFIG_TUTORIAL_CHECK								=	"IDEA_CONFIG_TUTORIAL_CHECK";
	public static final String CONFIG_TODAY_RUN_CHECK								=	"IDEA_CONFIG_TODAY_RUN_CHECK";
	
	public static final String CONFIG_KEYWORD										=	"IDEA_CONFIG_KEYWORD_";
	public static final String CONFIG_KEYWORD_1										=	"IDEA_CONFIG_KEYWORD_1";
	public static final String CONFIG_KEYWORD_2										=	"IDEA_CONFIG_KEYWORD_2";
	public static final String CONFIG_KEYWORD_3										=	"IDEA_CONFIG_KEYWORD_3";
	
	public static final String CONFIG_KEYWORD_CATEGORY								=	"IDEA_CONFIG_KEYWORD_CATEGORY_";
	public static final String CONFIG_KEYWORD_CATEGORY_1							=	"IDEA_CONFIG_KEYWORD_CATEGORY_1";
	public static final String CONFIG_KEYWORD_CATEGORY_2							=	"IDEA_CONFIG_KEYWORD_CATEGORY_2";
	public static final String CONFIG_KEYWORD_CATEGORY_3							=	"IDEA_CONFIG_KEYWORD_CATEGORY_3";
	
	public static final String CONFIG_THEME_INDEX									=	"IDEA_CONFIG_THEME_INDEX";
	
	// 업데이트 관련 변수
	public static final String CONFIG_UPDATE_2_27									=	"CONFIG_UPDATE_2_27";
	
	// 인텐트 관련 변수
	public static final String PUT_SEND_KEYWORD										=	"PUT_SEND_KEYWORD";
	public static final String PUT_SEND_NOTE_WROTE_DATE								=	"PUT_SEND_NOTE_WROTE_DATE";
	public static final String PUT_SEND_NOTE_WROTE_KEYWORD							=	"PUT_SEND_NOTE_WROTE_KEYWORD";
	public static final String PUT_CALLER_ACTIVITY									=	"PUT_CALLER_ACTIVITY";
	public static final String PUT_TODAY_RUN_CHECK									=	"PUT_TODAY_RUN_CHECK";
	
	// 호출자 액티비티 관련 변수
	public static final String CALLER_CREATE_IDEA									=	"CALLER_CREATE_IDEA";
	public static final String CALLER_MANAGE_IDEA									=	"CALLER_MANAGE_IDEA";
	
	// 데이터베이스 관련 변수
	public static final String DATABASE_NAME										=	"AllThatIdea.db";
	public static final int DATABASE_VERSION										=	1;
	public static final int DATABASE_FILE_INDEX										=	1;
	public static final String DATABASE_ROOT_DIR									=	"/data/data/madcat.studio.idea/databases/";
	
	// 데이터베이스 테이블 관련 변수
	public static final String TABLE_IDEA_NOTE										=	"NOTE";
	public static final String NOTE_COLUMN_ID										=	"_id";
	public static final String NOTE_COLUMN_DATE										=	"_date";
	public static final String NOTE_COLUMN_KEYWORD									=	"_keyword";
	public static final String NOTE_COLUMN_GRAPHICS									=	"_graphics";
	
	public static final String TABLE_IDEA_KEYWORD									=	"KEYWORD";
	public static final String KEYWORD_COLUMN_ID									=	"_id";
	public static final String KEYWORD_COLUMN_CATEGORY								=	"_category";
	public static final String KEYWORD_COLUMN_KEYWORD								=	"_keyword";
	
	public static final String TABLE_IDEA_CATEGORY									=	"CATEGORY";
	public static final String CATEGORY_COLUMN_ID									=	"_id";
	public static final String CATEGORY_COLUMN_CATEGORY								=	"_category";
	
	public static final String TABLE_IDEA_CHALLENGE									=	"CHALLENGE";
	public static final String CHALLENGE_COLUMN_ID									=	"_id";
	public static final String CHALLENGE_COLUMN_TITLE								=	"_title";
	public static final String CHALLENGE_COLUMN_TEXT								=	"_text";
	public static final String CHALLENGE_COLUMN_DIFFCULT							=	"_diffcult";
	public static final String CHALLENGE_COLUMN_ICON								=	"_icon";
	public static final String CHALLENGE_COLUMN_SUCCEED								=	"_succeed";
	
	public static final String TABLE_IDEA_BACKUP									=	"BACKUP";
	public static final String BACKUP_COLUMN_ID										=	"_id";
	public static final String BACKUP_COLUMN_DATE									=	"_date";
	public static final String BACKUP_COLUMN_KEYSIZE								=	"_keysize";
	public static final String BACKUP_COLUMN_NOTE_THEME_INDEX						=	"_t_idx";
	public static final String BACKUP_COLUMN_KEYWORD_A								=	"_keyword_a";
	public static final String BACKUP_COLUMN_KEYWORD_B								=	"_keyword_b";
	public static final String BACKUP_COLUMN_KEYWORD_C								=	"_keyword_c";
	public static final String BACKUP_COLUMN_CATEGORY_A								=	"_category_a";
	public static final String BACKUP_COLUMN_CATEGORY_B								=	"_category_b";
	public static final String BACKUP_COLUMN_CATEGORY_C								=	"_category_c";
	
	// 백업 & 복구 관련 변수
	public static final String IDEA_NOTE_PATH										=	"/AllThatIdea";
	public static final String IDEA_NOTE_BACKUP_PATH								=	"/MadCatStudio/AllThatIdea/BackUp";
	public static final String IDEA_NOTE_RESTORE_PATH								=	"/MadCatStudio/AllThatIdea/Restore";
	
	// 다이얼로그 플래그 변수
	public static final int DIALOG_KEYWORD_ADD										=	1;
	public static final int DIALOG_KEYWORD_MODIFY									=	2;
	
	public static final int DIALOG_CATEGORY_ADD										=	1;
	public static final int DIALOG_CATEGORY_MODIFY									=	2;
	
	public static final int DIALOG_CALENDAR_NOTE_DEFAULT							=	0;
	public static final int DIALOG_CALENDAR_NOTE_VIEW								=	1;
	public static final int DIALOG_CALENDAR_NOTE_DELETE								=	2;
	
	// 도전과제 환경 변수
	public static final String COMPLETE_CHALLENGE_SHAKE								=	"COMPLETE_CHALLENGE_SHAKE";
	public static final String COMPLETE_CHALLENGE_TIME_AND_ROOM_OF_THE_MIND			=	"COMPLETE_CHALLENGE_TIME_AND_ROOM_OF_THE_MIND";
	public static final String COMPLETE_CHALLENGE_WORK_ALL_NIGHT					=	"COMPLETE_CHALLENGE_WORK_ALL_NIGHT";
	public static final String COMPLETE_CHALLENGE_MAD								=	"COMPLETE_CHALLENGE_MAD";
	public static final String COMPLETE_CHALLENGE_BEGINNER_PLANNER					=	"COMPLETE_CHALLENGE_BEGINNER_PLANNER";
	public static final String COMPLETE_CHALLENGE_INTERMEDIATE_PLANNER				=	"COMPLETE_CHALLENGE_INTERMEDIATE_PLANNER";
	public static final String COMPLETE_CHALLENGE_ADVANCED_PLANNER					=	"COMPLETE_CHALLENGE_ADVANCED_PLANNER";
	public static final String COMPLETE_CHALLENGE_IDEA_BANK							=	"COMPLETE_CHALLENGE_IDEA_BANK";
	public static final String COMPLETE_CHALLENGE_AMOUNT_OF_QUALITY					=	"COMPLETE_CHALLENGE_AMOUNT_OF_QUALITY";
	public static final String COMPLETE_CHALLENGE_HARD_WORK							=	"COMPLETE_CHALLENGE_HARD_WORK";
	public static final String COMPLETE_CHALLENGE_NO_PAIN_NO_GAIN					=	"COMPLETE_CHALLENGE_NO_PAIN_NO_GAIN";	
	public static final String COMPLETE_CHALLENGE_MANUAL_CONQUEROR					=	"COMPLETE_CHALLENGE_MANUAL_CONQUEROR";
	public static final String COMPLETE_CHALLENGE_THANKS							=	"COMPLETE_CHALLENGE_THANKS";
	
	// 도전과제 명칭
	public static final String CHALLENGE_BEGINNER_PLANNER							=	"초급 기획자";
	public static final String CHALLENGE_INTERMEDIATE_PLANNER						=	"중급 기획자";
	public static final String CHALLENGE_ADVANCED_PLANNER							=	"고급 기획자";
	public static final String CHALLENGE_HARD_WORK									=	"근면성실";
	public static final String CHALLENGE_NO_PAIN_NO_GAIN							=	"지성이면 감천";
	public static final String CHALLENGE_IDEA_BANK									=	"아이디어 뱅크";
	public static final String CHALLENGE_SHAKE										=	"섞어야 제 맛";
	public static final String CHALLENGE_MANUAL_CONQUEROR							=	"메뉴얼 정복자";
	public static final String CHALLENGE_AMOUNT_OF_QUALITY							=	"질보단 양";
	public static final String CHALLENGE_MAD										=	"정신나간 녀석";
	public static final String CHALLENGE_THANKS										=	"감사합니다. 고갱님";
	public static final String CHALLENGE_TIME_AND_ROOM_OF_THE_MIND					=	"시간과 정신의 방";
	public static final String CHALLENGE_WORK_ALL_NIGHT								=	"철야근무";
	
	// 초급, 중급, 기획자 도전과제 변수
	public static final int MAX_BEGINNER_PLANNER									=	10;
	public static final int MAX_INTERMEDIATE_PLANNER								=	30;
	public static final int MAX_ADVANCED_PLANNER									=	50;
	public static final int MAX_IDEA_BANK											=	100;
	
	// 질보단 양 도전과제 변수
	public static final int MAX_AMOUNT_OF_QUALITY									=	10;
	
	// 근면성실, 지성이면 감천 도전과제 변수
	public static final int MAX_HARD_WORK											=	7;
	public static final int MAX_NO_PAIN_NO_GAIN										=	30;
	
	// 시간과 정신의 방 도전과제 변수
	public static final int MAX_CHALLENGE_TIME										=	1800;
	
	// 노트 관련 변수
	public static final int MAX_NOTE_PAGE											=	10;
	
	// Intent Action 관련 변수
	public static final String ACTION_WIDGET_UPDATE									=	"android.appwidget.action.APPWIDGET_UPDATE";
	public static final String ACTION_THEME_UPDATE									=	"action.AllThatIdea.THEME";
	public static final String ACTION_KEYWORD_REFRESH								=	"actoin.AllThatIdea.REFRESH";
	public static final String ACTION_KEYWORD_NUMBER								=	"action.AllThatIdea.NUMBER";
	
	// Intent Caller 관련 변수
	public static final String CALL_BY_WIDGET_REFRESH								=	"CALL_BY_WIDGET_REFRESH";
	
	// 기타 변수
	public static final String DEFAULT_KEYWORD										=	"DEFAULT";
	public static final String DEFAULT_ALL_CATEGORY									=	"All";
	public static final int MAX_KEYWORD_TEXTVIEW									=	3;
	public static final int MAX_REFREHS_CHALLENGE_COUNT								=	30;
	public static final String PREFIX_CHALLENGE_ICON								=	"challenge_icon_";
	public static final long BUTTON_SAFE_DELAY										=	3000;
	public static final long BUTTON_MAD_SAFE_DELAY									=	500;
	public static final int DEFAULT_KEYWORD_TEXTVIEW_SIZE							=	22;
	public static final String DEFAULT_LINE_BREAK									=	"\n";
}
