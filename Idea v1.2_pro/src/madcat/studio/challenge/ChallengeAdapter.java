package madcat.studio.challenge;

import java.util.ArrayList;

import madcat.studio.idea.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class ChallengeAdapter extends ArrayAdapter<Challenge> {
	
	private Context mContext;
	private int mResId;
	private ArrayList<Challenge> mItems;
	private LayoutInflater mLayoutInflater;
	
	public ChallengeAdapter(Context context, int resId, ArrayList<Challenge> items) {
		super(context, resId, items);
		
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mLayoutInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			holder.imageIcon = (ImageView)convertView.findViewById(R.id.challenge_icon);
			holder.ratingDiffcult = (RatingBar)convertView.findViewById(R.id.challenge_diffcult);
			holder.textTitle = (TextView)convertView.findViewById(R.id.challenge_title);
			holder.textDetail = (TextView)convertView.findViewById(R.id.challenge_detail);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder)convertView.getTag();
		}
		
		switch(mItems.get(position).isSucceed()) {
		case 0:		//	도전과제 실패
			holder.imageIcon.setBackgroundResource(R.drawable.challenge_icon_lock);
			holder.textDetail.setText(mContext.getString(R.string.etc_challenge_lock_text));
			break;
		case 1:		//	도전과제 성공
			holder.imageIcon.setBackgroundResource(mItems.get(position).getIconId());
			holder.textDetail.setText(mItems.get(position).getText());
			break;
		}
		
		
		holder.ratingDiffcult.setRating(mItems.get(position).getDiffcult());
		holder.textTitle.setText(mItems.get(position).getTitle());
		

		return convertView;
	}
	
	public class ViewHolder {
		public ImageView imageIcon;
		public RatingBar ratingDiffcult;
		public TextView textTitle;
		public TextView textDetail;
	}
}
