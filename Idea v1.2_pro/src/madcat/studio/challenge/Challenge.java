package madcat.studio.challenge;

public class Challenge {

	private String mTitle;
	private String mText;
	private float mDiffcult;
	private int mIconId;
	private int mSucceed;
	
	public int isSucceed() {	return mSucceed;	}
	public void setSucceed(int succeed) {	this.mSucceed = succeed;	}
	public float getDiffcult() {	return mDiffcult;	}
	public void setDiffcult(float diffcult) {	this.mDiffcult = diffcult;	}
	public String getText() {	return mText;	}
	public void setText(String text) {	this.mText = text;	}
	public String getTitle() {	return mTitle;	}
	public void setTitle(String title) {	this.mTitle = title;	}
	public int getIconId() {	return mIconId;	}
	public void setIconId(int iconId) {	this.mIconId = iconId;	}
	
}
