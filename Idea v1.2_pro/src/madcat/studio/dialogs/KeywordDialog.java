package madcat.studio.dialogs;

import madcat.studio.constants.Constants;
import madcat.studio.idea.R;
import madcat.studio.keyword.Keyword;
import madcat.studio.utils.Utils;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class KeywordDialog extends Dialog implements OnClickListener, OnItemSelectedListener {
	
//	private final String TAG										=	"AddKeywordDialog";
	
	private boolean mDialogFlag										=	false;
	
	private Context mContext;
	private Button mBtnOk, mBtnCancel;
	private EditText mEditKeyword;
	private Spinner mSpinCategory;
	
	private String mSelectedCategory;
	private int mCtrlFlag;
	private Keyword mKeyword;
	
	private String mPrevKeyword, mPrevCategory;
	
	public KeywordDialog(Context context, int ctrlFlag) {
		super(context);
		this.mContext = context;
		this.mCtrlFlag = ctrlFlag;
	}
	
	public KeywordDialog(Context context, int ctrlFlag, Keyword keyword) {
		super(context);
		this.mContext = context;
		this.mCtrlFlag = ctrlFlag;
		this.mKeyword = keyword;
	}
	
	public boolean getDialogFlag() {
		return mDialogFlag;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		switch(mCtrlFlag) {
		case Constants.DIALOG_KEYWORD_ADD:
			setTitle(mContext.getString(R.string.dialog_title_keyword_add));
			break;
		case Constants.DIALOG_KEYWORD_MODIFY:
			setTitle(mContext.getString(R.string.dialog_title_keyword_modify));
			break;
		default:
			break;
		}
		
		setContentView(R.layout.dialog_keyword_add);
		
		// Resource Id 설정
		mBtnOk = (Button)findViewById(R.id.dialog_keyword_add_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dialog_keyword_add_btn_cancel);
		mEditKeyword = (EditText)findViewById(R.id.dialog_keyword_add_name);
		mSpinCategory = (Spinner)findViewById(R.id.dialog_keyword_select_category);
		
		// 리스너 설정
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		mSpinCategory.setOnItemSelectedListener(this);
		
		// Spinner 설정
		String[] category = Utils.getCategoryListToString(mContext);
		ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, category);
		spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinCategory.setAdapter(spinnerAdapter);
		
		// 수정 시 선택된 값을 불러온다.
		if(mCtrlFlag == Constants.DIALOG_KEYWORD_MODIFY) {
			mPrevKeyword = mKeyword.getKeyword();
			mPrevCategory = mKeyword.getCategory();
			
			// 키워드 값을 재설정
			mEditKeyword.setText(mPrevKeyword);
			
			// Spinner 값을 재설정
			for(int i=0; i < category.length; i++) {
				if(category[i].equals(mPrevCategory)) {
					mSpinCategory.setSelection(i);
				}
			}
			
			mSelectedCategory = mPrevCategory;
		}
	}
	
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.dialog_keyword_add_btn_ok:
				if(mEditKeyword.length() != 0) {
					// 데이터 베이스에 삽입
					String insertKeyword = mEditKeyword.getText().toString().trim();
					SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);

					if(mCtrlFlag == Constants.DIALOG_KEYWORD_ADD) {
//						Log.d(TAG, "키워드 추가");
						if(!Utils.duplicationKeywordCheck(sdb, insertKeyword)) {
							ContentValues values = new ContentValues();
							values.put(Constants.KEYWORD_COLUMN_CATEGORY, mSelectedCategory);
							values.put(Constants.KEYWORD_COLUMN_KEYWORD, insertKeyword);
							sdb.insert(Constants.TABLE_IDEA_KEYWORD, null, values);
							
							mDialogFlag = true;
							dismiss();
//							Log.d(TAG, "키워드 추가 완료");
						} else {
							Toast.makeText(mContext, mContext.getString(R.string.toast_title_duplication_keyword), Toast.LENGTH_SHORT).show();
						}
					} else if(mCtrlFlag == Constants.DIALOG_KEYWORD_MODIFY) {
//						Log.d(TAG, "키워드 수정");
						if(!mPrevKeyword.equals(insertKeyword) || 
								(!mPrevKeyword.equals(insertKeyword) && !mPrevCategory.equals(mSelectedCategory))) {	// 키워드 혹은 키워드,카테고리
							if(!Utils.duplicationKeywordCheck(sdb, insertKeyword)) {
								ContentValues updateValues = new ContentValues();
								updateValues.put(Constants.KEYWORD_COLUMN_CATEGORY, mSelectedCategory);
								updateValues.put(Constants.KEYWORD_COLUMN_KEYWORD, insertKeyword);
								sdb.update(Constants.TABLE_IDEA_KEYWORD, updateValues, 
										Constants.KEYWORD_COLUMN_KEYWORD + " = '" + mPrevKeyword + "'", null);
								
								mDialogFlag = true;
								dismiss();
								
//								Log.d(TAG, "키워드 혹은 키워드와 카테고리가 변경되었습니다.");
							} else {
								Toast.makeText(mContext, mContext.getString(R.string.toast_title_duplication_keyword), Toast.LENGTH_SHORT).show();
							}
						} else if(mPrevKeyword.equals(insertKeyword) && !mPrevCategory.equals(mSelectedCategory)) {		// 카테고리만 변경
							ContentValues updateValues = new ContentValues();
							updateValues.put(Constants.KEYWORD_COLUMN_CATEGORY, mSelectedCategory);
							sdb.update(Constants.TABLE_IDEA_KEYWORD, updateValues, 
									Constants.KEYWORD_COLUMN_KEYWORD + " = '" + mPrevKeyword + "'", null);
							
							mDialogFlag = true;
							dismiss();
							
//							Log.d(TAG, "카테고리만 변경 되었습니다.");
						} else {		// 변경 안함
							mDialogFlag = false;
							dismiss();
							
//							Log.d(TAG, "변경 안함");
						}
					}
					
					sdb.close();
				} else {
					Toast.makeText(mContext, mContext.getString(R.string.toast_title_empty_add_keyword), Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.dialog_keyword_add_btn_cancel:
				mDialogFlag = false;
				dismiss();
				break;
			default:
				break;
		}
	}

	public void onItemSelected(AdapterView<?> parent, View v, int position,long id) {
		mSelectedCategory = mSpinCategory.getSelectedItem().toString();
	}

	public void onNothingSelected(AdapterView<?> arg0) {}
}
