package madcat.studio.dialogs;

import madcat.studio.constants.Constants;
import madcat.studio.idea.R;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;


public class ChoiceCategoryDialog extends Dialog implements OnClickListener, OnItemSelectedListener {
	
//	private final String TAG										=	"ChoiceCategoryDialog";
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private Button mBtnOk, mBtnCancel;
	private Spinner[] mSpinCategory;
	private String[] mCategoryList, mSelectedCategory;
	
	public ChoiceCategoryDialog(Context context) {
		super(context);
		mContext = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(mContext.getString(R.string.dialog_title_choice_keyword_category));
		setContentView(R.layout.dialog_category_choice);
		
		// SharedPrefrence 설정
		mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		// 데이터베이스에 저장된 카테고리 목록을 미리 불러온다.
		mCategoryList = Utils.getCategoryListToString(mContext);
		
		// Resource Id 설정
		mBtnOk = (Button)findViewById(R.id.dialog_choice_category_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dialog_choice_category_btn_cancel);

		// Spinner 설정
		mSpinCategory = new Spinner[Constants.MAX_KEYWORD_TEXTVIEW];
		ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, mCategoryList);
		spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		for(int i=0; i < mSpinCategory.length; i++) {
			int resId = mContext.getResources().getIdentifier("dialog_choice_category_" + (i+1), "id", mContext.getPackageName());
			mSpinCategory[i] = (Spinner)findViewById(resId);
			mSpinCategory[i].setAdapter(spinnerAdapter);
			mSpinCategory[i].setOnItemSelectedListener(this);				//	Spinner 리스너 설정
		}
		
		// 사용자가 설정한 카테고리를 불러와서 Spinner 값을 설정한다.
		mSelectedCategory = new String[Constants.MAX_KEYWORD_TEXTVIEW];
		
		for(int i=0; i < mSelectedCategory.length; i++) {
			mSelectedCategory[i] = mConfigPref.getString(Constants.CONFIG_KEYWORD_CATEGORY + (i+1), Constants.DEFAULT_ALL_CATEGORY);
			for(int j=0; j < mCategoryList.length; j++) {
				if(mCategoryList[j].equals(mSelectedCategory[i])) {
					mSpinCategory[i].setSelection(j);
				}
			}
		}
		
		// 이벤트 리스너 설정
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.dialog_choice_category_btn_ok:
				boolean lessKeywordFlag = false;
				
				// 각 카테고리의 키워드가 4개 미만이면 설정할 수 없도록 한다.
				for(int i=0; i < mSelectedCategory.length; i++) {
					if(Utils.getKeywordList(mContext, mSelectedCategory[i]).size() < 4) {
						lessKeywordFlag = true;
					}
				}
				
				if(!lessKeywordFlag) {
					SharedPreferences.Editor configEditor = mConfigPref.edit();
					for(int i=0; i < mSelectedCategory.length; i++) {
						configEditor.putString(Constants.CONFIG_KEYWORD_CATEGORY + (i+1), mSelectedCategory[i]);
					}
					configEditor.commit();
					dismiss();
					
					Toast.makeText(mContext, mContext.getString(R.string.toast_title_notice_select_category), Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(mContext, mContext.getString(R.string.toast_title_caution_select_category), Toast.LENGTH_SHORT).show();
				}
				
				break;
			case R.id.dialog_choice_category_btn_cancel:
				dismiss();
				break;
			default:
				break;
		}
	}

	public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
		for(int i=0; i < mSpinCategory.length; i++) {
			mSelectedCategory[i] = mSpinCategory[i].getSelectedItem().toString();
		}
	}

	public void onNothingSelected(AdapterView<?> arg0) {}
}
