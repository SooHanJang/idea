package madcat.studio.dialogs;

import madcat.studio.idea.R;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;


public class NoSaveNoteExit extends Dialog implements OnClickListener {

//	private final String TAG										=	"NoSaveNoteExit";
	
	private final int NOTE_STATE_DEFAULT							=	0;
	private final int NOTE_NO_SAVE_EXIT								=	1;
	private final int NOTE_SAVE_EXIT								=	2;
	private final int NOTE_CANCEL									=	3;
	
	private int mDialogFlag											=	NOTE_STATE_DEFAULT;
	
	private Context mContext;
	private ImageButton mBtnOk, mBtnCancel, mBtnExit;
	
	public NoSaveNoteExit(Context context, int styleId) {
		super(context, styleId);
		this.mContext = context;
	}
	
	public int getDialogFlag() {
		return mDialogFlag;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_no_save_note_exit);
		
		mBtnOk = (ImageButton)findViewById(R.id.dialog_no_save_btn_ok);
		mBtnCancel = (ImageButton)findViewById(R.id.dialog_no_save_cancel);
		mBtnExit = (ImageButton)findViewById(R.id.dialog_no_save_btn_exit);
		
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		mBtnExit.setOnClickListener(this);
	}

	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.dialog_no_save_btn_ok:
			mDialogFlag = NOTE_SAVE_EXIT;
			dismiss();
			break;
		case R.id.dialog_no_save_cancel:
			mDialogFlag = NOTE_CANCEL;
			dismiss();
			break;
		case R.id.dialog_no_save_btn_exit:
			mDialogFlag = NOTE_NO_SAVE_EXIT;
			dismiss();
			break;
		}
	}
}
