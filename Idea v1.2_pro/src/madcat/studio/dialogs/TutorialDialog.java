package madcat.studio.dialogs;

import madcat.studio.constants.Constants;
import madcat.studio.idea.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;


public class TutorialDialog extends Dialog {
	
	private Context mContext;
	private CheckBox mCheckBox;
	
	public TutorialDialog(Context context, int styleId) {
		super(context, styleId);
		mContext = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_tutorial);
		
		mCheckBox = (CheckBox)findViewById(R.id.tutorial_check);
		
		mCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					SharedPreferences configPref = mContext.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
					SharedPreferences.Editor editor = configPref.edit();
					
					editor.putBoolean(Constants.CONFIG_TUTORIAL_CHECK, true);
					editor.commit();
					
					dismiss();
				}
			}
		});
	}
}
