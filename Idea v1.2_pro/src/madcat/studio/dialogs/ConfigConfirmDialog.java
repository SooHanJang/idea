package madcat.studio.dialogs;

import madcat.studio.idea.R;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class ConfigConfirmDialog extends Dialog implements OnClickListener {

//	private final String TAG												=	"ConfirmDialog";
	private boolean mDialogStateFlag										=	false;
	
	private Context mContext;
	private TextView mTextTitle;
	private Button mBtnOk, mBtnCancel;
	
	private String mDialogTitle;
	
	public ConfigConfirmDialog(Context context, String title) {
		super(context);
		mContext = context;
		mDialogTitle = title;
	}
	
	public void setTitleText(String title) {
		mDialogTitle = title;
	}
	
	public boolean getDialogStateFlag() {
		return mDialogStateFlag;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_config_confirm);
		
		mTextTitle = (TextView)findViewById(R.id.dialog_confirm_title);
		mBtnOk = (Button)findViewById(R.id.dialog_confirm_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dialog_confirm_btn_cancel);
		
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
		// setTitleText 함수를 통해 넘어온 값으로 다이얼로그에 표시될 텍스트를 설정
		if(mDialogTitle != null) {
			mTextTitle.setText(mDialogTitle);
		}
	}
	
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.dialog_confirm_btn_ok:
				mDialogStateFlag = true;
				dismiss();
				break;
			case R.id.dialog_confirm_btn_cancel:
				mDialogStateFlag = false;
				dismiss();
				break;
		}
	}
}
