package madcat.studio.dialogs;

import madcat.studio.constants.Constants;
import madcat.studio.idea.R;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CategoryDialog extends Dialog implements OnClickListener {
	
//	private final String TAG										=	"AddCategoryDialog";
	private boolean mDialogFlag										=	false;
	
	private Context mContext;
	private Button mBtnOk, mBtnCancel;
	private EditText mEditCategory;
	
	private int mCtrlFlag;
	private String mPrevCategory;
	private SharedPreferences mConfigPref;
	
	public CategoryDialog(Context context, int ctrlFlag) {
		super(context);
		this.mCtrlFlag = ctrlFlag;
		this.mContext = context;
		this.mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
	}
	
	public CategoryDialog(Context context, int ctrlFlag, String category) {
		super(context);
		this.mContext = context;
		this.mCtrlFlag = ctrlFlag;
		this.mPrevCategory = category;
		this.mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
	}
	
	public boolean getDialogFlag() {
		return mDialogFlag;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		switch(mCtrlFlag) {
		case Constants.DIALOG_CATEGORY_ADD:
			setTitle(mContext.getString(R.string.dialog_title_category_add));
			break;
		case Constants.DIALOG_CATEGORY_MODIFY:
			setTitle(mContext.getString(R.string.dialog_title_category_modify));
			break;
		default:
			break;
		}

		setContentView(R.layout.dialog_category_add);
		
		// Resource Id 설정
		mBtnOk = (Button)findViewById(R.id.dialog_category_add_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dialog_category_add_btn_cancel);
		mEditCategory = (EditText)findViewById(R.id.dialog_category_add_name);
		
		// 리스너 설정
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
		// 수정 시 선택된 값을 불러온다.
		if(mCtrlFlag == Constants.DIALOG_CATEGORY_MODIFY) {
			mEditCategory.setText(mPrevCategory);
		}
	}

	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.dialog_category_add_btn_ok:
			if(mEditCategory.length() != 0) {
				// 데이터베이스에 삽입
				String insertCategory = mEditCategory.getText().toString().trim();
				SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
				
				if(mCtrlFlag == Constants.DIALOG_CATEGORY_ADD) {
					if(!Utils.duplicationCategoryCheck(sdb, insertCategory)) {
						ContentValues insertValues = new ContentValues();
						insertValues.put(Constants.CATEGORY_COLUMN_CATEGORY, insertCategory);
						sdb.insert(Constants.TABLE_IDEA_CATEGORY, null, insertValues);
						
						mDialogFlag = true;
						dismiss();
					} else {
						Toast.makeText(mContext, mContext.getString(R.string.toast_title_duplication_category), Toast.LENGTH_SHORT).show();
					}
				} else if(mCtrlFlag == Constants.DIALOG_CATEGORY_MODIFY) {
					if(!mPrevCategory.equals(insertCategory)) {
						if(!Utils.duplicationCategoryCheck(sdb, insertCategory)) {
							// 카테고리 테이블의 카테고리를 변경
							ContentValues updateCategoryValues = new ContentValues();
							updateCategoryValues.put(Constants.CATEGORY_COLUMN_CATEGORY, insertCategory);
							sdb.update(Constants.TABLE_IDEA_CATEGORY, updateCategoryValues, 
									   Constants.CATEGORY_COLUMN_CATEGORY + " = '" + mPrevCategory + "'", null);
							
							// 키워드 테이블의 카테고리를 변경
							ContentValues updateKeywordValues = new ContentValues();
							updateKeywordValues.put(Constants.KEYWORD_COLUMN_CATEGORY, insertCategory);
							sdb.update(Constants.TABLE_IDEA_KEYWORD, updateKeywordValues, 
									   Constants.KEYWORD_COLUMN_CATEGORY + " = '" + mPrevCategory + "'", null);
							
							// 사용자가 환경설정에 등록한 카테고리 값을 수정 값으로 변경
							SharedPreferences.Editor editor = mConfigPref.edit();
							for(int i=0; i < Constants.MAX_KEYWORD_TEXTVIEW; i++) {
								if(mPrevCategory.equals(
										mConfigPref.getString(Constants.CONFIG_KEYWORD_CATEGORY + (i+1), Constants.DEFAULT_ALL_CATEGORY))) {
									editor.putString(Constants.CONFIG_KEYWORD_CATEGORY + (i+1), insertCategory);
								}
							}
							editor.commit();
							
							mDialogFlag = true;
							dismiss();
						} else {
							Toast.makeText(mContext, mContext.getString(R.string.toast_title_duplication_category), Toast.LENGTH_SHORT).show();
						}
					} else {
						mDialogFlag = false;
						dismiss();
					}
				}
				
				sdb.close();
			} else {
				Toast.makeText(mContext, mContext.getString(R.string.toast_title_empty_add_category), Toast.LENGTH_SHORT).show();
			}
			
			break;
		case R.id.dialog_category_add_btn_cancel:
			mDialogFlag = false;
			dismiss();
			break;
		default:
			break;
		}
	}
}
