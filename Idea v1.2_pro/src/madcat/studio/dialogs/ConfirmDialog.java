package madcat.studio.dialogs;

import madcat.studio.idea.R;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;

public class ConfirmDialog extends Dialog implements OnClickListener {

//	private final String TAG												=	"ConfirmDialog";
	private boolean mDialogStateFlag										=	false;
	
	private Context mContext;
	private ImageView mDialogTitle;
	private ImageButton mBtnOk, mBtnCancel;
	
	private int mResId;
	
	public ConfirmDialog(Context context, int resId) {
		super(context, R.style.PopupDialog);
		mContext = context;
		mResId = resId;
	}
	
	public boolean getDialogStateFlag() {
		return mDialogStateFlag;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_confirm);
		
		mDialogTitle = (ImageView)findViewById(R.id.dialog_confirm_title);
		mBtnOk = (ImageButton)findViewById(R.id.dialog_confirm_btn_ok);
		mBtnCancel = (ImageButton)findViewById(R.id.dialog_confirm_btn_cancel);
		
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
		// setTitleText 함수를 통해 넘어온 값으로 다이얼로그에 표시될 텍스트를 설정
		if(mResId != 0) {
			mDialogTitle.setBackgroundResource(mResId);
		}
	}
	
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.dialog_confirm_btn_ok:
				mDialogStateFlag = true;
				dismiss();
				break;
			case R.id.dialog_confirm_btn_cancel:
				mDialogStateFlag = false;
				dismiss();
				break;
		}
	}
}
