package madcat.studio.dialogs;

import madcat.studio.idea.R;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class ContextListDialog extends Dialog implements OnClickListener {

//	private final String TAG										=	"ContextListDialog";
	
	private int FLAG_NOTE_DEFAULT									=	0;
	private int FLAG_NOTE_VIEW										=	1;
	private int FLAG_NOTE_DELETE									=	2;
	
	private int mDialogFlag											=	FLAG_NOTE_DEFAULT;
	
	private Context mContext;
	private ImageButton mBtnNoteView, mBtnNoteDel;
	
	public ContextListDialog(Context context, int styleId) {
		super(context, styleId);
		this.mContext = context;
	}
	
	public int getDialogFlag() {
		return mDialogFlag;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_note_view_list);
		
		mBtnNoteView = (ImageButton)findViewById(R.id.dialog_note_view);
		mBtnNoteDel = (ImageButton)findViewById(R.id.dialog_note_delete);
		
		mBtnNoteView.setOnClickListener(this);
		mBtnNoteDel.setOnClickListener(this);
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.dialog_note_view:
				mDialogFlag = FLAG_NOTE_VIEW;
				dismiss();
				break;
			case R.id.dialog_note_delete:
				mDialogFlag = FLAG_NOTE_DELETE;
				dismiss();
				break;
			default:
				mDialogFlag = FLAG_NOTE_DEFAULT;
				dismiss();
				break;
		}
	}
}
